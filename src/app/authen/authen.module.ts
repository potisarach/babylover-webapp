import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthenComponent } from './authen.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { AuthenRoutingModule } from './authen-routing.module';
import { Ng4LoadingSpinnerModule } from 'ng4-loading-spinner';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    AuthenRoutingModule, 
    Ng4LoadingSpinnerModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  declarations: [
    AuthenComponent, 
    LoginComponent, 
    RegisterComponent
  ]
})
export class AuthenModule { }
