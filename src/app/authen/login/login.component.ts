import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthenticationService } from '../../shares/authentication.service';
import { GlobalFunctionService } from '../../shares/global-function.service';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { DialogBoxService } from '../../shares/dialog-box/dialog-box.service';

declare var $: any;

@Component({
  selector: 'ngx-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  username: string;
  password: string;
  constructor(
    public func: GlobalFunctionService,
    private loading: Ng4LoadingSpinnerService,
    private router: Router,
    private dialog: DialogBoxService,
    private authenticationService: AuthenticationService) {

  }

  ngOnInit() {

  }

  async onSubmit() {

    this.loading.show();
    let user = await this.authenticationService.login(this.username, this.password);
    if (user.success && user.body.token) {
      console.log('user =>', user);
      this.router.navigate(["/"]);
    } else {
      console.log('user =>', user);
      this.dialog.error({ title: "ไม่สามารถเข้าสู่ระบบได้", message: "username or password invalid!" });
    }
    this.loading.hide();
  }

  async onEnter($event) {
    var keycode = $event.which || $event.keycode;
    if (keycode == 13) {
      this.loading.show();

      let user = await this.authenticationService.login(this.username, this.password);
      
      if (user.success && user.body.token) {
        console.log('user =>', user);
        this.router.navigate(["/"]);
      } else {
        console.log('user =>', user);
        this.dialog.error({ title: "ไม่สามารถเข้าสู่ระบบได้", message: user.error });
      }
      this.loading.hide();
    }
  }
}
