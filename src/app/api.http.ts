import { ReflectiveInjector } from "@angular/core";
import { Http, Headers, BrowserXhr, RequestOptions, BaseRequestOptions, ResponseOptions, BaseResponseOptions, ConnectionBackend, XHRBackend, XSRFStrategy, CookieXSRFStrategy } from "@angular/http";
import * as _config from './global.conf';
export class ApiHTTP{

    public http: Http;
    constructor() {
        let injector = ReflectiveInjector.resolveAndCreate([
            Http,
            BrowserXhr,
            { provide: RequestOptions, useClass: BaseRequestOptions },
            { provide: ResponseOptions, useClass: BaseResponseOptions },
            { provide: ConnectionBackend, useClass: XHRBackend },
            { provide: XSRFStrategy, useFactory: () => new CookieXSRFStrategy() },
        ]);
        this.http = injector.get(Http);
    }


    public async post(url: string, params: any){
        let headobj = {'Content-Type': 'application/json'};
        try{
            let headers = new Headers(headobj);
            let options = new RequestOptions({ headers: headers, method: "post" });
            let result = await this.http.post(_config.HOST_API_SERVER + url, params, options).toPromise();
            return this.success(url, params, headobj, result.json());
        }catch(err){
            return this.error(url, params, headobj, err);
        }
    }
    private success(url, params, headobj, result):{data:any}{
        console.log('SUCCESSED >>>', url);
        console.log('parameters', params);
        console.log('DATA', result);
        console.log('<<<');
        return { data: result};
    }
    private error(url, params, headobj, error):{error:any}{
        console.log('ERROR >>>', url);
        console.log('parameters', params);
        console.log(error);
        console.log('<<<');
        return { error: error};
    }
}