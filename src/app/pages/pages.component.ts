import { Component, OnInit } from '@angular/core';

import { MENU_ITEMS } from './pages-menu';
import { GlobalHttpService } from '../shares/global-http.service';

@Component({
  selector: 'ngx-pages',
  template: `
    <ngx-sample-layout>
      <nb-menu [items]="menu2"></nb-menu>
      <router-outlet></router-outlet>
    </ngx-sample-layout>
  `,
})
export class PagesComponent implements OnInit {

  menu: any;
  menu2: any;
  user: any;
  username: string;
  password: string;
  public model = {
    username: "",
    firstname: "",
    lastname: "",
    email: "",
    phone: "",
    userstatusid: "",
    userstatusname: ""
  }

  constructor(
    private http: GlobalHttpService) {
  }

  ngOnInit() {
    this.GetUser();

    this.menu = MENU_ITEMS;
    console.log("this.menu", this.menu[4]);

    console.log("this.model.userstatusname", this.model.userstatusname);
    
    this.menu2 = this.menu;
    console.log("this.menu2 > ", this.menu2);
    
  }

  async GetUser() {
    let strUser = localStorage.getItem('currentUser');
    console.log("strUser คือ ", strUser);
    let usernames = JSON.parse(strUser);
    console.log("strUser คือ > ", strUser);
    let usernamess = usernames.username;
    let user = await this.http.post('api/user/get', { username: usernamess });
    this.model = user.body;
   
    if (this.model.userstatusname == "ผู้ดูแลระบบ" || this.model.userstatusid == "1") {
      this.menu[4] = {
        title: 'จัดการข้อมูลผู้ใช้งาน',
        icon: 'fas fa-user',
        link: '/pages/UserManage',
        hidden: false
      }
    }else{
      this.menu[4] = {
        title: 'ไม่แสดง',
        icon: 'fas fa-user',
        link: '/pages/UserManage',
        hidden: true
      }
    }

  }

}
