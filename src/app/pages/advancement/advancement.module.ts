import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdvancementRoutingModule } from './advancement-routing.module';
import { AdvancementManagementComponent, OrphanFormTbButtonViewComponent} from './advancement-management/advancement-management.component';
import { FormsModule } from '../forms/forms.module';
import { ReactiveFormsModule } from '@angular/forms';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { ThemeModule } from '../../@theme/theme.module';
import { MatButtonModule, MatCheckboxModule, MatIconModule } from '@angular/material';
import { Ng4LoadingSpinnerModule } from 'ng4-loading-spinner';
import { ModalAdvancementComponent, OrphanFormTbButtonViewAdvancementComponent} from './advancement-management/modal-advancement/modal-advancement.component';
import { ModalReportAdvancementComponent } from './advancement-management/modal-advancement/modal-report-advancement/modal-report-advancement.component';
import { ModalViewAdvancementComponent } from './advancement-management/modal-advancement/modal-view-advancement/modal-view-advancement.component';
import { ModalEditAdvancementComponent } from './advancement-management/modal-advancement/modal-edit-advancement/modal-edit-advancement.component';

@NgModule({
  imports: [
    CommonModule,
    AdvancementRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    Ng2SmartTableModule,
    ThemeModule,
    MatButtonModule,
    MatCheckboxModule,
    MatIconModule,
    Ng4LoadingSpinnerModule.forRoot()
  ],
  declarations: [ 
                AdvancementManagementComponent,
                OrphanFormTbButtonViewComponent,
                ModalAdvancementComponent,
                ModalReportAdvancementComponent,
                OrphanFormTbButtonViewAdvancementComponent,
                ModalViewAdvancementComponent,
                ModalEditAdvancementComponent,
                
                ] , entryComponents: [
                OrphanFormTbButtonViewComponent,
                ModalAdvancementComponent,
                ModalReportAdvancementComponent,
                OrphanFormTbButtonViewAdvancementComponent,
                ModalViewAdvancementComponent,
                ModalEditAdvancementComponent,
              ]
})
export class AdvancementModule { }
