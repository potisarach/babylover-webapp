import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ModalReportAdvancementComponent } from './modal-report-advancement/modal-report-advancement.component';
import { LocalDataSource, ViewCell } from 'ng2-smart-table';
import { GlobalFunctionService } from '../../../../shares/global-function.service';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { GlobalHttpService } from '../../../../shares/global-http.service';
import { ModalViewAdvancementComponent } from './modal-view-advancement/modal-view-advancement.component';
import { ModalEditAdvancementComponent } from './modal-edit-advancement/modal-edit-advancement.component';
import { DialogBoxService } from '../../../../shares/dialog-box/dialog-box.service';

var advancementid; // ใช้เพื่อเก็บค่าข้อมูลที่ประเมินเด็ดล่าสุดแและเป็นเงื่อนไขแสดงปุ่มแก้ไขข้อมูลล่าสุด
@Component({
    selector: 'ngx-modal-advancement',
    templateUrl: './modal-advancement.component.html',
    styleUrls: ['./modal-advancement.component.scss']
})
export class ModalAdvancementComponent implements OnInit {
    public model = {
        fullname: "",
        orphanpersonid: "",
        orphanid: "",
        patronageid: "",
        patronid: "",
        firstname: "",
        lastname: "",
        birthdate: "",
        nickname: "",
        birthdateorphan: "",
        nicknameorphan: "",
        picturepath: "",
        fullnameorphan: "",
        fullnamepatron: "",
        agepatron: "",
        ageorphan: "",
        joinfamilydate: "",
        agestartorphan: "",
        ageendorphan: "",
        weight: "",
        hight: "",
        aroundhead: "",
        chest: "",
        searchdate: "",
        pastchanges: "",
        eat: "",
        sleep: "",
        excretion: "",
        characters: "",
        updatedate: "",
    }
    public modellistyear = [
        {
            // Year: ""
        }
    ]
    public datasource: LocalDataSource;
    public tbSettings: any;
    public datas = [];
    public imagePreView = "";
    constructor(private modalService: NgbModal, private activeModal: NgbActiveModal,
        public func: GlobalFunctionService, private loading: Ng4LoadingSpinnerService,
        private http: GlobalHttpService, private dialog: DialogBoxService) {

        this.datasource = new LocalDataSource();
        this.setColumns();
    }

    ngOnInit() {
        if (this.model.patronageid) {
            this.onSetjoinfamilydate();
        } else {
            this.model.fullnamepatron = " - ";
            this.model.joinfamilydate = " - ";
        }
        if (this.model.birthdateorphan) {
            this.model.birthdate = this.model.birthdateorphan;
            this.onSetbirthdate();
        } else {
            this.onSetbirthdate();
        }
        this.onGetYear();
    }
    AddReport() {
        const activeModal = this.modalService.open(ModalReportAdvancementComponent, { size: 'lg', keyboard: false, backdrop: "static", container: 'nb-layout' });
        activeModal.componentInstance.model = this.model;
    }
    onSetbirthdate() {
        let fullbirthdate = this.model.birthdate;
        let year = fullbirthdate.substr(0, 4);
        let month = fullbirthdate.substr(5, 2);
        let day = fullbirthdate.substr(8, 2);
        let y;
        let m;
        let d;

        if (month == "01") {
            m = "มกราคม";
        } else if (month == "02") {
            m = "กุมภาพันธ์";
        } else if (month == "03") {
            m = "มีนาคม";
        } else if (month == "04") {
            m = "เมษายน";
        } else if (month == "05") {
            m = "พฤษภาคม";
        } else if (month == "06") {
            m = "มิถุนายน";
        } else if (month == "07") {
            m = "กรกฎาคม";
        } else if (month == "08") {
            m = "สิงหาคม";
        } else if (month == "09") {
            m = "กันยายน";
        } else if (month == "10") {
            m = "ตุลาคม";
        } else if (month == "11") {
            m = "พฤศจิกายน";
        } else if (month == "12") {
            m = "ธันวาคม";
        }

        let years: any = parseInt(year) + 543;
        y = years;

        if (day < "10") {
            d = day.substr(1, 1);
        } else {
            d = day;
        }

        this.model.birthdate = d + " " + m + " " + y;
    }
    onSetjoinfamilydate() {
        let fullbirthdate = this.model.joinfamilydate;
        let year = fullbirthdate.substr(0, 4);
        let month = fullbirthdate.substr(5, 2);
        let day = fullbirthdate.substr(8, 2);
        let y;
        let m;
        let d;

        if (month == "01") {
            m = "มกราคม";
        } else if (month == "02") {
            m = "กุมภาพันธ์";
        } else if (month == "03") {
            m = "มีนาคม";
        } else if (month == "04") {
            m = "เมษายน";
        } else if (month == "05") {
            m = "พฤษภาคม";
        } else if (month == "06") {
            m = "มิถุนายน";
        } else if (month == "07") {
            m = "กรกฎาคม";
        } else if (month == "08") {
            m = "สิงหาคม";
        } else if (month == "09") {
            m = "กันยายน";
        } else if (month == "10") {
            m = "ตุลาคม";
        } else if (month == "11") {
            m = "พฤศจิกายน";
        } else if (month == "12") {
            m = "ธันวาคม";
        }

        let years: any = parseInt(year) + 543;
        y = years;

        if (day < "10") {
            d = day.substr(1, 1);
        } else {
            d = day;
        }
        this.model.joinfamilydate = d + " " + m + " " + y;
    }

    setDateAdvance(row) {

        let fullbirthdate = row.updatedate;
        let year = fullbirthdate.substr(0, 4);
        let month = fullbirthdate.substr(5, 2);
        let day = fullbirthdate.substr(8, 2);
        let y;
        let m;
        let d;

        if (month == "01") {
            m = "มกราคม";
        } else if (month == "02") {
            m = "กุมภาพันธ์";
        } else if (month == "03") {
            m = "มีนาคม";
        } else if (month == "04") {
            m = "เมษายน";
        } else if (month == "05") {
            m = "พฤษภาคม";
        } else if (month == "06") {
            m = "มิถุนายน";
        } else if (month == "07") {
            m = "กรกฎาคม";
        } else if (month == "08") {
            m = "สิงหาคม";
        } else if (month == "09") {
            m = "กันยายน";
        } else if (month == "10") {
            m = "ตุลาคม";
        } else if (month == "11") {
            m = "พฤศจิกายน";
        } else if (month == "12") {
            m = "ธันวาคม";
        }

        let years: any = parseInt(year) + 543;
        y = years;

        if (day < "10") {
            d = day.substr(1, 1);
        } else {
            d = day;
        }
        row.updatedate = d + " " + m + " " + y;
        return row.updatedate;
    }

    setColumns() {
        let _this = this;
        this.tbSettings = this.func.getTableSetting({
            updatedate: {
                title: 'วัน/เดือน/ปี',
                width: '100px',
                type: 'html',
                valuePrepareFunction: (cell, row) => {
                    _this.setDateAdvance(row);
                    return '<div class="text-center">' + row.updatedate + '</div>';
                }
            },
            advancement: {
                title: 'จัดการข้อมูล',
                width: '50px',
                type: 'custom',
                renderComponent: OrphanFormTbButtonViewAdvancementComponent,
                onComponentInitFunction(instance) {
                    instance.advancement.subscribe(row => {
                        _this.onAdvancementView(row);
                    });
                    instance.advancementedit.subscribe(row => {
                        _this.onAdvancementEdit(row);
                    });
                    instance.advancementdelete.subscribe(row => {
                        _this.onAdvancementDelete(row);
                    });
                }
            }
        });
    }
    async onGetYear() {
        let orphanid = this.model.orphanid;
        this.loading.show();
        let result = await this.http.post('api/advancement/getyear', { orphanid: orphanid });
        this.modellistyear = result.body;
        this.loading.hide();
    }

    async onSearchAdvancement() {
        let searchdate = this.model.searchdate;
        let orphanid = this.model.orphanid;
        this.loading.show();
        console.log("searchdate > > > : ", searchdate);

        if (searchdate == undefined) {
            this.dialog.alert({ title: 'แจ้งเตือน', message: "กรุณาเลือกข้อมูล" });
        } else {
            let result = await this.http.post('api/advancement/search', { searchdate: searchdate, orphanid: orphanid });
            console.log("Result", result.body);

            if (result.body != "") {
                advancementid = result.body[0].orphanadvancementid;
                console.log("Pass");

            } else {
                this.onGetYear();
                console.log("Error");

            }
            console.log("result.body > > > ", result.body);

            if (result.success) {
                this.datasource = this.func.getTableDataSource(result.body);
            }
        }

        this.loading.hide();
    }
    chkNum($event) {
        let charCode = $event.which || $event.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        } else {
            return true;
        }
    }
    async onAdvancementView(rowData) {
        this.loading.show();

        const activeModal = this.modalService.open(ModalViewAdvancementComponent, { size: 'lg', keyboard: false, backdrop: "static", container: 'nb-layout' });
        activeModal.componentInstance.model = rowData;
        activeModal.componentInstance.modelorphan = this.model;
        this.loading.hide();
    }
    async onAdvancementEdit(rowData) {
        this.loading.show();

        const activeModal = this.modalService.open(ModalEditAdvancementComponent, { size: 'lg', keyboard: false, backdrop: "static", container: 'nb-layout' });
        activeModal.componentInstance.model = rowData;
        activeModal.componentInstance.modelorphan = this.model;
        this.loading.hide();
    }
    async onAdvancementDelete(rowData) {
        this.dialog.confirm({ title: 'ยืนยัน', message: 'ต้องการลบข้อมูลใช่หรือไม่' }, async (ans) => {
            if (ans) {
                let orphanadvancementid = rowData.orphanadvancementid;
                console.log("orphanadvancementid > > > > ", orphanadvancementid);

                if (orphanadvancementid == undefined) {
                    this.dialog.alert({ title: 'แจ้งเตือน', message: "ไม่มีข้อมูล" });
                } else {
                    let result = await this.http.post('api/advancement/delete', { orphanadvancementid });
                    if (result.success) {
                        console.log("delete");
                        this.dialog.alert({ title: 'แจ้งเตือน', message: "ลบข้อมูลสำเร็จ" });
                        this.onSearchAdvancement();
                        console.log("rowData.orphanadvancementid >>> ", rowData.orphanadvancementid);
                    }else {
                        this.dialog.error({ title: 'แจ้งเตือน', message: "ไม่สามารถลบข้อมูลได้" });
                    }  
                }
            }
        });
    }
    close() {
        this.activeModal.close();
    }
}

@Component({
    selector: 'modal-advancement-list-button-view',
    template: `<div class="text-center">
    <a (click)="onAdvancementView()" class="button-view" title="ดูข้อมูล"><i class="far fa-eye icon"></i></a>&nbsp;&nbsp;
    <a href="{{func.getHostAPI()}}report/advancement/?orphanid={{rowData.orphanid}}&orphanadvancementid={{rowData.orphanadvancementid}}" class="button-view" title="ดาวน์โหลดข้อมูล"><i class="fas fa-file-download icon"></i></a>&nbsp;&nbsp;
    <a *ngIf="cadvancementid == this.rowData.orphanadvancementid" (click)="onAdvancementEdit()" class="button-view" title="แก้ไขข้อมูล"><i class="fas fa-edit"></i></a>&nbsp;&nbsp;
    <a *ngIf="cadvancementid == this.rowData.orphanadvancementid" (click)="onAdvancementDelete()" class="button-view" title="ลบข้อมูล" ><i class="fas fa-trash-alt icon"></i></a>
    </div>
  `,
    styleUrls: ['./modal-advancement.component.scss']
})

export class OrphanFormTbButtonViewAdvancementComponent implements ViewCell, OnInit {
    renderValue: string;
    constructor(private modalService: NgbModal, public func: GlobalFunctionService) { }
    public cadvancementid;

    @Input() value: string | number;
    @Input() rowData: any;
    @Input() advancementid: any;
    @Output() advancementedit: EventEmitter<any> = new EventEmitter();
    @Output() advancement: EventEmitter<any> = new EventEmitter();
    @Output() advancementdelete: EventEmitter<any> = new EventEmitter();

    ngOnInit() {
        this.renderValue = this.value.toString().toUpperCase();
        this.cadvancementid = advancementid;
    }
    onAdvancementView() {
        this.advancement.emit(this.rowData);
    }
    onAdvancementEdit() {
        this.advancementedit.emit(this.rowData);
    }
    onAdvancementDelete() {
        this.advancementdelete.emit(this.rowData);
    }
} 