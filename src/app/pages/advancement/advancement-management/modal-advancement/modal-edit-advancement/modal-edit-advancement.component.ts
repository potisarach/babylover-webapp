import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { GlobalHttpService } from '../../../../../shares/global-http.service';
import { DialogBoxService } from '../../../../../shares/dialog-box/dialog-box.service';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
  selector: 'ngx-modal-edit-advancement',
  templateUrl: './modal-edit-advancement.component.html',
  styleUrls: ['./modal-edit-advancement.component.scss']
})
export class ModalEditAdvancementComponent implements OnInit {
public checkInput:boolean = false;
public selectedFile: File = null;
imagePreView: string = '';
public chkYears: boolean= false;
public isCheck: boolean = false;
  model = {
    advancementid: "",
    orphanid: "",
    pastchanges: "",
    eat: "",
    sleep: "",
    excretion: "",
    characters: "",
    updatedate : "",
    weight : "",
    height : "",
    aroundhead : "",
    chest : "",
    picturepath : "",
    }
  form: FormGroup;
  constructor(private activeModal: NgbActiveModal,private apiHttp: GlobalHttpService,private dialog: DialogBoxService,    private loading: Ng4LoadingSpinnerService
) { 
    this.form = new FormGroup({
      'pastchanges': new FormControl('', [Validators.required, Validators.maxLength(500), this.isEmpty]),
      'eat': new FormControl('', [Validators.required, Validators.maxLength(500), this.isEmpty]),
      'sleep': new FormControl('', [Validators.required, Validators.maxLength(500), this.isEmpty]),
      'excretion': new FormControl('', [Validators.required, Validators.maxLength(500), this.isEmpty]),
      'characters': new FormControl('', [Validators.required, Validators.maxLength(500), this.isEmpty]),
      'weight' : new FormControl('',[Validators.required, Validators.maxLength(3), this.isEmpty]),
      'height' : new FormControl('',[Validators.required, Validators.maxLength(3), this.isEmpty]),
      'aroundhead' : new FormControl('',[Validators.required, Validators.maxLength(3), this.isEmpty]),
      'chest' : new FormControl('',[Validators.required, Validators.maxLength(3), this.isEmpty]),
      'picturepath' : new FormControl(null)
    });
  }

  ngOnInit() {
  }
  isError(control) {
    if (control.invalid && control.touched || this.isCheck) {
      return control.errors || false;
    }
    return false;
  }

  public isEmpty(control: FormControl) {
    const isWhitespace = (control.value || '').trim().length === 0;
    const isValid = !isWhitespace;
    return isValid ? null : { 'whitespace': true };
  }
  async onFileSelected(event, input) {
    this.onConvertImage(input)
    this.selectedFile = await <File>event.target.files[0];

  }
  onConvertImage(input: HTMLInputElement) {
    if (input.files.length == 0) return this.imagePreView = '';
    const reader = new FileReader();
    reader.readAsDataURL(input.files[0]);
    reader.addEventListener('load', () => {

      this.imagePreView = reader.result.toString();
      console.log(this.model);

    })
  }

  async onSave(){
    this.model.picturepath = this.imagePreView;
    console.log(this.model);
    let datecurrent;
    datecurrent = new Date().toISOString().slice(0,10); 
    this.model.updatedate = datecurrent;
    if (this.form.invalid){
      this.dialog.error({ title: 'แจ้งเตือน', message: "กรุณากรอกข้อมูลให้สมบูรณ์"})
    } else {
      let result: any = await this.apiHttp.post('api/advancement/put', this.model);
      if (result.success) {
        this.activeModal.close({ success: true });
        this.dialog.alert({ title: 'แจ้งเตือน', message: "บันทึกข้อมูลสำเร็จ" });
      } else {
        this.dialog.error({ title: 'แจ้งเตือน', message: "ไม่สามารถบันทึกข้อมูลได้" });
      }  
    }
    this.loading.hide();

  }
  async chkYear(){
    let years;
    years = new Date().toISOString().slice(0,4);
    let orphanid = this.model.orphanid; 
    let results = await this.apiHttp.post('api/advancement/countyear',{orphanid , years})
    if(results.body[0].Years > 4){
        this.chkYears = true;
    }else{
        this.chkYears = false;
    }
    

  }
  CheckInput($event){
    console.log("Event > > >",$event);
    
    if($event.key){
      this.checkInput = true;
        }
}
  closeMod() {
    if(this.checkInput == true){
      this.dialog.confirm({ title: 'ยืนยัน', message: 'ต้องการยกเลิกใช่หรือไม่' }, (ans) => {
        if (ans) {
            this.activeModal.close();
        }
    });
    }else{
      this.activeModal.close();
    }
  }

  close(){
    this.dialog.confirm({ title: 'ยืนยัน', message: 'ต้องการยกเลิกใช่หรือไม่' }, (ans) => {
      if (ans) {
          this.activeModal.close();
      }
  });  
  }

  chkChar($event){
    let charCode = $event.which || $event.keyCode;

    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    } else {
      return true;
    }

  }
}
