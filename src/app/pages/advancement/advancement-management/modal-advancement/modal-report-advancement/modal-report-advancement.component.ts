import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { GlobalHttpService } from '../../../../../shares/global-http.service';
import { DialogBoxService } from '../../../../../shares/dialog-box/dialog-box.service';

@Component({
  selector: 'ngx-modal-report-advancement',
  templateUrl: './modal-report-advancement.component.html',
  styleUrls: ['./modal-report-advancement.component.scss']
})
export class ModalReportAdvancementComponent implements OnInit {
  public selectedFile: File = null;
  imagePreView: string = '';
  public chkYears: boolean = false;
  public isCheck: boolean = false;
  model = {
    orphanid: "",
    pastchanges: "",
    eat: "",
    sleep: "",
    excretion: "",
    characters: "",
    updatedate: "",
    weight: "",
    height: "",
    aroundhead: "",
    chest: "",
    picturepath: "",
    orphanadvancementid: ""
  }
  form: FormGroup;
  constructor(private activeModal: NgbActiveModal, private apiHttp: GlobalHttpService, private dialog: DialogBoxService) {
    this.form = new FormGroup({
      'pastchanges': new FormControl('', [Validators.required, Validators.maxLength(500), this.isEmpty]),
      'eat': new FormControl('', [Validators.required, Validators.maxLength(500), this.isEmpty]),
      'sleep': new FormControl('', [Validators.required, Validators.maxLength(500), this.isEmpty]),
      'excretion': new FormControl('', [Validators.required, Validators.maxLength(500), this.isEmpty]),
      'characters': new FormControl('', [Validators.required, Validators.maxLength(500), this.isEmpty]),
      'weight': new FormControl('', [Validators.required, Validators.maxLength(3), this.isEmpty]),
      'height': new FormControl('', [Validators.required, Validators.maxLength(3), this.isEmpty]),
      'aroundhead': new FormControl('', [Validators.required, Validators.maxLength(3), this.isEmpty]),
      'chest': new FormControl('', [Validators.required, Validators.maxLength(3), this.isEmpty]),
      'picturepath': new FormControl(null)
    });
  }
  isError(control) {
    if (control.invalid && control.touched || this.isCheck) {
      return control.errors || false;
    }
    return false;
  }

  public isEmpty(control: FormControl) {
    const isWhitespace = (control.value || '').trim().length === 0;
    const isValid = !isWhitespace;
    return isValid ? null : { 'whitespace': true };
  }
  async onFileSelected(event, input) {
    this.onConvertImage(input)
    this.selectedFile = await <File>event.target.files[0];
  }
  onConvertImage(input: HTMLInputElement) {
    if (input.files.length == 0) return this.imagePreView = '';
    const reader = new FileReader();
    reader.readAsDataURL(input.files[0]);
    reader.addEventListener('load', () => {

      this.imagePreView = reader.result.toString();
      console.log(this.model);

    })
  }

  async onSave() {
    this.model.picturepath = this.imagePreView;
    console.log(this.model);
    let datecurrent;
    datecurrent = new Date().toISOString().slice(0, 10);
    this.model.updatedate = datecurrent;
    if (this.form.invalid) {
      this.dialog.error({ title: 'แจ้งเตือน', message: "กรุณากรอกข้อมูลให้สมบูรณ์" })
    } else {
      this.model.orphanadvancementid = ""; 
      // เมื่อดึงข้อมูลจากข้อมูลที่มีใน db แล้วต้องการเพิ่มเติมข้อมูล ต้อง clear ค่า orphanadvancementid เพื่อเพิ่มข้อมูลความก้าวหน้าของเด็กใหม่
      let result: any = await this.apiHttp.post('api/advancement/put', this.model);
      if (result.success) {
        this.activeModal.close({ success: true });
        this.dialog.alert({ title: 'แจ้งเตือน', message: "บันทึกข้อมูลสำเร็จ" });
      } else {
        this.dialog.error({ title: 'แจ้งเตือน', message: "ไม่สามารถบันทึกข้อมูลได้" });
      }
    }

  }
  async chkYear() {
    let years;
    years = new Date().toISOString().slice(0, 4);
    let orphanid = this.model.orphanid;
    let results = await this.apiHttp.post('api/advancement/countyear', { orphanid, years })
    if (results.body[0].Years > 4) {
      this.chkYears = true;
    } else {
      this.chkYears = false;
    }


  }
  close() {
    this.dialog.confirm({ title: 'ยืนยัน', message: 'ต้องการยกเลิกใช่หรือไม่' }, (ans) => {
      if (ans) {
        this.activeModal.close();
      }
    });
  }
  ngOnInit() {
    this.chkYear();
    // this.setClear();
    console.log("orphanid >>> ", this.model.orphanid);
    this.showGet()

  }
  setClear() {
    this.model.pastchanges = "";
    this.model.eat = "";
    this.model.sleep = "";
    this.model.excretion = "";
    this.model.characters = "";
    this.model.updatedate = "";
    this.model.weight = "";
    this.model.height = "";
    this.model.aroundhead = "";
    this.model.chest = "";
    this.model.picturepath = "";
  }
  chkChar($event) {
    let charCode = $event.which || $event.keyCode;

    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    } else {
      return true;
    }

  }
  
  async showGet() {
    let orphanid = this.model.orphanid
    let results = await this.apiHttp.post('api/advancement/getguide', { orphanid })
    if (results.success) {
      // เแสดงข้อมูลล่าสุดของเด็ก

      console.log("Get Guide Success");
      console.log("results > > ", results);

      if (results.body[1][0]) {
        // console.log("results. > > ", results.body[1][0].orphanid);

        this.model = results.body[1][0]

      }


    }
  }


}
