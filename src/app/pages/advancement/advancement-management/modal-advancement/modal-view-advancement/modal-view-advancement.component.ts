import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { GlobalHttpService } from '../../../../../shares/global-http.service';
import { DialogBoxService } from '../../../../../shares/dialog-box/dialog-box.service';

@Component({
  selector: 'ngx-modal-view-advancement',
  templateUrl: './modal-view-advancement.component.html',
  styleUrls: ['./modal-view-advancement.component.scss']
})
export class ModalViewAdvancementComponent implements OnInit {
    public isCheck: boolean = false;
    public imagePreView = "";
    model = {
      pastchanges: "",
      eat: "",
      sleep: "",
      excretion: "",
      characters: "",
      picturepath: "",
      weight: "",
      height: "",
      fullnameorphan: "",
      nicknameorphan: "",
      birthdate: "",
      ageorphan: "",
      aroundhead: "",
      chest: "",
      fullnamepatron: "",
      joinfamilydate: "",
      }
    form: FormGroup;
      constructor(private activeModal: NgbActiveModal,private apiHttp: GlobalHttpService,private dialog: DialogBoxService) { 
        this.form = new FormGroup({
          'pastchanges': new FormControl('', [Validators.required]),
          'eat': new FormControl('', [Validators.required]),
          'sleep': new FormControl('', [Validators.required]),
          'excretion': new FormControl('', [Validators.required]),
          'characters': new FormControl('', [Validators.required])
        });
      }
      close(){
              this.activeModal.close();
      }
      ngOnInit() {  
        console.log(this.model);
              
      }
    }
    