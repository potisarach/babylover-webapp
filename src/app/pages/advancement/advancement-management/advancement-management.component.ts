import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { Router } from '@angular/router';
import { GlobalHttpService } from '../../../shares/global-http.service';
import { GlobalFunctionService } from '../../../shares/global-function.service';
import { DialogBoxService } from '../../../shares/dialog-box/dialog-box.service';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { LocalDataSource, ViewCell } from 'ng2-smart-table';
import { GlobalApiService } from '../../../shares/global-api.service';
import { ModalAdvancementComponent } from './modal-advancement/modal-advancement.component';
import { ModalReportAdvancementComponent } from './modal-advancement/modal-report-advancement/modal-report-advancement.component';
import { Row } from 'ng2-smart-table/lib/data-set/row';

declare var $: any;  

@Component({
  selector: 'ngx-advancement-management',
  templateUrl: './advancement-management.component.html',
  styleUrls: ['./advancement-management.component.scss']
})
export class AdvancementManagementComponent implements OnInit {

  public orphanstatus: Array<any>;

  public model = {
    fullname: "",
    orphanpersonid: "",
    orphanid: "",
    patronageid: "",
    patronid: "",
    firstname: "",
    lastname: "",
    birthdate: "",
    nickname: "",
    birthdateorphan: "",
    nicknameorphan: "",
    picturepath: "",
    fullnameorphan: "",
    fullnamepatron: "",
    agepatron: "",
    ageorphan: "",
    joinfamilydate: "",
    agestartorphan: "",
    ageendorphan: "",
  }

  public datasource: LocalDataSource;
  public tbSettings: any;
  public datas = [];

  constructor(private modalService: NgbModal, public func: GlobalFunctionService, private loading: Ng4LoadingSpinnerService, private http: GlobalHttpService, private dropdownApi: GlobalApiService) {
    this.datasource = new LocalDataSource();
    this.setColumns();
  }

  ngOnInit() {
    this.loading.show();
    this.onSearchOrphan();
    this.onReset();
    this.setupDropdown();
    this.loading.hide();
  }
  async onReset() {
    this.loading.show();
    this.model.ageendorphan = "";
    this.model.agestartorphan = "";
    this.model.fullname = "";
    this.onSearchOrphan();
    this.loading.hide();
  }

  async setupDropdown() {
    this.loading.show();
    this.orphanstatus = await this.dropdownApi.getOrphanstatusList();
    this.loading.hide();
  }

  setColumns() {
    let _this = this;
    this.tbSettings = this.func.getTableSetting({
      fullname: {
        title: 'ชื่อ - นามสกุล',
        width: '35%',
        type: 'string'
      },
      age: {
        title: 'อายุ (ปี.เดือน)',
        width: '35%',
        type: 'html',
        valuePrepareFunction: (cell, row) => {
          return '<div class="text-center">' + row.age + '</div>';
        }
      },
      advancement: {
        title: 'จัดการ',
        width: '30%',
        sort: false,
        type: 'custom',
        renderComponent: OrphanFormTbButtonViewComponent,
        onComponentInitFunction(instance) {
          instance.advancement.subscribe(row => {
            console.log(row);
            _this.onAdvancement(row);
          });
          instance.insert.subscribe(row => {
            console.log(row);
            _this.onAdvancementReport(row);
          });
        }
      }
    });
  }

  // ค้นหาเด็ก
  async onSearchOrphan() {
    this.loading.show();
    let fullname = this.model.fullname.trim();
    let result = await this.http.post('api/orphan/searchadvancement', { fullname: fullname, agestartorphan: this.model.agestartorphan, ageendorphan: this.model.ageendorphan});
    if (result.success) {
      this.datasource = this.func.getTableDataSource(result.body);
    }
    this.loading.hide();
  }

  onEnter($event) {
    var keyCode = $event.which || $event.keyCode;
    if (keyCode === 13) {
      this.onSearchOrphan();
    }
  }

  // เช็คการพิมพ์ตัวอักษร (พิมพ์ได้เฉพาะตัวเลข)
  chkAge($event) {
    let charCode = $event.which || $event.keyCode;

    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    } else {
      return true;
    }
  }

  // เช็คการพิมพ์ตัวเลข(พิมพ์ได้เฉพาะตัวอักษร)
  chkName($event) {
    let charCode = $event.which || $event.keyCode;
    console.log(charCode);

    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return true;
    } else {
      return false;
    }
  }
  async onAdvancement(rowData) {
    this.loading.show();
    let res = await this.http.post('api/orphan/getorphanadvance', { orphanid: rowData.orphanid });
    if(res.success){
      const activeModal = this.modalService.open(ModalAdvancementComponent, { size: 'lg', keyboard: false, backdrop: "static", container: 'nb-layout' });
      activeModal.componentInstance.model = res.body;
  }
    this.loading.hide();
  }

  async onAdvancementReport(rowData) {
    this.loading.show();
    let res = await this.http.post('api/orphan/getorphanadvance', { orphanid: rowData.orphanid });
    if(res.success){
      const activeModal = this.modalService.open(ModalReportAdvancementComponent, { size: 'lg', keyboard: false, backdrop: "static", container: 'nb-layout'});
      activeModal.componentInstance.model = res.body;
    }
    this.loading.hide();
    }
  }




@Component({
  selector: 'tb-advancement-list-button-view',
  template: `<div class="text-center">
    <a (click)="onAdvancement()" class="button-view" title="รายงานความก้าวหน้า"><i class="fa fa-vcard"></i></a> &nbsp;
    <a (click)="onAdvancementReport()" class="button-view" title="เพิ่มรายงานความก้าวหน้า"><i class="fas fa-clipboard-list"></i></a>
    </div>
  `,
  styleUrls: ['./advancement-management.component.scss']
})
export class OrphanFormTbButtonViewComponent implements ViewCell, OnInit {

  renderValue: string;

  constructor(private modalService: NgbModal) { }

  @Input() value: string | number;
  @Input() rowData: any;
  @Output() advancement: EventEmitter<any> = new EventEmitter();
  @Output() insert: EventEmitter<any> = new EventEmitter();

  ngOnInit() {
    this.renderValue = this.value.toString().toUpperCase();
  }

  onAdvancement() {
    this.advancement.emit(this.rowData);
}
 onAdvancementReport() {
   this.insert.emit(this.rowData);
 }

}
