import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdvancementManagementComponent } from './advancement-management/advancement-management.component';

const routes: Routes = [
  {
    path: 'management',
    component: AdvancementManagementComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdvancementRoutingModule { }
