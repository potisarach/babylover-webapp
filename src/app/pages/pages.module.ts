import { NgModule } from '@angular/core';
import { PagesComponent } from './pages.component';
import { DashboardModule } from './dashboard/dashboard.module';
import { PagesRoutingModule } from './pages-routing.module';
import { ThemeModule } from '../@theme/theme.module';
import { MiscellaneousModule } from './miscellaneous/miscellaneous.module';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { DevelopmentsComponent } from './developments/developments.component';
import { Ng4LoadingSpinnerModule } from 'ng4-loading-spinner';
import { CKEditorModule } from 'ng2-ckeditor';
import { MemberComponent, UserFormTbButtonViewComponent } from './member/member.component';
import { ModalAddUserComponent } from './member/modal-add-user/modal-add-user.component';
import { ModalEditUserComponent } from './member/modal-edit-user/modal-edit-user.component';
import { ModalDetailUserComponent } from './member/modal-detail-user/modal-detail-user.component';



const PAGES_COMPONENTS = [
  PagesComponent,
];

@NgModule({
  imports: [
    PagesRoutingModule,
    ThemeModule,
    DashboardModule,
    MiscellaneousModule,
    Ng2SmartTableModule,
    Ng4LoadingSpinnerModule.forRoot(),
    CKEditorModule
    
  ],
  declarations: [
    ...PAGES_COMPONENTS,
    DevelopmentsComponent,
    MemberComponent,
    UserFormTbButtonViewComponent,
    ModalAddUserComponent,
    ModalEditUserComponent,
    ModalDetailUserComponent, 
    ModalDetailUserComponent
 
  ],
  entryComponents:[
    MemberComponent,
    UserFormTbButtonViewComponent,
    ModalAddUserComponent,
    ModalEditUserComponent,
    ModalDetailUserComponent
    
  ]
})
export class PagesModule {
}
