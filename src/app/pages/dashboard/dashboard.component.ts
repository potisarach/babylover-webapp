import { Component, OnDestroy } from '@angular/core';
import { NbThemeService } from '@nebular/theme';
import { takeWhile } from 'rxjs/operators/takeWhile';
import { Http, RequestOptions, Headers } from '@angular/http';
import * as _conf from "../../global.conf";
import { Router } from '@angular/router';
import { GlobalApiService } from '../../shares/global-api.service';
import { GlobalFunctionService } from '../../shares/global-function.service';
import { DialogBoxService } from '../../shares/dialog-box/dialog-box.service';
import { AuthenticationService } from '../../shares/authentication.service';

interface CardSettings {
  title: string;
  iconClass: string;
  type: string;
}

@Component({
  selector: 'ngx-dashboard',
  styleUrls: ['./dashboard.component.scss'],
  templateUrl: './dashboard.component.html',
})
export class DashboardComponent implements OnDestroy {

  private alive = true;
  public provinces: Array<any> = new Array<any>();
  public amphurs: Array<any> = new Array<any>()
  public tumbols: Array<any> = new Array<any>()
  public model = { provincecode: '', amphurcode: '', tumbolcode: '' }
  lightCard: CardSettings = {
    title: 'Light',
    iconClass: 'nb-lightbulb',
    type: 'primary',
  };
  rollerShadesCard: CardSettings = {
    title: 'Roller Shades',
    iconClass: 'nb-roller-shades',
    type: 'success',
  };
  wirelessAudioCard: CardSettings = {
    title: 'Wireless Audio',
    iconClass: 'nb-audio',
    type: 'info',
  };
  coffeeMakerCard: CardSettings = {
    title: 'Coffee Maker',
    iconClass: 'nb-coffee-maker',
    type: 'warning',
  };

  statusCards: string;

  commonStatusCardsSet: CardSettings[] = [
    this.lightCard,
    this.rollerShadesCard,
    this.wirelessAudioCard,
    this.coffeeMakerCard,
  ];

  statusCardsByThemes: {
    default: CardSettings[];
    cosmic: CardSettings[];
    corporate: CardSettings[];
  } = {
      default: this.commonStatusCardsSet,
      cosmic: this.commonStatusCardsSet,
      corporate: [
        {
          ...this.lightCard,
          type: 'warning',
        },
        {
          ...this.rollerShadesCard,
          type: 'primary',
        },
        {
          ...this.wirelessAudioCard,
          type: 'danger',
        },
        {
          ...this.coffeeMakerCard,
          type: 'secondary',
        },
      ],
    };

  constructor(private dialog: DialogBoxService, private func: GlobalFunctionService, private api: GlobalApiService, private themeService: NbThemeService, public http: Http, private route: Router,private authenticationService: AuthenticationService,private router: Router) {

    this.themeService.getJsTheme()
      .pipe(takeWhile(() => this.alive))
      .subscribe(theme => {
        this.statusCards = this.statusCardsByThemes[theme.name];
      });
  }

  ngOnInit() {
    this.getProvinceList();
  }
  ngOnDestroy() {
    this.alive = false;
  }
  logout(){
    this.authenticationService.logout();
    this.router.navigate(["/authen/login"]);
    console.log('ลงชื่อออก');
    
  }

  async getProvinceList() {
    this.model.amphurcode = '';
    this.model.tumbolcode = '';
    let result = await this.api.getProvinceList();
    if (result.error) {

    } else {
      this.provinces = result;
    }
    console.log(this.provinces);
  }

  async getAmphurList() {
    this.model.amphurcode = '';
    this.model.tumbolcode = '';
    this.amphurs = [];
    this.tumbols = [];
    if (!this.model.provincecode) {
      return;
    }
    console.log(this.model);
    let result = await this.api.getAmphurList(this.model.provincecode);
    if (result.error) {

    } else {
      this.amphurs = result;
    }
    console.log(this.amphurs);
  }
  async getTumbolList() {
    this.tumbols = [];
    if (!this.model.amphurcode) {
      return;
    }
    let result = await this.api.getTumbolList(this.model.amphurcode);
    if (result.error) {

    } else {
      this.tumbols = result;
    }
    console.log(this.tumbols);
  }

  goto() {
    let name = "tar";
    this.route.navigate(['/pages/sponsor/participant/form/' + name]);
  }

  confirm() {
    this.dialog.confirm({ message: 'confirm' }, (r) => {
      console.log(r);
    });
  }
  alert() {
    this.dialog.alert({ message: 'Alert' }, () => {
      console.log('alert');
    });
  }
}
