import { Routes, RouterModule } from "@angular/router";
import { NgModule } from "@angular/core";
import { CommunitydataFormComponent } from "./communitydata-form/communitydata-form.component";

const routes: Routes = [
    {
        path: 'form',
        component: CommunitydataFormComponent
    }
]
@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
  })
  export class CommunitydataRoutingModule {
  }