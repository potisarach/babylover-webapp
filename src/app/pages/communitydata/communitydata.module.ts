import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CommunitydataFormComponent } from './communitydata-form/communitydata-form.component';
import { CommunitydataRoutingModule } from './communitydata-routing.module';
import { CommunitydataForm1Component }  from './communitydata-form/communitydata-form1/communitydata-form1.component';
import { CommunitydataForm2Component }  from './communitydata-form/communitydata-form2/communitydata-form2.component';
import { CommunitydataForm3Component }  from './communitydata-form/communitydata-form3/communitydata-form3.component';
import { CommunitydataForm4Component }  from './communitydata-form/communitydata-form4/communitydata-form4.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    CommunitydataRoutingModule,
    ReactiveFormsModule,
    FormsModule,
  ],
  declarations: [
    CommunitydataFormComponent,
    CommunitydataForm1Component,
    CommunitydataForm2Component,
    CommunitydataForm3Component,
    CommunitydataForm4Component
   
  ],
  entryComponents:[
    CommunitydataFormComponent,
    
  ]
})
export class CommunitydataModule { }
