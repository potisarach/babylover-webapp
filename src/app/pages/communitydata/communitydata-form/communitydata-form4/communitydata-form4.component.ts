import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'ngx-communitydata-form4',
  templateUrl: './communitydata-form4.component.html',
  styleUrls: ['./communitydata-form4.component.scss']
})
export class CommunitydataForm4Component implements OnInit {
  public form:FormGroup ;
  public index: number;
  public model:Array<any> = [];

  constructor(private activeModal: NgbActiveModal) { }

  ngOnInit() {
  }
  closeModal() {
    var press = confirm("คุณต้องการยกเลิกหรือไม่ ?");
    if (press == true) {
      this.activeModal.close();
    }
  }
  pageAction(action) {

    if (this.form.invalid) {
      alert("กรุณากรอกข้อมูลให้ครบ !")
    } else {
      let data = {
        success: this.form.valid,
        page: 'next',
        index: this.index,
        model: this.model
      };
      this.activeModal.close(data);
    }
  }

}
