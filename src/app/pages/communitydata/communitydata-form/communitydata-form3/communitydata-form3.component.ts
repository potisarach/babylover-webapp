import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'ngx-communitydata-form3',
  templateUrl: './communitydata-form3.component.html',
  styleUrls: ['./communitydata-form3.component.scss']
})
export class CommunitydataForm3Component implements OnInit {
  public headerTitle:string;
  public index:number;
  public model = {
    street:"",
    home_electricity:"",
    water_source:"",
    school:"",
    temple:"",
    hospital:"",
    public_places:"",
    factory:""
  }

  form:FormGroup;
  constructor(private activeModal: NgbActiveModal) {
    this.form = new FormGroup({
      'street' : new FormControl('',[Validators.required]),
      'home_electricity' : new FormControl('',[Validators.required]),
      'water_source' : new FormControl('',[Validators.required]),
      'school' : new FormControl('',[Validators.required]),
      'temple' : new FormControl('',[Validators.required]),
      'hospital' : new FormControl('',[Validators.required]),
      'public_places' : new FormControl('',[Validators.required]),
      'factory' : new FormControl('',[Validators.required]),
      
    })
   }


  isError(control){
    if(control.invalid && control.touched){
      // return errorKey in control.errors
    }
      return false ;
  }
  
  closeModal() {
    var press = confirm("คุณต้องการยกเลิกหรือไม่ ?");
    if (press == true) {
      this.activeModal.close();
    }
  }
  nextModal() {
    if (this.form.invalid) {
      alert("กรุณากรอกข้อมูลให้ครบ !")
    } else {
      let data = {
        success: this.form.valid,
        page: 'next',
        index: this.index,
        model: this.model
      };
      this.activeModal.close(data);
    }
  }
  prevModal() {
    let data = {
      success: this.form.valid,
      page: 'prev',
      index: this.index,
      model: this.model
    };
    this.activeModal.close(data);
  }


  ngOnInit() {
  }

}