import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { DialogBoxService } from '../../../shares/dialog-box/dialog-box.service';
import { CommunitydataForm1Component } from './communitydata-form1/communitydata-form1.component';
import { CommunitydataForm2Component } from './communitydata-form2/communitydata-form2.component';
import { CommunitydataForm3Component } from './communitydata-form3/communitydata-form3.component';
import { CommunitydataForm4Component } from './communitydata-form4/communitydata-form4.component';

@Component({
  selector: 'ngx-communitydata-form',
  templateUrl: './communitydata-form.component.html',
  styleUrls: ['./communitydata-form.component.scss']
})
export class CommunitydataFormComponent implements OnInit {

  public modalComponents: any;
  constructor(private modalService: NgbModal, private dialog: DialogBoxService) {
    this.modalComponents = [
      { component: CommunitydataForm1Component, title: 'ข้อมูลพื้นฐานหมู่บ้าน/ชุมชน'},
      { component: CommunitydataForm2Component, title: 'โครงสร้างและประชากร'},
      { component: CommunitydataForm3Component, title: 'ระบบสาธารณูปโภค'},
      { component: CommunitydataForm4Component, title: 'สภาพเศรษฐกิจ สังคม'}
    ];
  }

  ngOnInit() {
  }

  onShowForms(oldIndex:number){
    let modal = this.modalComponents[oldIndex];
    const activeModal = this.modalService.open(modal.component, { size: 'lg', container: 'nb-layout',backdrop: 'static' });
    activeModal.componentInstance.headerTitle = (oldIndex+1)+'. ' + modal.title;
    activeModal.componentInstance.index = oldIndex;
    if(modal.model){
      activeModal.componentInstance.model = modal.model;
    }
    activeModal.result.then((result)=>{
      console.log(result);
      if(!result){
        return;
      }
      if(result.page == 'next'){
        modal.model = result.model;
        modal.success = result.success;
        this.onShowForms(result.index+1);
      }else if(result.page == 'prev'){
        modal.model = result.model;
        this.onShowForms(result.index-1)
      }
    }) 
  }

  isCompleted(i){
    return this.modalComponents[i]['success'];
  }

  isEnable(){}

  cssCompleted(i){
    if(this.isCompleted(i))
      return 'status-completed';
    else
      return 'status-uncomplete';
  }

  onDownloademptyform(){
    console.log('onDownloademptyform');
  }

  onViewreport(){
    console.log('onViewreport');
  }

  onSave(){
    console.log('modalComponents', this.modalComponents)
    for(let item of this.modalComponents){
      if(!item.success){
        console.log(item);
        this.dialog.error({title: 'โปรดตรวจสอบข้อมูล', message: 'กรุณาใส่ข้อมูลให้ครบถ่วนก่อนการบันทึก'});
        return;
      }
    }
    this.dialog.confirm({message: 'คุณต้องการเพิ่มข้อมูลชุมชนใช่หรือไม่'}, (isConfirm)=>{
      console.log(isConfirm);
      if(isConfirm){
        // TODO CALL API
      }
    });
  }
}
