import { Component, OnInit, ViewChild } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup } from '@angular/forms';


@Component({
  selector: 'ngx-communitydata-form1',
  templateUrl: './communitydata-form1.component.html',
  styleUrls: ['./communitydata-form1.component.scss']
})
export class CommunitydataForm1Component implements OnInit {

  constructor(private activeModal: NgbActiveModal) {}

  ngOnInit() {
  }
 
  closeModal() {
    var press = confirm("คุณต้องการยกเลิกหรือไม่ ?");
    if (press == true) {
      this.activeModal.close();
    }
  }
}
