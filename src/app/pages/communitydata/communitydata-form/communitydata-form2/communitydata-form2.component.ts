import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'ngx-communitydata-form2',
  templateUrl: './communitydata-form2.component.html',
  styleUrls: ['./communitydata-form2.component.scss']
})
export class CommunitydataForm2Component implements OnInit {
  public form:FormGroup ;
  public index: number;
  public model:Array<any> = [];
  public modalEducation: any;
  modalAge:any;
  modalGroup:any;
  public memberCount: number;

  constructor(private activeModal: NgbActiveModal) { 
    this.modalEducation = [
      {title: 'ไม่ได้รับการศึกษา'},
      {title: 'ข้อเท็จจริงเกี่ยวกับสามีภรรยาของผู้ขออุปการะเด็ก'},
      {title: 'จบชั้น ป.4-6'},
      {title: 'จบชั้นมัธยมศึกษาตอนต้น'},
      {title: 'จบชั้นมัธยมศึกษาตอนปลาย/จบ ปวช.'},
      {title: 'อนุปริญญา/ปวส'},
      {title: 'จบปริญญาตรี'},
      {title: 'จบปริญญาโท'},
      {title: 'อื่นๆ'}
    ];

    this.modalAge = [
      {title: 'แรกเกิด-15 ปี'},
      {title: '16-30 ปี'},
      {title: '31-45 ปี'},
      {title: '46-60 ปี'},
      {title: 'มากกว่า 60 ปี'}
    ];

    this.modalGroup = [
      {title: 'ผู้นาชุมชน'},
      {title: 'ผู้นาเกษตร'},
      {title: 'สมาชิกกองทุนหมู่บ้าน'},
      {title: 'สมาชิก ทสม.(อาสาสมัครพิทักษ์ทรัพยากรธรรมชาติและสิ่งแวดล้อมหมู่บ้าน)'},
      {title: 'สมาชิก รสทป. (ราษฎรอาสาสมัครพิทักษ์ป่า)'},
      {title: 'สมาชิกกลุ่มสหกรณ์ออมทรัพย์'},
      {title: 'สมาชิกกลุ่มสตรี/แม่บ้าน'},
      {title: 'สมาชิกกลุ่มอาสาสมัคร อสม.'},
      {title: 'กลุ่มทอผ้า'},
      {title: 'กลุ่มเกษตรแม่แจ่ม'},
      {title: 'ไม่ได้เป็นสมาชิกกลุ่มใด'},
    ];
  }

  ngOnInit() {
  }
  closeModal() {
    var press = confirm("คุณต้องการยกเลิกหรือไม่ ?");
    if (press == true) {
      this.activeModal.close();
    }
  }
  pageAction(action) {

    if (this.form.invalid) {
      alert("กรุณากรอกข้อมูลให้ครบ !")
    } else {
      let data = {
        success: this.form.valid,
        page: 'next',
        index: this.index,
        model: this.model
      };
      this.activeModal.close(data);
    }
  }

  onMembercountChange() {
    let modelSize = this.model.length;
    let mcount = this.memberCount - modelSize;
    if(mcount < 0){
      let rem = this.model.splice(modelSize + mcount);
      console.log('Remove => ', rem);
    }else{
      for(let i=0; i<mcount; i++){
        // let ix = i + modelSize;
        // this.form.addControl("prefix"+ix, new FormControl('',[Validators.required]));
        // this.form.addControl("firstname"+ix, new FormControl('',[Validators.required]));
        // this.form.addControl("lastname"+ix, new FormControl('',[Validators.required]));
        // this.form.addControl("birthdate"+ix, new FormControl('',[Validators.required]));
        // this.form.addControl("concerned"+ix , new FormControl('',[Validators.required]));
        // this.form.addControl("education"+ix, new FormControl('',[Validators.required]));
        // this.form.addControl("note"+ix, new FormControl('',[Validators.required]));

        let model = {
          prefix:"",
          firstname:"",
          lastname:"",
          birthdate:"",
          concerned:"",
          education:"",
          note:""
        }
        this.model.push(model);
      }
    }
    
    
  }

}
