import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OrphanevaluationComponent, OrphanEvaluationTbButtonViewComponent, OrphanEvaluationTbButtonView2Component, OrphanEvaluationTbButtonView3Component, OrphanEvaluationTbButtonView4Component } from './orphanevaluation.component';
import { OrphanevaluationRoutingModule } from './orphanevaluation-routing.module';
import { Evaluation1Component } from './evaluation1/evaluation1.component';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { FormsModule } from '@angular/forms';
import { Ng4LoadingSpinnerModule } from 'ng4-loading-spinner';
import { HistoryevaluationComponent } from './evaluation1/historyevaluation/historyevaluation.component';


@NgModule({
  imports: [
    CommonModule,
    OrphanevaluationRoutingModule,
    Ng2SmartTableModule,
    FormsModule,
    Ng4LoadingSpinnerModule.forRoot()
  ],
  declarations: [
    OrphanevaluationComponent,
    Evaluation1Component,
    OrphanEvaluationTbButtonViewComponent,
    OrphanEvaluationTbButtonView2Component,
    OrphanEvaluationTbButtonView3Component,
    OrphanEvaluationTbButtonView4Component,
    HistoryevaluationComponent
  ],
  entryComponents:[
    OrphanEvaluationTbButtonViewComponent,
    OrphanEvaluationTbButtonView2Component,
    OrphanEvaluationTbButtonView3Component,
    OrphanEvaluationTbButtonView4Component
  ]
})
export class OrphanevaluationModule { }
