import { Component, OnInit } from '@angular/core';
import { GlobalFunctionService } from '../../../shares/global-function.service';
import { LocalDataSource } from 'ng2-smart-table';
import { GlobalHttpService } from '../../../shares/global-http.service';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { ActivatedRoute, Router } from '@angular/router';
import { isInteger } from '@ng-bootstrap/ng-bootstrap/util/util';
import { DialogBoxService } from '../../../shares/dialog-box/dialog-box.service';
import { subscribeOn, window } from 'rxjs/operators';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'ngx-evaluation1',
  templateUrl: './evaluation1.component.html',
  styleUrls: ['./evaluation1.component.scss']
})
export class Evaluation1Component implements OnInit {
  public tbSettings: any;
  public fullname: any;
  public nickname: any;
  public birthdate: any;
  public ageorphan: any;
  public ageorphanrank: any;
  public orphanstatusname: any;
  public datasource: LocalDataSource;
  public form: FormGroup;
  public isCheck: boolean = false;
  public IsRank1: boolean = false;
  public IsRank2: boolean = false;
  public IsRank3: boolean = false;
  public IsRank4: boolean = false;
  public IsRank5: boolean = false;
  public EachRank1: boolean = false;
  public EachRank2: boolean = false;
  public EachRank3: boolean = false;
  public EachRank4: boolean = false;
  public orphanEval: any = {
    orphan: {
      traimas: "",
      age: ""
    }
  };
  public orphanEvaluation1: any;
  public orphanEvaluation2: any;
  public orphanEvaluation3: any;
  public traimas: any;

  public orphanList: any;
  public title: any;
  public sub = {
    subs: {

    }
  }

  public model = {
    date: "",
    month: "",
    year: ""
  }
  public orphanids;
  public age;
  public DidDo1 = [];
  public DidDo2 = [];
  public DidDo3 = [];
  public ranks = [1.5, 2.5, 3.5, 4.5];
  public eachrank: Array<any> = [];

  constructor(public func: GlobalFunctionService, private http: GlobalHttpService, private loading: Ng4LoadingSpinnerService, private activatedRoute: ActivatedRoute, private route: Router, private dialog: DialogBoxService) {
    this.datasource = new LocalDataSource();
    this.form = new FormGroup({});
  }

  async ngOnInit() {
    let _this = this;
    this.activatedRoute.params.subscribe(params => {
      console.log('activatedRoute');
      let orphanid = params['orphanid'];
      let age = params['age'];
      let traimas = params['traimas'];
      _this.orphanids = params['orphanid'];
      _this.loadDataList(age, orphanid, traimas);
      _this.loadDataListBefore1(age, orphanid, traimas);
      _this.loadDataListBefore2(age, orphanid, traimas);
      _this.loadDataListBefore3(age, orphanid, traimas);
      _this.age = age;
      _this.traimas = traimas;
    });
    // if(this. traimas != 1){
    //   this.activatedRoute.params.subscribe(params => {
    //     console.log('activatedRoute');
    //     let orphanid = params['orphanid'];
    //     let age = params['age'];
    //     let traimas = params['traimas'];
    //     _this.orphanids = params['orphanid'];
    //     _this.loadDataListBefore1(age, orphanid, traimas);
    //     _this.traimas = traimas;
    //   });
    // }
    this.SetRank(this.age);
    this.LoadDataEvalution();
  }

  async onSaveEvalution() {

    this.orphanEval.orphan.traimas = this.traimas;
    let temp = [];
    for (let data of this.orphanEval.evaluations) {
      for (let evaluations of data.subs) {
        if (evaluations.value != null) {
          temp.push(evaluations.value);
        }
      }
    }

    if (temp.length <= 0) {
      this.dialog.alert({ title: 'ผิดพลาด', message: "กรุณากรอกข้อมูล!" });
      return;
    } else {
      this.loading.show();
      this.orphanEval.age = this.ageorphanrank;

      let result = await this.http.post('api/evaluation/add', this.orphanEval);
      this.loading.hide();
      if (!result.error) {
        this.orphanEval = result.body;
        this.dialog.alert({ title: 'แจ้งเตือน', message: "บันทึกข้อมูลสำเร็จ" });
        this.route.navigate(['pages/orphan/evaluation/list']);
      }
      this.loading.hide();

    }
  }

  async loadDataList(age, orphanid, traimas) {
    let params = {
      age: age,
      orphanid: orphanid,
      traimas: traimas
    };
    this.loading.show();
    let result = await this.http.post('api/evaluation/getevaluation', params);
    this.loading.hide();
    if (!result.error) {
      this.orphanEval = result.body;
      this.fullname = this.orphanEval.orphan.fullname;
      this.nickname = this.orphanEval.orphan.nickname;
      this.birthdate = this.orphanEval.orphan.birthdate;
      this.ageorphan = parseInt(age);
      this.ageorphanrank = age;
      this.orphanstatusname = this.orphanEval.orphan.orphanstatusname;

      let Year = new Date().toISOString().slice(0, 4);
      let ThisYear = parseInt(Year) + 543;

      if (age <= 2.0) {
        // this.title = 'การประเมินพัฒนาการสำหรับเด็กอายุ 1-2 ปี' + ' ' + 'ประจำปี' + ' ' + ThisYear + ' ' + 'ครั้งที่ ' + params.traimas;
        this.title = `การประเมินพัฒนาการสำหรับเด็กอายุ 1-2 ปี ประจำปี ${ThisYear} ครั้งที่ ${params.traimas}`
        /* เมื่อก่อนเราใช้ + เพื่อต่อข้อความ แต่สำหรับ ES2015 นั้น Template String ทำให้การต่อข้อความเป็นเรื่องง่ายขึ้น ใช้ `` 
        ครอบข้อความที่จะทำเป็น template string จากนั้นใช้ ${} สำหรับส่วนที่ต้องการแทรกส่วนของ JavaScript */
      } else if (age <= 3.0) {
        this.title = `การประเมินพัฒนาการสำหรับเด็กอายุ 2-3 ปี ประจำปี ${ThisYear} ครั้งที่ ${params.traimas}`
      } else if (age <= 4.0) {
        this.title = `การประเมินพัฒนาการสำหรับเด็กอายุ 3-4 ปี ประจำปี ${ThisYear} ครั้งที่ ${params.traimas}`
      } else if (age <= 5.0) {
        this.title = `การประเมินพัฒนาการสำหรับเด็กอายุ 4-5 ปี ประจำปี ${ThisYear} ครั้งที่ ${params.traimas}`
      } else if (age <= 6.0) {
        this.title = `การประเมินพัฒนาการสำหรับเด็กอายุ 5-6 ปี ประจำปี ${ThisYear} ครั้งที่ ${params.traimas}`
      }
    }
    let fullbirthdate = this.birthdate;
    let year = fullbirthdate.substr(0, 4);
    let month = fullbirthdate.substr(5, 2);
    let day = fullbirthdate.substr(8, 2);

    if (month == "01") {
      this.model.month = "มกราคม";
    } else if (month == "02") {
      this.model.month = "กุมภาพันธ์";
    } else if (month == "03") {
      this.model.month = "มีนาคม";
    } else if (month == "04") {
      this.model.month = "เมษายน";
    } else if (month == "05") {
      this.model.month = "พฤษภาคม";
    } else if (month == "06") {
      this.model.month = "มิถุนายน";
    } else if (month == "07") {
      this.model.month = "กรกฎาคม";
    } else if (month == "08") {
      this.model.month = "สิงหาคม";
    } else if (month == "09") {
      this.model.month = "กันยายน";
    } else if (month == "10") {
      this.model.month = "ตุลาคม";
    } else if (month == "11") {
      this.model.month = "พฤศจิกายน";
    } else if (month == "12") {
      this.model.month = "ธันวาคม";
    }
    let y: any = parseInt(year) + 543;
    this.model.year = y;
    if (day < "10") {
      this.model.date = day.substr(1, 1);
    } else {
      this.model.date = day;
    }
  };

  async loadDataListBefore1(age, orphanid, traimas) {
    let i = 0;
    traimas = 1
    let params = {
      age: age,
      orphanid: orphanid,
      traimas: traimas
    };
    this.loading.show();
    let result = await this.http.post('api/evaluation/getevaluation', params);
    this.loading.hide();
    if (!result.error) {
      this.orphanEvaluation1 = result.body;
    }
    for (let item of this.orphanEvaluation1.evaluations) {
      console.log("Item", item.subs);
      for (let items of item.subs) {
        if (items.value == 1) {
          this.DidDo1[i] = "/"
        } else if (items.value == 0) {
          this.DidDo1[i] = "X"
        }
        else if (items.value == null) {
          this.DidDo1[i] = "-"
        }
        i++;
      }
    }

  }
  async loadDataListBefore2(age, orphanid, traimas) {
    let i = 0
    traimas = 2
    let params = {
      age: age,
      orphanid: orphanid,
      traimas: traimas
    };
    this.loading.show();
    let result = await this.http.post('api/evaluation/getevaluation', params);
    this.loading.hide();
    if (!result.error) {
      this.orphanEvaluation2 = result.body;
    }
    for (let item of this.orphanEvaluation2.evaluations) {
      for (let items of item.subs) {
        if (items.value == 1) {
          this.DidDo2[i] = "/"
        } else if (items.value == 0) {
          this.DidDo2[i] = "X"
        }
        else if (items.value == null) {
          this.DidDo2[i] = "-"
        }
        i++;
      }
    }

  }
  async loadDataListBefore3(age, orphanid, traimas) {
    let i = 0
    traimas = 3
    let params = {
      age: age,
      orphanid: orphanid,
      traimas: traimas
    };
    this.loading.show();
    let result = await this.http.post('api/evaluation/getevaluation', params);
    this.loading.hide();
    if (!result.error) {
      this.orphanEvaluation3 = result.body;
    }
    for (let item of this.orphanEvaluation3.evaluations) {
      for (let items of item.subs) {
        if (items.value == 1) {
          this.DidDo3[i] = "/"
        } else if (items.value == 0) {
          this.DidDo3[i] = "X"
        }
        else if (items.value == null) {
          this.DidDo3[i] = "-"
        }
        i++;
      }
    }


  }
  onCancle() {
    this.dialog.confirm({ title: 'ยืนยัน', message: 'ต้องการยกเลิกใช่หรือไม่' }, (ans) => {
      if (ans) {
        this.route.navigate(['/pages/orphan/evaluation/list']);
      }
    });
  }

  checkCando(sub) {
    let value;
    for (let item of sub.subs) {
      value = item.value;
      if (value == null || value == 0) {
        item.value = 1;
      } else {
        item.value = 1;
      }

    }
  }
  checkCannotdo(sub) {
    let value;
    for (let item of sub.subs) {
      value = item.value;
      if (value == null || value == 1) {
        item.value = 0;
      } else {
        item.value = 0;
      }
    }
  }

  onResetEvalution() {
    for (let csub of this.orphanEval.evaluations) {
      for (let sub of csub.subs) {
        sub.value = null;
      }
    }
  }

  ChangCanDo(sub) {
    if (sub.value == 1 || sub.value == null) {
      sub.value = null;
    }
  }
  ChangNotCanDo(sub) {
    if (sub.value == 0 || sub.value == null) {
      sub.value = null;
    }
  }

  async LoadDataEvalution() {
    console.log("this.ranks.length",this.ranks.length);
    
    for (let i = 0; i < 4; i++) {
      let params = {
        age: this.ranks[i],
        orphanid: this.orphanids,
        traimas: this.traimas
      }
      this.eachrank[i] = await this.http.post('api/evaluation/getevaluation', params);      
      
    }
    if (this.eachrank[0].body.evaluations[0].createdate == null) {
      this.EachRank1 = true;
    }
    if (this.eachrank[1].body.evaluations[0].createdate == null) {
      this.EachRank2 = true;
    }
    if (this.eachrank[2].body.evaluations[0].createdate == null) {
      this.EachRank3 = true;
    }
    if (this.eachrank[3].body.evaluations[0].createdate == null) {
      this.EachRank4 = true;
    }




  }

  changed(event) {
    console.log("event.target.value >>>>> ", event.target.value);
    if (event.target.value == "--กรุณาเลือก--") {
      // console.log("ไม่เปิดหน้าใหม่");

    }
    else if (event.target.value == "1-2") {
      if(this.EachRank1 != true){
        this.route.navigate(['pages/orphan/evaluation/form/history/' + '1.5' + '/' + this.orphanids + '/' + this.traimas]);
      }else{
        this.dialog.alert({ title: 'แจ้งเตือน', message: "ไม่พบข้อมูล!" });        
      }
    }
    else if (event.target.value == "2-3") {
      if(this.EachRank2 != true){
        this.route.navigate(['pages/orphan/evaluation/form/history/' + '2.5' + '/' + this.orphanids + '/' + this.traimas]);
      }else{
        this.dialog.alert({ title: 'แจ้งเตือน', message: "ไม่พบข้อมูล!" });
      }
    }
    else if (event.target.value == "3-4") {
      if(this.EachRank3 != true){
        this.route.navigate(['pages/orphan/evaluation/form/history/' + '3.5' + '/' + this.orphanids + '/' + this.traimas]);
      }else{
        this.dialog.alert({ title: 'แจ้งเตือน', message: "ไม่พบข้อมูล!" });
      }
    }
    else if (event.target.value == "4-5") {
      if(this.EachRank4 != true){
        this.route.navigate(['pages/orphan/evaluation/form/history/' + '4.5' + '/' + this.orphanids + '/' + this.traimas]);
      }else{
        this.dialog.alert({ title: 'แจ้งเตือน', message: "ไม่พบข้อมูล!" });
      }
    }
    else {
      
    }
  }
  SetRank(age) {
    if (age >= 5.1 && age <= 6.0) {
      this.IsRank5 = true;
    } else {
      this.IsRank5 = false;
    }
    if (age >= 4.1 && age <= 5.0) {
      this.IsRank4 = true;
    } else {
      this.IsRank4 = false;
    } if (age >= 3.1 && age <= 4.0) {
      this.IsRank3 = true;
    } else {
      this.IsRank3 = false;

    } if (age >= 2.1 && age <= 3.0) {
      this.IsRank2 = true;
    } else {
      this.IsRank2 = false;
    } if (age >= 1.0 && age <= 2.0) {
      this.IsRank1 = true;
    } else {
      this.IsRank1 = false;

    }
  }
}
