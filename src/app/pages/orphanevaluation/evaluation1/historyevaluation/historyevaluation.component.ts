import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { GlobalHttpService } from '../../../../shares/global-http.service';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { GlobalFunctionService } from '../../../../shares/global-function.service';
import { DialogBoxService } from '../../../../shares/dialog-box/dialog-box.service';

@Component({
  selector: 'ngx-historyevaluation',
  templateUrl: './historyevaluation.component.html',
  styleUrls: ['./historyevaluation.component.scss']
})
export class HistoryevaluationComponent implements OnInit {

  public tbSettings: any;
  public fullname: any;
  public nickname: any;
  public birthdate: any;
  public ageorphan: any;
  public ageorphanrank: any;
  public orphanstatusname: any;
  public isCheck: boolean = false;
  public orphanEval: any = {
    orphan: {
      traimas: "",
      age: ""
    }
  };
  public traimas: any;

  public orphanList: any;
  public title: any;
  public sub = {
    subs: {

    }
  }

  public model = {
    date: "",
    month: "",
    year: ""
  }
  public orphanids;
  public age;


  constructor(public func: GlobalFunctionService, private http: GlobalHttpService, private loading: Ng4LoadingSpinnerService, private activatedRoute: ActivatedRoute, private route: Router, private dialog: DialogBoxService) {}


  ngOnInit() {
    let _this = this;
    this.activatedRoute.params.subscribe(params => {
      console.log('activatedRoute');
      let orphanid = params['orphanid'];
      let age = params['age'];
      let traimas = params['traimas'];
      _this.orphanids = params['orphanid'];
      _this.age = params['age'];
      _this.traimas = params['traimas'];
      console.log("log >>> ", age, " ", orphanid, " ", traimas);
      _this.loadDataListHistory(age, orphanid, traimas);
    });
    
  }

  async loadDataListHistory(age, orphanid, traimas) {
    let params = {
      age: age,
      orphanid: orphanid,
      traimas: traimas
    };
    this.loading.show();
    let result = await this.http.post('api/evaluation/getevaluation', params);
    this.loading.hide();
    if (!result.error) {
      this.orphanEval = result.body;
      this.fullname = this.orphanEval.orphan.fullname;
      this.nickname = this.orphanEval.orphan.nickname;
      this.birthdate = this.orphanEval.orphan.birthdate;
      this.ageorphan = parseInt(age);
      this.ageorphanrank = age;
      this.orphanstatusname = this.orphanEval.orphan.orphanstatusname;

      let Year = this.orphanEval.evaluations[0].createdate;
      Year = Year.substr(0,4)
      let ThisYear = parseInt(Year) + 543;

      // console.log("this.orphanEval",this.orphanEval.evaluations[0].createdate);
      

      if (age <= 2.0) {
        // this.title = 'การประเมินพัฒนาการสำหรับเด็กอายุ 1-2 ปี' + ' ' + 'ประจำปี' + ' ' + ThisYear + ' ' + 'ครั้งที่ ' + params.traimas;
        this.title = `การประเมินพัฒนาการสำหรับเด็กอายุ 1-2 ปี ประจำปี ${ThisYear} ครั้งที่ ${params.traimas}`
        /* เมื่อก่อนเราใช้ + เพื่อต่อข้อความ แต่สำหรับ ES2015 นั้น Template String ทำให้การต่อข้อความเป็นเรื่องง่ายขึ้น ใช้ `` 
        ครอบข้อความที่จะทำเป็น template string จากนั้นใช้ ${} สำหรับส่วนที่ต้องการแทรกส่วนของ JavaScript */
      } else if (age <= 3.0) {
        this.title = `การประเมินพัฒนาการสำหรับเด็กอายุ 2-3 ปี ประจำปี ${ThisYear} ครั้งที่ ${params.traimas}`
      } else if (age <= 4.0) {
        this.title = `การประเมินพัฒนาการสำหรับเด็กอายุ 3-4 ปี ประจำปี ${ThisYear} ครั้งที่ ${params.traimas}`
      } else if (age <= 5.0) {
        this.title = `การประเมินพัฒนาการสำหรับเด็กอายุ 4-5 ปี ประจำปี ${ThisYear} ครั้งที่ ${params.traimas}`
      } else if (age <= 6.0) {
        this.title = `การประเมินพัฒนาการสำหรับเด็กอายุ 5-6 ปี ประจำปี ${ThisYear} ครั้งที่ ${params.traimas}`
      }
    }
    let fullbirthdate = this.birthdate;
    let year = fullbirthdate.substr(0, 4);
    let month = fullbirthdate.substr(5, 2);
    let day = fullbirthdate.substr(8, 2);

    if (month == "01") {
      this.model.month = "มกราคม";
    } else if (month == "02") {
      this.model.month = "กุมภาพันธ์";
    } else if (month == "03") {
      this.model.month = "มีนาคม";
    } else if (month == "04") {
      this.model.month = "เมษายน";
    } else if (month == "05") {
      this.model.month = "พฤษภาคม";
    } else if (month == "06") {
      this.model.month = "มิถุนายน";
    } else if (month == "07") {
      this.model.month = "กรกฎาคม";
    } else if (month == "08") {
      this.model.month = "สิงหาคม";
    } else if (month == "09") {
      this.model.month = "กันยายน";
    } else if (month == "10") {
      this.model.month = "ตุลาคม";
    } else if (month == "11") {
      this.model.month = "พฤศจิกายน";
    } else if (month == "12") {
      this.model.month = "ธันวาคม";
    }
    let y: any = parseInt(year) + 543;
    this.model.year = y;
    if (day < "10") {
      this.model.date = day.substr(1, 1);
    } else {
      this.model.date = day;
    }
  }
}
