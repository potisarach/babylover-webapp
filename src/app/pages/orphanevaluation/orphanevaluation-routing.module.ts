import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { OrphanevaluationComponent } from './orphanevaluation.component';
import { Evaluation1Component } from './evaluation1/evaluation1.component';
import { HistoryevaluationComponent } from './evaluation1/historyevaluation/historyevaluation.component';

const routes: Routes = [
  {
    path: ':rank',
    component: OrphanevaluationComponent
  },
  {
    path: 'form1/:age/:orphanid/:traimas',
    component: Evaluation1Component
  },
  {
    path: 'form/history/:age/:orphanid/:traimas',
    component: HistoryevaluationComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class OrphanevaluationRoutingModule {
}
