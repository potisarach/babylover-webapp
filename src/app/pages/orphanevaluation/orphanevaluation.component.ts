import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { ActivatedRoute, Router } from '@angular/router';
import { LocalDataSource, ViewCell } from 'ng2-smart-table';
import { GlobalFunctionService } from '../../shares/global-function.service';
import { GlobalHttpService } from '../../shares/global-http.service';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { GlobalApiService } from '../../shares/global-api.service';
import { log } from 'util';

declare var $: any;

@Component({
  selector: 'ngx-orphanevaluation',
  templateUrl: './orphanevaluation.component.html',
  styleUrls: ['./orphanevaluation.component.scss']
})
export class OrphanevaluationComponent implements OnInit {

  public datasource: LocalDataSource;
  public tbSettings: any;
  public data = [];
  public evaluationstatus: Array<any>;
  ranks: any;
  public orphanevaluationstatusid: any;
  public model = {
    fullname: "",
    agestartorphan: "",
    ageendorphan: "",
    orphanstatus: "",
    citizenid: "",
    genderid: "",
    prefixid: "",
    firstname: "",
    lastname: "",
    birthdate: "",
    bornlocation: "",
    fathername: "",
    mathername: "",
    historybefore: "",
    vaccinename: "",
    bodygrowth: "",
    emotionsocial: "",
    languagelearn: "",
    planworkwithorphan: "",
    ranks: "",
    evaluation: []
  }

  constructor(private route: ActivatedRoute, public func: GlobalFunctionService, private http: GlobalHttpService, private loading: Ng4LoadingSpinnerService,
    private modalService: NgbModal, private dropdownApi: GlobalApiService) {


    this.datasource = new LocalDataSource();
    this.setColumns();
  }

  // async loadDataList() {
  //   this.loading.show();
  //   let result = await this.http.post('api/evaluation/list', {});
  //   if (!result.error) {
  //     this.datasource = this.func.getTableDataSource(result.body);
  //     this.datasource.refresh();
  //   }
  //   this.loading.hide();
  // }


  ngOnInit() {
    this.onSearchOrphan();
    this.onReset();
    this.setupDropdown();

  }
  async onReset() {
    this.model.fullname = "";
    this.model.agestartorphan = "";
    this.model.ageendorphan = "";
    this.model.ranks = "";
    this.onSearchOrphan();
  }

  async setupDropdown() {
    this.loading.show();
    this.evaluationstatus = await this.dropdownApi.getOrphanEvaluationStatus();
    this.loading.hide();
  }

  // ค้นหาเด็ก
  async onSearchOrphan() {
    let fullname = this.model.fullname.trim();
    this.loading.show();
    let result = await this.http.post('api/evaluation/list', { fullname: fullname, ranks: this.model.ranks });
    let orphanValue = result.body;
    console.log("orphanValue >>>>> ",orphanValue);
    
    for (let item of orphanValue) {
      this.orphanevaluationstatusid = item.orphanevaluationstatusid;
    }

    if (result.success) {
      this.datasource = this.func.getTableDataSource(result.body);
    }
    this.loading.hide();
  }



  setColumns() {

    this.tbSettings = this.func.getTableSetting({
      fullname: {
        title: 'ชื่อ - นามสกุล',
        width: '23%',
        type: 'string'
      },
      age: {
        title: 'อายุ (ปี.เดือน)',
        width: '2%',
        type: 'html',
        valuePrepareFunction: (cell, row) => {
          return '<div class="text-center">' + cell + '</div>';
        }
      },
      orphanstatusname: {
        title: 'สถานภาพของเด็ก',
        width: '34%',
        type: 'html',
        valuePrepareFunction: (cell, row) => {
          if (!row.orphanstatusname) {
            return '<div class="text-left">ไม่ได้อยู่ในการอุปการะ</div>';
          } else {
            return '<div class="text-left">' + cell + '</div>'
          }
        }
      },
      evaluate1: {
        title: 'ครั้งที่ 1',
        width: '10%',
        type: 'custom',
        renderComponent: OrphanEvaluationTbButtonViewComponent,
        onComponentInitFunction(instance) {
          instance.evaluteHistory.subscribe(row => {
            console.log("Row of ประเมินเด็ก", row);
          })
        }
      },
      evaluate2: {
        title: 'ครั้งที่ 2',
        width: '10%',
        type: 'custom',
        renderComponent: OrphanEvaluationTbButtonView2Component,
        onComponentInitFunction(instance) {
          instance.evaluteHistory.subscribe(row => {
            console.log("Row of ประเมินเด็ก", row);
          })
        }
      },
      evaluate3: {
        title: 'ครั้งที่ 3',
        width: '10%',
        type: 'custom',
        renderComponent: OrphanEvaluationTbButtonView3Component,
        onComponentInitFunction(instance) {
          instance.evaluteHistory.subscribe(row => {
            console.log("Row of ประเมินเด็ก", row);
          })
        }
      },
      evaluate4: {
        title: 'ครั้งที่ 4',
        width: '10%',
        type: 'custom',
        renderComponent: OrphanEvaluationTbButtonView4Component,
        onComponentInitFunction(instance) {
          instance.evaluteHistory.subscribe(row => {
            console.log("Row of ประเมินเด็ก", row);
          })
        }
      },
    })
  }

  onEnter($event) {
    var keycode = $event.which || $event.keyCode;
    if (keycode == 13) {
      this.onSearchOrphan();
    }
  }
  chkName($event) {
    let charCode = $event.which || $event.keyCode;
    console.log(charCode);

    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return true;
    } else {
      return false;
    }
  }

}


// ปุ่มไตรมาสที่ 1
@Component({
  selector: 'tb-patron-list-button-view',
  template: `<div class="text-center">
    <a *ngIf="!trimasData && !completed" (click)="orphanEvaluation()"  class="button-view" title="ประเมินพัฒนาการเด็ก"><i class="fas fa-portrait"></i></a>
    <a *ngIf="trimasData && !completed"  (click)="orphanEvaluation()"  class="button-view" title="ประเมินพัฒนาการเด็ก"><i class="fas fa-portrait btn-compeleted"></i></a>
    <a *ngIf="completed"  (click)="orphanEvaluation()"  class="button-view" title="ประเมินพัฒนาการเด็ก"><i class="fas fa-portrait btn-uncompeleted"></i></a>
    </div>
  `,
  styleUrls: ['./orphanevaluation.component.scss']
})
export class OrphanEvaluationTbButtonViewComponent implements ViewCell, OnInit {

  renderValue: string;
  trimasData: any;
  completed: any;

  constructor(private http: GlobalHttpService, private rout: Router, private loading: Ng4LoadingSpinnerService) { }

  @Input() value: string | number;
  @Input() rowData: any;
  @Output() save: EventEmitter<any> = new EventEmitter();
  @Output() evaluteHistory: EventEmitter<any> = new EventEmitter();

  ngOnInit() {
    this.renderValue = this.value.toString().toUpperCase();
    this.setEvaluation();


  }

  async orphanEvaluation() {
    let trimas = 1;
    this.evaluteHistory.emit(this.rowData);
    this.loading.show();
    this.rout.navigate(['pages/orphan/evaluation/form1/' + this.rowData.age + '/' + this.rowData.orphanid + '/' + trimas]);
    this.loading.hide();
  }
  setEvaluation() {
    let age = this.rowData.age;
    let ranks;
    if (age >= 5.1 && age <= 6.0) {
      ranks = '5-6';
    } else if (age >= 4.1 && age <= 5.0) {
      ranks = '4-5';
    } else if (age >= 3.1 && age <= 4.0) {
      ranks = '3-4';
    } else if (age >= 2.1 && age <= 3.0) {
      ranks = '2-3';
    } else if (age >= 1.0 && age <= 2.0) {
      ranks = '1-2';
    }
    
    for (let data of this.rowData.evaluation) {

      if (data.time == null && data.completed == 0 && data.ranks == ranks) {
        this.trimasData = false;
        
      }
      if (data.time == 1 && data.completed == 0 && data.ranks == ranks) {
        this.completed = true;

      }
      if (data.time == 1 && data.completed == 1 && data.ranks == ranks) {
        this.trimasData = true;

      }
    }
  }
}


// ปุ่มไตรมาสที่ 2
@Component({
  selector: 'tb-patron-list-button-view',
  template: `<div class="text-center">
    <a *ngIf="!trimasData && !completed" (click)="orphanEvaluation2()"  class="button-view" title="ประเมินพัฒนาการเด็ก"><i class="fas fa-portrait"></i></a>
    <a *ngIf="trimasData && !completed"  (click)="orphanEvaluation2()"  class="button-view" title="ประเมินพัฒนาการเด็ก"><i class="fas fa-portrait btn-compeleted"></i></a>
    <a *ngIf="completed"  (click)="orphanEvaluation2()"  class="button-view" title="ประเมินพัฒนาการเด็ก"><i class="fas fa-portrait btn-uncompeleted"></i></a>
    </div>
  `,
  styleUrls: ['./orphanevaluation.component.scss']
})
export class OrphanEvaluationTbButtonView2Component implements ViewCell, OnInit {

  renderValue: string;
  trimasData: any;
  completed: any;

  constructor(private http: GlobalHttpService, private rout: Router, private loading: Ng4LoadingSpinnerService) { }

  @Input() value: string | number;
  @Input() rowData: any;
  @Output() save: EventEmitter<any> = new EventEmitter();
  @Output() evaluteHistory: EventEmitter<any> = new EventEmitter();

  ngOnInit() {
    this.renderValue = this.value.toString().toUpperCase();
    this.setEvaluation();


  }

  async orphanEvaluation2() {
    let trimas = 2;
    this.evaluteHistory.emit(this.rowData);
    this.loading.show();
    this.rout.navigate(['pages/orphan/evaluation/form1/' + this.rowData.age + '/' + this.rowData.orphanid + '/' + trimas]);
    this.loading.hide();
  }
  setEvaluation() {  
    let age = this.rowData.age;
    let ranks;
    if (age >= 5.1 && age <= 6.0) {
      ranks = '5-6';
    } else if (age >= 4.1 && age <= 5.0) {
      ranks = '4-5';
    } else if (age >= 3.1 && age <= 4.0) {
      ranks = '3-4';
    } else if (age >= 2.1 && age <= 3.0) {
      ranks = '2-3';
    } else if (age >= 1.0 && age <= 2.0) {
      ranks = '1-2';
    }
    for (let data of this.rowData.evaluation) {

      if (data.time == null && data.completed == 0 && data.ranks == ranks) {
        this.trimasData = false;
      }
      if (data.time == 2 && data.completed == 0 && data.ranks == ranks) {
        this.completed = true;
      }
      if (data.time == 2 && data.completed == 1 && data.ranks == ranks) {
        this.trimasData = true;
      }
    }
  }
}


// ปุ่มไตรมาสที่ 3
@Component({
  selector: 'tb-patron-list-button-view',
  template: `<div class="text-center">
  <a *ngIf="!trimasData && !completed" (click)="orphanEvaluation3()"  class="button-view" title="ประเมินพัฒนาการเด็ก"><i class="fas fa-portrait"></i></a>
  <a *ngIf="trimasData && !completed"  (click)="orphanEvaluation3()"  class="button-view" title="ประเมินพัฒนาการเด็ก"><i class="fas fa-portrait btn-compeleted"></i></a>
  <a *ngIf="completed"  (click)="orphanEvaluation3()"  class="button-view" title="ประเมินพัฒนาการเด็ก"><i class="fas fa-portrait btn-uncompeleted"></i></a>
    </div>
  `,
  styleUrls: ['./orphanevaluation.component.scss']
})
export class OrphanEvaluationTbButtonView3Component implements ViewCell, OnInit {

  renderValue: string;
  trimasData: any;
  completed: any;

  constructor(private http: GlobalHttpService, private rout: Router, private loading: Ng4LoadingSpinnerService) { }

  @Input() value: string | number;
  @Input() rowData: any;
  @Output() save: EventEmitter<any> = new EventEmitter();
  @Output() evaluteHistory: EventEmitter<any> = new EventEmitter();

  ngOnInit() {
    this.renderValue = this.value.toString().toUpperCase();
    this.setEvaluation();

  }

  async orphanEvaluation3() {
    let trimas = 3;
    this.evaluteHistory.emit(this.rowData);
    this.loading.show();
    this.rout.navigate(['pages/orphan/evaluation/form1/' + this.rowData.age + '/' + this.rowData.orphanid + '/' + trimas]);
    this.loading.hide();
  }
  setEvaluation() {
    let age = this.rowData.age;
    let ranks;
    if (age >= 5.1 && age <= 6.0) {
      ranks = '5-6';
    } else if (age >= 4.1 && age <= 5.0) {
      ranks = '4-5';
    } else if (age >= 3.1 && age <= 4.0) {
      ranks = '3-4';
    } else if (age >= 2.1 && age <= 3.0) {
      ranks = '2-3';
    } else if (age >= 1.0 && age <= 2.0) {
      ranks = '1-2';
    }
    for (let data of this.rowData.evaluation) {
      if (data.time == null && data.completed == 0 && data.ranks == ranks) {
        this.trimasData = false;
      }
      if (data.time == 3 && data.completed == 0 && data.ranks == ranks)  {
        this.completed = true;
      }
      if (data.time == 3 && data.completed == 1 && data.ranks == ranks) {
        this.trimasData = true;
      }
    }
  }
}


// ปุ่มไตรมาสที่ 4
@Component({
  selector: 'tb-patron-list-button-view',
  template: `<div class="text-center">
  <a *ngIf="!trimasData && !completed" (click)="orphanEvaluation4()"  class="button-view" title="ประเมินพัฒนาการเด็ก"><i class="fas fa-portrait"></i></a>
  <a *ngIf="trimasData && !completed"  (click)="orphanEvaluation4()"  class="button-view" title="ประเมินพัฒนาการเด็ก"><i class="fas fa-portrait btn-compeleted"></i></a>
  <a *ngIf="completed"  (click)="orphanEvaluation4()"  class="button-view" title="ประเมินพัฒนาการเด็ก"><i class="fas fa-portrait btn-uncompeleted"></i></a>
    </div>
  `,
  styleUrls: ['./orphanevaluation.component.scss']
})
export class OrphanEvaluationTbButtonView4Component implements ViewCell, OnInit {

  renderValue: string;
  trimasData: any;
  completed: any;

  constructor(private http: GlobalHttpService, private rout: Router, private loading: Ng4LoadingSpinnerService) { }

  @Input() value: string | number;
  @Input() rowData: any;
  @Output() save: EventEmitter<any> = new EventEmitter();
  @Output() evaluteHistory: EventEmitter<any> = new EventEmitter();

  ngOnInit() {
    this.renderValue = this.value.toString().toUpperCase();
    this.setEvaluation();



  }

  async orphanEvaluation4() {
    let trimas = 4;
    this.evaluteHistory.emit(this.rowData);
    this.loading.show();
    this.rout.navigate(['pages/orphan/evaluation/form1/' + this.rowData.age + '/' + this.rowData.orphanid + '/' + trimas]);
    this.loading.hide();
  }
  setEvaluation() {
    let age = this.rowData.age;
    let ranks;
    if (age >= 5.1 && age <= 6.0) {
      ranks = '5-6';
    } else if (age >= 4.1 && age <= 5.0) {
      ranks = '4-5';
    } else if (age >= 3.1 && age <= 4.0) {
      ranks = '3-4';
    } else if (age >= 2.1 && age <= 3.0) {
      ranks = '2-3';
    } else if (age >= 1.0 && age <= 2.0) {
      ranks = '1-2';
    }
    for (let data of this.rowData.evaluation) {
      if (data.time == null && data.completed == 0 && data.ranks == ranks) {
        this.trimasData = false;
      }
      if (data.time == 4 && data.completed == 0 && data.ranks == ranks) {
        this.completed = true;
      }
      if (data.time == 4 && data.completed == 1 && data.ranks == ranks) {
        this.trimasData = true;
      }
    }
  }
}