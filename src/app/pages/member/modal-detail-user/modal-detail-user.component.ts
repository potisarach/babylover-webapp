import { Component, OnInit } from '@angular/core';
import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { GlobalFunctionService } from '../../../shares/global-function.service';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { GlobalHttpService } from '../../../shares/global-http.service';
import { GlobalApiService } from '../../../shares/global-api.service';

@Component({
  selector: 'ngx-modal-detail-user',
  templateUrl: './modal-detail-user.component.html',
  styleUrls: ['./modal-detail-user.component.scss']
})
export class ModalDetailUserComponent implements OnInit {
  public model = {
    userid: "",
    firstname: "",
    lastname: "",
    fullname:"",
    username: "",
    password: "",
    email: "",
    phone: "",
    picturepath: "",
    userstatusid: "",
    userstatusname: ""

  }
  public imagePreView = "";
  constructor(private modalService: NgbModal,private activeModal:NgbActiveModal, public func: GlobalFunctionService, private loading: Ng4LoadingSpinnerService, private http: GlobalHttpService, private dropdownApi: GlobalApiService) {

   }

  ngOnInit() {
  }

  close(){   
    this.activeModal.close();
  }

}
