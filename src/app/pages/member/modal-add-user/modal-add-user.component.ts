import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { GlobalApiService } from '../../../shares/global-api.service';
import { GlobalHttpService } from '../../../shares/global-http.service';
import { DialogBoxService } from '../../../shares/dialog-box/dialog-box.service';
import { Title } from '@angular/platform-browser';
import { GlobalFunctionService } from '../../../shares/global-function.service';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { LocalDataSource, ViewCell } from 'ng2-smart-table';

declare var $: any;

@Component({
  selector: 'ngx-modal-add-user',
  templateUrl: './modal-add-user.component.html',
  styleUrls: ['./modal-add-user.component.scss']
})
export class ModalAddUserComponent implements OnInit {
  public isCheck: boolean = false;
  public datenow: any;
  public isDuplicate: boolean = false;
  public selectedFile: File = null;
  imagePreView: string = '';
  public model = {
    userid: "",
    firstname: "",
    lastname: "",
    username: "",
    password: "",
    email: "",
    phone: "",
    picturepath: "",
    userstatusid: ""

  }

  public userstatus: any;
  form: FormGroup;


  constructor(private dialog: DialogBoxService, private activeModal: NgbActiveModal, private dropdownApi: GlobalApiService, private apiHttp: GlobalHttpService, private titleService: Title, public func: GlobalFunctionService, private loading: Ng4LoadingSpinnerService) {
    this.form = new FormGroup({

      'firstname': new FormControl('', [Validators.required]),
      'lastname': new FormControl('', [Validators.required]),
      // 'username': new FormControl(null),
      // 'username': new FormControl('', [Validators.required]),
      'password': new FormControl('', [Validators.required]),
      'email': new FormControl('', [Validators.required]),
      'phone': new FormControl('', [Validators.required, Validators.maxLength(10)]),
      'picturepath': new FormControl(null),
      'userstatusid': new FormControl('', [Validators.required])

    })
  }

  ngOnInit() {
    this.GetStatusName();
  }
  isError(control) {
    if (control.invalid && control.touched || this.isCheck) {
      // return errorKey in control.errors
      return control.errors || false;
    }
    return false;
  }
  async onFileSelected(event, input) {
    this.onConvertImage(input)
    console.log("Input > > >",input);
    
    console.log("Evn>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>", event);
    this.selectedFile = await <File>event.target.files[0];
    console.log("selectedFile", this.selectedFile);

  }
  onConvertImage(input: HTMLInputElement) {
    console.log("Value", this.form.value)
    console.log("INPUT>>>>>>>>>>>>>>>>" + input + "<<<<<<<<<<<<<<<<<<<<<");
    // const imageControl = this.form.controls['personimg'];
    // imageControl.setValue(null)
    console.log("HTMLInputElement = ", input);
    if (input.files.length == 0) return this.imagePreView = '';
    console.log("INPUTFile", input.files[0].type);
    const reader = new FileReader();
    reader.readAsDataURL(input.files[0]);
    reader.addEventListener('load', () => {

      this.imagePreView = reader.result.toString();
      console.log(this.model);

    })
  }
  async onSave() {
    this.loading.show();
    if (this.selectedFile) {
      console.log(this.selectedFile.size);
    }
    this.isCheck = true;
    this.model.picturepath = this.imagePreView
    console.log("this.model > ", this.model);

    if (this.form.invalid || this.isDuplicate == true) {
      this.dialog.error({ title: 'แจ้งเตือน', message: "กรุณากรอกข้อมูลให้สมบูรณ์" });
      console.log("this.form", this.form);
      console.log("this.isDuplicate >>>>>>>>>> ",this.isDuplicate); 
    } else {
      this.model.username = this.model.email;
      let result: any = await this.apiHttp.post('api/user/update', this.model);
      if (result.success) {
        this.dialog.alert({ title: 'แจ้งเตือน', message: "บันทึกข้อมูลสำเร็จ" });
        this.activeModal.close({ success: true });
        let email = this.model.email
        // let username = this.model.username
        let password = this.model.password
        let resultsend = await this.apiHttp.post('api/user/sendemail', { to: email, subject: 'แจ้งรหัสผ่าน', password: password })
        if (resultsend.success) {
          // this.dialog.alert({ title: 'แจ้งเตือน', message: "บันทึกข้อมูลสำเร็จ" });
        }
      } else {
        this.dialog.error({ title: 'แจ้งเตือน', message: "ไม่สามารถบันทึกข้อมูลได้" });
      }
    }
    this.loading.hide();
  }
  Generate() {
    this.model.password = ""
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for (var i = 0; i < 8; i++)
      this.model.password += possible.charAt(Math.floor(Math.random() * possible.length));

  }
  async GetStatusName() {
    let result = await this.apiHttp.post('api/user/getstatus');
    console.log("result.body   > > >", result.body);

    this.userstatus = result.body

    console.log("userstatus > > >", this.userstatus);

  }
  async OnBlur(){
    let email = this.model.email;
    console.log("Email > ", this.model.email);

    let results = await this.apiHttp.post("api/validate/checkduplicate/email");

    console.log("results.body > ", results.body);
    console.log("results.body.length > ", results.body.length);
    console.log("results.body[0] > ", results.body[0].email);

    for (let i = 0; i < results.body.length; i++) {
      console.log("results.body[i].email ", results.body[i].email);
      if (results.body[i].email == this.model.email) {
        this.isDuplicate = true;
        console.log("i ", i);
        console.log("results.body[i].email >>", results.body[i].email);
      }else{
        this.isDuplicate = false;
      }
    } 
  }
  closeModal() {
    this.dialog.confirm({ title: 'ยืนยัน', message: 'ต้องการยกเลิกใช่หรือไม่' }, (ans) => {
      if (ans) {
        this.activeModal.close();
      }
    });
  }
}
