import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { GlobalHttpService } from '../../shares/global-http.service';
import { GlobalFunctionService } from '../../shares/global-function.service';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { GlobalApiService } from '../../shares/global-api.service';
import { LocalDataSource, ViewCell } from 'ng2-smart-table';
import { DialogBoxService } from '../../shares/dialog-box/dialog-box.service';
import { Router, RouterLink } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ModalAddUserComponent } from './modal-add-user/modal-add-user.component';
import { ModalEditUserComponent } from './modal-edit-user/modal-edit-user.component';
import { ModalDetailUserComponent } from './modal-detail-user/modal-detail-user.component';
declare var $: any;

@Component({
  selector: 'ngx-member',
  templateUrl: './member.component.html',
  styleUrls: ['./member.component.scss']
})
export class MemberComponent implements OnInit {
  public action: string;
  public model = {
    fullname: "",
    username: "",
    password: "",
    email: "",
    phone: ""
  }

  public datasource: LocalDataSource;
  public tbSettings: any;
  constructor(private modalService: NgbModal, public func: GlobalFunctionService, private loading: Ng4LoadingSpinnerService, private http: GlobalHttpService, private dropdownApi: GlobalApiService, private httpclient: HttpClient, private dialog: DialogBoxService) {
    this.datasource = new LocalDataSource();
    this.setColumns();
  }

  ngOnInit() {
    this.model.fullname = "";
    this.onSearchUser();
  }
  setColumns() {
    let _this = this;
    this.tbSettings = this.func.getTableSetting({
      fullname: {
        title: 'ชื่อ - นามสกุล',
        width: '30%',
        type: 'string',
        valuePrepareFunction: (cell, row) => {
          return row.fullname;
        }
      },
      username: {
        title: 'ชื่อผู้เข้าใช้งาน',
        width: '25%',
        type: 'html',
        valuePrepareFunction: (cell, row) => {
          return '<div class="text-center">' + row.username + '</div>';
        }
      },
      status: {
        title: 'สถานะผู้เข้าใช้งาน',
        width: '25%',
        type: 'html',
        valuePrepareFunction: (cell, row) => {
          return '<div class="text-center">' + row.status + '</div>';
        }
      },
      action: {
        title: 'จัดการ',
        width: '30%',
        sort: false,
        type: 'custom',
        renderComponent: UserFormTbButtonViewComponent,
        onComponentInitFunction(instance) {
          instance.view.subscribe(row => {
            console.log(row);
            _this.onView(row);
          });
          instance.edit.subscribe(row => {
            console.log(row);
            _this.onEdit(row);
          });
          instance.delete.subscribe(row => {
            console.log(row);
            _this.onDelete(row);
          });
        }
      }
    });
  }
  async onView(rowData) {
    this.loading.show();
    let res = await this.http.post('api/user/getEdit', { userid: rowData.userid });
    if (res.success) {
      const activeModal = this.modalService.open(ModalDetailUserComponent, { size: 'lg', backdrop: 'static', keyboard: false, container: 'nb-layout' });
      activeModal.componentInstance.model = res.body;
      activeModal.result.then((result) => {
        console.log(result);
        if (result && result.success) {
          this.onSearchUser();
        }
      });
    }
    this.loading.hide();
  }

  //เรียกค่าเดิม และmodalมาแสดงก่อนทำการแก่ไขใน modal นั้น ๆ
  async onEdit(rowData) {
    this.loading.show();
    console.log("hiiiiiii");

    let res = await this.http.post('api/user/getEdit', { userid: rowData.userid });
    console.log("res >>>>>", res);

    if (res.success) {
      const activeModal = this.modalService.open(ModalEditUserComponent, { size: 'lg', backdrop: 'static', keyboard: false, container: 'nb-layout' });
      activeModal.componentInstance.model = res.body;
      activeModal.result.then((result) => {
        console.log(result);
        if (result && result.success) {
          this.onSearchUser();
        }
      });
    }
    this.loading.hide();
  }

  async onDelete(rowData) {
    this.dialog.confirm({ title: 'ยืนยัน', message: 'ต้องการลบข้อมูลใช่หรือไม่' }, async (ans) => {
      if (ans) {
        console.log("ID > ", rowData.userid);
        let userid = rowData.userid;
        console.log("ID > > ", userid);

        // let result = await this.http.post('api/user/delete', {userid});


        console.log("ลบข้อมูลผู้ใช้นะจ๊ะ");

        if (userid == undefined) {
          this.dialog.alert({ title: 'แจ้งเตือน', message: "ไม่มีข้อมูล" });
        } else {
          let result = await this.http.post('api/user/delete', { userid });
          if (result.success) {
            console.log("delete");
            this.dialog.alert({ title: 'แจ้งเตือน', message: "ลบข้อมูลสำเร็จ" });
            this.onSearchUser();
          } else {
            this.dialog.error({ title: 'แจ้งเตือน', message: "ไม่สามารถลบข้อมูลได้" });
          }
        }

      }

    });
  }

  addUser() {
    this.loading.show();
    const activeModal = this.modalService.open(ModalAddUserComponent, { size: 'lg', keyboard: false, backdrop: "static", container: 'nb-layout' });
    activeModal.result.then((result) => {
      console.log(result);
      if (result && result.success) {
        this.onSearchUser();
      }
    });
    this.loading.hide();
  }
  async onReset() {
    this.loading.show();
    this.model.fullname = "";
    this.onSearchUser();
    this.loading.hide();
  }
  async onSearchUser() {
    let user = this.model.fullname.trim();
    this.loading.show();
    let result = await this.http.post('api/user/search', { fullname: user });

    if (result.success) {
      this.datasource = this.func.getTableDataSource(result.body);
    }
    this.loading.hide();
  }
}

@Component({
  selector: 'tb-patron-list-button-view',
  template: `<div class="text-center"> 
    <a (click)="onVeiw()" class="button-view" title="ดูข้อมูลส่วนตัว"><i class="far fa-eye icon"></i></a>&nbsp;
    <a *ngIf="this.rowData.userstatusid != 1" (click)="onEdit()" class="button-view" title="แก้ไขข้อมูลส่วนตัว"><i class="fas fa-user-edit icon"></i></a>&nbsp;
    <a *ngIf="this.rowData.userstatusid != 1" (click)="onDelete()" class="button-view" title="ลบผู้ใช้งาน"><i class="fas fa-trash-alt icon"></i></a>&nbsp;
    </div>
  `,
  styleUrls: ['./member.component.scss']
})
export class UserFormTbButtonViewComponent implements ViewCell, OnInit {
  renderValue: string;
  @Input() value: string | number;
  @Input() rowData: any;
  @Output() view: EventEmitter<any> = new EventEmitter();
  @Output() edit: EventEmitter<any> = new EventEmitter();
  @Output() delete: EventEmitter<any> = new EventEmitter();

  constructor() { }

  ngOnInit() { //ฟังก์ชันแรกที่ทำงาน
    this.renderValue = this.value.toString().toUpperCase();
    console.log("this.rowData ที่ userform ", this.rowData.userstatusid);
  }

  onEdit() {
    this.edit.emit(this.rowData);
  }
  onVeiw() {
    this.view.emit(this.rowData);
    // alert("ดูข้อมูลผู้อุปถัมภ์");
  }
  onDelete() {
    this.delete.emit(this.rowData);
    // alert("ลบ")
  }

}
