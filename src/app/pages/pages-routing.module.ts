import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { PagesComponent } from './pages.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { NotFoundComponent } from './miscellaneous/not-found/not-found.component';
import { DevelopmentsComponent } from './developments/developments.component';
import { MemberComponent } from './member/member.component';

const routes: Routes = [{
  path: '',
  component: PagesComponent,
  children: [
    {
      path: 'communitydata',
      loadChildren: './communitydata/communitydata.module#CommunitydataModule'
    },
    {
      path: 'patron',
      loadChildren: './patron/patron.module#PatronModule'
    },
    {
      path: 'orphan',
      loadChildren: './orphan/orphan.module#OrphanModule'
    },
    {
      path: 'advancement',
      loadChildren: './advancement/advancement.module#AdvancementModule'
    },
    {
      path: 'orphan/evaluation',
      loadChildren: './orphanevaluation/orphanevaluation.module#OrphanevaluationModule'
    }
    ,
    {
      path: 'developments',
      component: DevelopmentsComponent
    },
    {
      path: 'dashboard',
      component: DashboardComponent,
    }, 
    {
      path: 'UserManage',
      component: MemberComponent,
    }, 
    {
      path: 'ui-features',
      loadChildren: './ui-features/ui-features.module#UiFeaturesModule',
    }, {
    path: 'components',
    loadChildren: './components/components.module#ComponentsModule',
  }, {
    path: 'maps',
    loadChildren: './maps/maps.module#MapsModule',
  }, {
    path: 'charts',
    loadChildren: './charts/charts.module#ChartsModule',
  }, {
    path: 'editors',
    loadChildren: './editors/editors.module#EditorsModule',
  }, {
    path: 'forms',
    loadChildren: './forms/forms.module#FormsModule',
  }, {
    path: 'tables',
    loadChildren: './tables/tables.module#TablesModule',
  }, {
    path: 'miscellaneous',
    loadChildren: './miscellaneous/miscellaneous.module#MiscellaneousModule',
  }, {
    path: '',
    redirectTo: 'dashboard',
    pathMatch: 'full',
  }, {
    path: '**',
    component: NotFoundComponent,
  }],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PagesRoutingModule {
}
