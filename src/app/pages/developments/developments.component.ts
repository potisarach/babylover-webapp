import { Component, OnInit } from '@angular/core';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { GlobalFunctionService } from '../../shares/global-function.service';
import { GlobalApiService } from '../../shares/global-api.service';

@Component({
  selector: 'ngx-developments',
  templateUrl: './developments.component.html',
  styleUrls: ['./developments.component.scss']
})
export class DevelopmentsComponent implements OnInit {
  public template: string;
  public address: any = {
    provincecode: '',
    amphurcode: '',
    tumbolcode: '',
    provinces: [],
    amphurs: [],
    tumbols: []
  
  }
  constructor(public fn: GlobalFunctionService, private api: GlobalApiService,private loading: Ng4LoadingSpinnerService) { 
  }
  ngOnInit() {
    this.setProvinceList();
  }

  async setProvinceList(){
    this.loading.show();
    this.address.provincecode = '';
    this.address.amphurcode = '';
    this.address.tumbolscode = '';
    this.address.amphurs = [];
    this.address.tumbols = [];
    this.address.provinces = await this.api.getProvinceList();
    this.loading.hide();
  }
  async setAmphurList(){
    this.loading.show();
    this.address.amphurcode = '';
    this.address.tumbolscode = '';
    this.address.amphurs = [];
    this.address.tumbols = [];
    this.address.amphurs = await this.api.getAmphurList(this.address.provincecode);
    this.loading.hide();
  }

  async setTumbolList(){
    this.loading.show();
    this.address.tumbolcode = '';
    this.address.tumbols = [];
    this.address.tumbols = await this.api.getTumbolList(this.address.amphurcode);
    this.loading.hide();
  }
}
