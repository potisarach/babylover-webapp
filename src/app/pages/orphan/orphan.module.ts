import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OrphanManagementComponent, OrphanFormTbButtonViewComponent, OrphanFormTextButtonHistoryComponent, OrphanFormTextStatusComponent } from './orphan-management/orphan-management.component';
import { OrphanRoutingModule } from './orphan-routing.module';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { ThemeModule } from '../../@theme/theme.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { ModalOrphanFormComponent } from './orphan-management/modal-orphan-form/modal-orphan-form.component';
import { ModalAddDetailOrphanComponent } from './orphan-management/modal-add-detail-orphan/modal-add-detail-orphan.component';
import { ModalAddStatusOrphanComponent, PatronFormTbCheckboxComponent } from './orphan-management/modal-add-status-orphan/modal-add-status-orphan.component';
import { Ng4LoadingSpinnerModule } from 'ng4-loading-spinner';
import { ModalListPatronComponent } from './orphan-management/modal-list-patron/modal-list-patron.component';
import { ModalViewDetailOrphanComponent } from './orphan-management/modal-view-detail-orphan/modal-view-detail-orphan.component';

@NgModule({
  imports: [
    CommonModule,
    OrphanRoutingModule,
    Ng2SmartTableModule,
    ThemeModule,
    ReactiveFormsModule,
    FormsModule,
    Ng4LoadingSpinnerModule.forRoot(),
  ],
  declarations: [ 
    OrphanManagementComponent,
    OrphanFormTbButtonViewComponent,
    OrphanFormTextStatusComponent,
    OrphanFormTextButtonHistoryComponent,
    ModalAddDetailOrphanComponent,
    ModalOrphanFormComponent,
    ModalAddStatusOrphanComponent,
    ModalListPatronComponent,
    ModalViewDetailOrphanComponent,
    PatronFormTbCheckboxComponent
  ],
  entryComponents:[
    OrphanFormTextStatusComponent,
    OrphanFormTbButtonViewComponent,
    OrphanFormTextButtonHistoryComponent,
    ModalAddDetailOrphanComponent,
    ModalOrphanFormComponent,
    ModalAddStatusOrphanComponent,
    ModalListPatronComponent,
    PatronFormTbCheckboxComponent,
    ModalViewDetailOrphanComponent,

  ]
})
export class OrphanModule { }
