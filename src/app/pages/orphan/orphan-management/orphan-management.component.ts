import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { LocalDataSource, ViewCell, Cell } from 'ng2-smart-table';
import { GlobalFunctionService } from '../../../shares/global-function.service';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { GlobalHttpService } from '../../../shares/global-http.service';
import { ModalAddDetailOrphanComponent } from './modal-add-detail-orphan/modal-add-detail-orphan.component';
import { ModalOrphanFormComponent } from './modal-orphan-form/modal-orphan-form.component';
import { GlobalApiService } from '../../../shares/global-api.service';
import { ModalAddStatusOrphanComponent } from './modal-add-status-orphan/modal-add-status-orphan.component';
import { ModalViewDetailOrphanComponent } from './modal-view-detail-orphan/modal-view-detail-orphan.component';
import { HttpClient, HttpParams } from '@angular/common/http';

declare var $: any;
  
@Component({
  selector: 'ngx-orphan-management',
  templateUrl: './orphan-management.component.html',
  styleUrls: ['./orphan-management.component.scss']
})
export class OrphanManagementComponent implements OnInit {


  public orphanstatus: Array<any>;

  public model = {
    fullname: "",
    agestartorphan: "",
    ageendorphan: "",
    orphanstatus: "",
    citizenid: "",
    genderid: "",
    prefixid: "",
    firstname: "",
    lastname: "",
    birthdate: "",
    bornlocation: "",
    fathername: "",
    mathername: "",
    historybefore: "",
    vaccinename: "",
    bodygrowth: "",
    emotionsocial: "",
    languagelearn: "",
    planworkwithorphan: "",
    orphanstatusname: ""
  }

  public datasource: LocalDataSource;
  public tbSettings: any;
  public datas = [];

  constructor(private modalService: NgbModal, public func: GlobalFunctionService, private loading: Ng4LoadingSpinnerService, private http: GlobalHttpService, private dropdownApi: GlobalApiService,private httpclient :HttpClient) {
    this.datasource = new LocalDataSource();
    this.setColumns();
  }

  ngOnInit() {
    this.loading.show();
    this.onSearchOrphan();
    this.onReset();
    this.setupDropdown();
    this.loading.hide();
  }
  async onReset() {
    this.loading.show();
    this.model.ageendorphan = "";
    this.model.agestartorphan = "";
    this.model.fullname = "";
    this.model.orphanstatus = "";
    this.onSearchOrphan();
    this.loading.hide();
  }

  async setupDropdown() {
    this.loading.show();
    this.orphanstatus = await this.dropdownApi.getOrphanstatusList();
    this.loading.hide();
  }

  setColumns() {
    let _this = this;
    this.tbSettings = this.func.getTableSetting({
      fullname: {
        title: 'ชื่อ - นามสกุล',
        width: '22%',
        type: 'string',
      },

      age: {
        title: 'อายุ (ปี.เดือน)',
        width: '10%',
        type: 'html',
        valuePrepareFunction: (cell, row) => {
          return '<div class="text-center">' + cell + '</div>';
        }
      },
      name: {
        title: 'สถานภาพของเด็กฯ',
        width: '36%',
        type: 'custom',
        renderComponent: OrphanFormTextStatusComponent,
        onComponentInitFunction(instance) {
          instance.statusOrphan.subscribe(row => {
            _this.onSearchOrphan();
          })
        }  
      },
      evaluate: {
        title: 'ประวัติเด็กก่อนเข้าโครงการ',
        width: '15%',
        type: 'custom',
        renderComponent: OrphanFormTextButtonHistoryComponent,  
        onComponentInitFunction(instance) {
          instance.evaluteHistory.subscribe(row => {
            console.log(row);
          })
        }
      },
      action: {
        title: 'จัดการ',
        width: '17%',
        type: 'custom',
        renderComponent: OrphanFormTbButtonViewComponent,
        onComponentInitFunction(instance) {
          instance.view.subscribe(row => {
            console.log(row);
            _this.onView(row);
          });
          instance.edit.subscribe(row => {
            console.log(row);
            _this.onEdit(row);
          });
        }
      }
    },
      (row) => {
        return 'status' + row.data.statusid;
      }
    );
  }

  // เพิ่มข้อมูลเด็ก
  onAdd() {
    this.loading.show();
    const activeModal = this.modalService.open(ModalOrphanFormComponent, { size: 'lg', backdrop: 'static', keyboard: false, container: 'nb-layout' });
    activeModal.result.then((result) => {
      console.log(result);
      if (result && result.success) {
        this.onSearchOrphan();
      }
    });
    this.loading.hide();
  }

  async onEdit(rowData) {
    this.loading.show();
    let res = await this.http.post('api/orphan/get', { orphanid: rowData.orphanid });
    if (res.success) {
      const activeModal = this.modalService.open(ModalOrphanFormComponent, { size: 'lg', backdrop: 'static', keyboard: false, container: 'nb-layout' });
      activeModal.componentInstance.model = res.body;
      activeModal.result.then((result) => {
        console.log(result);
        if (result && result.success) {
          this.onSearchOrphan();
        }
      });
    }
    this.loading.hide();
  }
  async onView(rowData) {
    this.loading.show();
    let res = await this.http.post('api/orphan/get', { orphanid: rowData.orphanid });
    if(res.success){
    const activeModal = this.modalService.open(ModalViewDetailOrphanComponent, { size: 'lg', backdrop: 'static', keyboard: false, container: 'nb-layout' });
    activeModal.componentInstance.model = res.body;
    activeModal.result.then((result) => {
      console.log(result);
      if (result && result.success) {
        this.onSearchOrphan();
      }
    });
  }
    this.loading.hide();
  }
  

  // ค้นหาเด็ก
  async onSearchOrphan() {
    this.loading.show();
    let fullname = this.model.fullname.trim();
    let result = await this.http.post('api/orphan/orphan/search', { fullname: fullname, agestartorphan: this.model.agestartorphan, ageendorphan: this.model.ageendorphan, orphanstatus: this.model.orphanstatus });
    if (result.success) {
      this.datasource = this.func.getTableDataSource(result.body);
      console.log("datasource -> ", this.datasource);
      
    }
    this.loading.hide();
  }

  onEnter($event) {
    var keyCode = $event.which || $event.keyCode;
    if (keyCode === 13) {
      this.onSearchOrphan();
    }
  }

  // เช็คการพิมพ์ตัวอักษร (พิมพ์ได้เฉพาะตัวเลข)
  chkAge($event) {
    let charCode = $event.which || $event.keyCode;

    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    } else {
      return true;
    }
  }

  // เช็คการพิมพ์ตัวเลข(พิมพ์ได้เฉพาะตัวอักษร)
  chkName($event) {
    let charCode = $event.which || $event.keyCode;
    console.log(charCode);

    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return true;
    } else {
      return false;
    }
  }
}

@Component({
  selector: 'tb-patron-list-button-view',
  template: `<div class="text-center">
    <a (click)="onView()" class="button-view" title="ดูข้อมูล"><i class="far fa-eye icon"></i></a>&nbsp;
    <a (click)="onEdit()" class="button-view" title="แก้ไขข้อมูล"><i class="fas fa-user-edit"></i></a>&nbsp;
    <a href="{{func.getHostAPI()}}report/orphan/?orphanid={{rowData.orphanid}}" class="button-view" title="ดาวน์โหลดข้อมูล"><i class="fas fa-file-download fa-sm"></i></a>
    
    </div>
  `,
  styleUrls: ['./orphan-management.component.scss']
})
export class OrphanFormTbButtonViewComponent implements ViewCell, OnInit {

  renderValue: string;

  constructor(private modalService: NgbModal, public func: GlobalFunctionService) { }

  @Input() value: string | number;
  @Input() rowData: any;
  @Output() view: EventEmitter<any> = new EventEmitter();
  @Output() edit: EventEmitter<any> = new EventEmitter();
  @Output() downlaodorphan: EventEmitter<any> = new EventEmitter();

  ngOnInit() {
    this.renderValue = this.value.toString().toUpperCase();
  }

  onView() {
    this.view.emit(this.rowData);
  }
  onEdit() {
    this.edit.emit(this.rowData);
  }

}

@Component({
  selector: 'tb-patron-list-button-view',
  template: `<div class="text-center">
    <a *ngIf="!statusCompleted" (click)="onEvalute()" title="จัดการข้อมูล"><i class="fas fa-edit"></i></a>
    <a *ngIf="statusCompleted" (click)="onEvalute()" title="จัดการข้อมูล"><i class="fas fa-edit icon-statusCompleted"></i></a> &nbsp;
    <a href="{{func.getHostAPI()}}report/historyorphan/?orphanid={{rowData.orphanid}}" class="button-view" title="ดาวน์โหลดข้อมูล"><i class="fas fa-file-download fa-sm"></i></a>
    </div>
  `,
  styleUrls: ['./orphan-management.component.scss']
})
export class OrphanFormTextButtonHistoryComponent implements ViewCell, OnInit {

  renderValue: string;
  statusCompleted: any;

  constructor(private modalService: NgbModal, private http: GlobalHttpService, public func: GlobalFunctionService, private loading: Ng4LoadingSpinnerService) { }

  @Input() value: string | number;
  @Input() rowData: any;
  @Output() evaluteHistory: EventEmitter<any> = new EventEmitter();


  ngOnInit() {
    this.loading.show();
    this.renderValue = this.value.toString().toUpperCase();
    this.loading.hide();
    this.statusCheck();
  }

  async onEvalute() {
    this.loading.show();
    this.evaluteHistory.emit(this.rowData);
    let orphan = await this.http.post('api/orphan/evaluation/get', { orphanid: this.rowData.orphanid });

    const activeModal = this.modalService.open(ModalAddDetailOrphanComponent, { size: 'lg', backdrop: 'static', keyboard: true, container: 'nb-layout' });

    activeModal.componentInstance.model = this.func.cloneObject(orphan.body);
    this.loading.hide();
  }
  statusCheck() {
    if (this.rowData.orphanhistoryid == null) {
      this.statusCompleted = false;
    } else {
      this.statusCompleted = true;
    }
  }


}

// ปุ่มจัดการสถานะภาพของเด็กฯ
@Component({
  selector: 'tb-patron-list-button-view',
  template: `<div class="text-left">
    <label title="สถานภาพปัจจุบัน : {{rowData.orphanstatusname}}\nรายละเอียด : {{rowData.detail}}\nเวลาการดำเนินการ : {{rowData.updatestatusdate}}">{{rowData.orphanstatusname}}</label>&nbsp;
    <a (click)="onStatus()" title="จัดการสถานะของเด็กในอุปถัมภ์"><i class="fas fa-chalkboard-teacher"></i></a>
    </div>
  `,
  styleUrls: ['./orphan-management.component.scss']
})
export class OrphanFormTextStatusComponent implements ViewCell, OnInit {

  renderValue: string;

  constructor(private modalService: NgbModal, private http: GlobalHttpService, public func: GlobalFunctionService, private loading: Ng4LoadingSpinnerService) { }

  @Input() value: string | number;
  @Input() rowData: any;
  @Output() view: EventEmitter<any> = new EventEmitter();
  @Output() statusOrphan: EventEmitter<any> = new EventEmitter();


  ngOnInit() {
    this.renderValue = this.value.toString().toUpperCase();
  }

  async onStatus() {
    this.loading.show();
    this.statusOrphan.emit(this.rowData);
    let result = await this.http.post('api/orphan/getorphanstatus', { orphanid: this.rowData.orphanid });
    const activeModal = this.modalService.open(ModalAddStatusOrphanComponent, { size: 'lg', backdrop: 'static', keyboard: true, container: 'nb-layout' });
    activeModal.componentInstance.model = this.func.cloneObject(result.body);
    activeModal.result.then((result) => {
      if (result && result.success) {
        this.statusOrphan.emit(this.rowData);
      }
    });
    this.loading.hide();
  }
}