import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { GlobalFunctionService } from '../../../../shares/global-function.service';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { DialogBoxService } from '../../../../shares/dialog-box/dialog-box.service';

@Component({
  selector: 'ngx-modal-list-patron',
  templateUrl: './modal-list-patron.component.html',
  styleUrls: ['./modal-list-patron.component.scss']
})
export class ModalListPatronComponent implements OnInit {

  constructor(private activeModal: NgbActiveModal, public fn: GlobalFunctionService, private loading: Ng4LoadingSpinnerService, private dialog: DialogBoxService) { }

  ngOnInit() {
  }

  onSave() {
    alert('On Save');
  }

  onClose() {
    this.loading.show();
    this.dialog.confirm({ title: 'ยืนยัน', message: 'ต้องการยกเลิกหรือไม่' }, (ans) => {
      if (ans) {
        this.activeModal.close();
      }
    });
    this.loading.hide();
  }

}
