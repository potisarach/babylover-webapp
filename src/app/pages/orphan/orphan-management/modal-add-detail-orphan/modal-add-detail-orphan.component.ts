import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { GlobalApiService } from '../../../../shares/global-api.service';
import { GlobalHttpService } from '../../../../shares/global-http.service';
import { count } from 'rxjs/operators';
import { DialogBoxService } from '../../../../shares/dialog-box/dialog-box.service';
import { GlobalFunctionService } from '../../../../shares/global-function.service';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

declare var $: any;

@Component({
    selector: 'ngx-modal-add-detail-orphan',
    templateUrl: './modal-add-detail-orphan.component.html',
    styleUrls: ['./modal-add-detail-orphan.component.scss']
})
export class ModalAddDetailOrphanComponent implements OnInit {

    public formVaccine: Array<any>;
    public itab: number = 1;
    public chkForm: any;
    public valueitemid: any;
    public valueitemdate: any;

    public model = {
        orphanid: "",
        citizenid: "",
        genderid: "",
        prefixid: "",
        firstname: "",
        lastname: "",
        birthdate: "",
        bornlocation: "",
        fathername: "",
        mathername: "",
        historybefore: "",
        bodygrowth: "",
        emotionsocial: "",
        languagelearn: "",
        planworkwithorphan: "",
        vaccine: [

        ]
    }
    public genderList: Array<any> = [];
    public prefixList: Array<any> = [];
    public nationalityList: Array<any> = [];
    public raceList: Array<any> = [];
    public religionList: Array<any> = [];
    public educaionList: Array<any> = [];
    public provinceList: Array<any> = [];
    public occupationList: Array<any> = [];
    public amphurList: Array<any> = [];
    public tumbolList: Array<any> = [];
    public vaccineList: Array<any> = [];
    public isCheck: boolean = false;
    public datenow: any;
    public minDate: any;
    public ischkdate: boolean;
    public vaccineid: any;
    public isCheckVaccine: boolean = true;


    form: FormGroup;

    constructor(private activeModal: NgbActiveModal, private dropdownApi: GlobalApiService, private apiHttp: GlobalHttpService, private dialog: DialogBoxService, public func: GlobalFunctionService, private loading: Ng4LoadingSpinnerService) {
        this.form = new FormGroup({
            'citizenid': new FormControl('', [Validators.required, Validators.minLength(13), Validators.maxLength(13), Validators.pattern('^[0-9]+$')]),
            'genderid': new FormControl('', [Validators.required]),
            'prefixid': new FormControl('', [Validators.required]),
            'firstname': new FormControl('', [Validators.required, Validators.maxLength(50)]),
            'lastname': new FormControl('', [Validators.required, Validators.maxLength(50)]),
            'birthdate': new FormControl('', [Validators.required]),
            'bornlocation': new FormControl('', [Validators.required, Validators.maxLength(500)]),
            'fathername': new FormControl('', [Validators.required, Validators.maxLength(100)]),
            'mathername': new FormControl('', [Validators.required, Validators.maxLength(100)]),
            'historybefore': new FormControl('', [Validators.required]),
            'bodygrowth': new FormControl('', [Validators.required]),
            'emotionsocial': new FormControl('', [Validators.required]),
            'languagelearn': new FormControl('', [Validators.required]),
            'planworkwithorphan': new FormControl('', [Validators.required]),
        })
        this.vaccineList = [1];
        let toDay = new Date();
        let date = toDay.getDate();
        let month = toDay.getMonth() + 1;
        let yyyy = toDay.getFullYear();
        let dd: any;
        let mm: any;
        if (date <= 9) {
            dd = '0' + date;
        } else {
            dd = date;
        }
        if (month <= 9) {
            mm = '0' + month;
        } else {
            mm = month;
        }

        this.datenow = yyyy + '-' + mm + '-' + dd;
    }

    ngOnInit() {
        this.minDate = this.model.birthdate;
        this.loading.show();
        this.setupDropdown();
        this.setPrefix();
        for (let i = 0; i < this.model.vaccine.length; i++) {
            let ix = i;
            this.form.addControl("vaccineid" + ix, new FormControl('', [Validators.required]));
            this.form.addControl("vaccinedate" + ix, new FormControl('', [Validators.required]));
        }
        this.loading.hide();
    }

    isError(control: FormControl, errorKey: string) {
        if (control.invalid && control.touched || this.isCheck) {
            return control.errors || false;
        }
        return false;
    }
    closeModal() {
        this.dialog.confirm({ title: 'ยืนยัน', message: 'ต้องการยกเลิกใช่หรือไม่' }, (ans) => {
            if (ans) {
                this.activeModal.close();
            }
        });
    }

    async onSaveDetailOrphan(chkForm) {
        this.isCheck = true;
        console.log(this.model);

        if (chkForm == false) {
            this.dialog.alert({ title: 'แจ้งเตือน', message: 'กรุณากรอกข้อมูลให้สมบูรณ์' });
        } else {
            this.loading.show();
            let result: any = await this.apiHttp.post('api/orphan/evaluation/addorphandetail', this.model);
            this.loading.hide();
            this.dialog.alert({ title: 'แจ้งเตือน', message: 'บันทึกข้อมูลสำเร็จ' });
            this.activeModal.close(result);
        }
    }
    setMinDateVaccine() {
        let vaccineData = [];

        for (let vaccinedata of this.model.vaccine) {
            let obj = {
                vaccineid: vaccinedata.vaccineid,
                vaccinedate: vaccinedata.vaccinedate
            }
            if (vaccinedata.vaccinedate != "") {
                this.minDate = vaccinedata.vaccinedate;
                this.vaccineid = vaccinedata.vaccineid;
            }
        }
    }
    addFormVaccine() {
        let i: any;
        for (let item of this.model.vaccine) {
            if (item.vaccineid != "" && item.vaccinedate != "") {
                if (item.vaccineid == this.valueitemid && item.vaccinedate == this.valueitemdate ) {
                    this.valueitemid = item.vaccineid;
                    this.valueitemdate = item.vaccinedate;
                    this.isCheckVaccine = false;

                } else {
                    this.valueitemid = item.vaccineid;
                    this.valueitemdate = item.vaccinedate;
                    this.isCheckVaccine = true;
                }
            } else {
                this.isCheckVaccine = false;
            }
        }

        if (this.isCheckVaccine == true) {
            if (this.vaccineid != "") {
                let obj = {
                    vaccineid: "",
                    vaccinedate: ""
                }
                this.model.vaccine.push(obj);
                for (let i = 0; i < this.model.vaccine.length; i++) {
                    let count = i;
                    this.form.addControl("vaccineid" + count, new FormControl('', [Validators.required]));
                    this.form.addControl("vaccinedate" + count, new FormControl('', [Validators.required]));
                    console.log("Length form add control => ", count);
                }
                this.setMinDateVaccine();
            }
            // this.isCheckVaccine = false;
        } else {
            this.dialog.alert({ title: 'แจ้งเตือน', message: 'กรุณากรอกข้อมูลให้สมบูรณ์' });
        }

    }
    delFormVaccine(index) {
        let valuefor;
        for (let i = 0; i < this.model.vaccine.length; i++) {
            let count = i;
            this.form.removeControl("vaccineid" + count);
            this.form.removeControl("vaccinedate" + count);

            if (i == 0) {
                this.minDate = this.model.birthdate;
                this.isCheckVaccine = true;
            }
            valuefor = i;
        }
        if (valuefor == 0) {
            this.minDate = this.model.birthdate;
            this.isCheckVaccine = true;
        }

        this.model.vaccine.splice(index, 1);

        let vaccineData = [];
        for (let vaccinedata of this.model.vaccine) {
            let obj = {
                vaccineid: vaccinedata.vaccineid,
                vaccinedate: vaccinedata.vaccinedate
            }
            vaccineData.push(obj);
            console.log("B");

        }

        for (let i = 0; i < this.model.vaccine.length; i++) {
            let count = i;
            this.form.addControl("vaccineid" + count, new FormControl('', [Validators.required]));
            this.form.addControl("vaccinedate" + count, new FormControl('', [Validators.required]));
            if (i == 0) {
                this.minDate = this.model.birthdate;
                this.isCheckVaccine = true;

            }

        }

        this.model.vaccine = [];
        for (let data of vaccineData) {
            let obj = {
                vaccineid: data.vaccineid,
                vaccinedate: data.vaccinedate
            }
            this.model.vaccine.push(obj);
            this.setMinDateVaccine();
            if (valuefor == 0) {
                this.minDate = this.model.birthdate;
                this.isCheckVaccine = true;

            }


        }

    }

    async setupDropdown() {
        this.loading.show();
        this.genderList = await this.dropdownApi.getGenderList();
        this.nationalityList = await this.dropdownApi.getNationalityList();
        this.raceList = await this.dropdownApi.getRaceList();
        this.religionList = await this.dropdownApi.getReligionList();
        this.educaionList = await this.dropdownApi.getEducationList();
        this.provinceList = await this.dropdownApi.getProvinceList();
        this.occupationList = await this.dropdownApi.getOccupationList();
        this.vaccineList = await this.dropdownApi.getVaccineList();
        this.loading.hide();
    }

    async getProvinceList() {
        this.loading.show();
        this.amphurList = [];
        this.tumbolList = [];
        this.provinceList = await this.dropdownApi.getProvinceList();

        this.getAmphurList();
        this.getTumbolList();
        this.loading.hide();
    }
    async getAmphurList() {
        this.loading.show();
        this.amphurList = [];
        this.tumbolList = [];
        this.loading.hide();
    }
    async getTumbolList() {
        this.loading.show();
        this.tumbolList = [];
        this.loading.hide();
    }

    tab1(itab) {
        if (itab != 1) {
            this.itab = 1;
        }
    }
    tab2(itab) {
        if (itab != 2) {
            this.itab = 2;
        }
    }
    tab3(itab) {
        for (let item in this.model) {
            let Model = `${item} = ${this.model[item]}`;
            console.log(Model);

            if (this.model[item] == null || this.model[item] == "" && Model == 'orphanhistoryid = null') {
                console.log("No Pass");
                this.chkForm = false;
                break;
            }
            if (this.model[item] != null || this.model[item] != "" && Model == 'orphanhistoryid = null') {
                console.log("Pass");
                this.chkForm = true;
                // break;
            }
        }
        console.log(this.chkForm);
        if (itab != 3) {
            this.itab = 3;
        }
    }

    onChkForm() {
        for (let item in this.model) {
            let Model = `${item} = ${this.model[item]}`;

            if (this.model[item] == null || this.model[item] == "" && Model != 'orphanhistoryid = null') {
                this.chkForm = false;
            }
            if (this.model[item] != null || this.model[item] != "" && Model == 'orphanhistoryid = null') {
                this.chkForm = true;
            }
        }
        console.log(this.chkForm);
    }

    async setPrefix() {
        if (this.model.genderid) {
            let control = this.form.get('prefixid');
            control.enable();
            this.prefixList = await this.dropdownApi.getPrefixList(this.model.genderid);
        } else {
            this.model.prefixid = "";
            let control = this.form.get('prefixid');
            control.disable();
        }
    }

    // ไม่ให้พิมพ์ตัวอักษร
    chkChar($event) {
        let charCode = $event.which || $event.keyCode;

        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        } else {
            return true;
        }
    }

    chkDate() {
        if (this.model.birthdate > this.datenow) {
            this.ischkdate = true;
        } else {
            this.ischkdate = false;
        }
    }

    // ไม่ให้พิมพ์ตัวเลข
    chkNum($event) {
        let charCode = $event.which || $event.keyCode;

        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return true;
        } else {
            return false;
        }
    }
}
