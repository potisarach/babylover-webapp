import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { GlobalApiService } from '../../../../shares/global-api.service';
import { GlobalHttpService } from '../../../../shares/global-http.service';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { GlobalFunctionService } from '../../../../shares/global-function.service';
import { LocalDataSource, ViewCell } from 'ng2-smart-table';
import { DialogBoxService } from '../../../../shares/dialog-box/dialog-box.service';
import { ModalPatronFormComponent } from '../../../patron/patron-management/modal-patron-form/modal-patron-form.component';
import { ModalListPatronComponent } from '../modal-list-patron/modal-list-patron.component';

declare var $: any;

@Component({
    selector: 'ngx-modal-add-status-orphan',
    templateUrl: './modal-add-status-orphan.component.html',
    styleUrls: ['./modal-add-status-orphan.component.scss']
})
export class ModalAddStatusOrphanComponent implements OnInit {

    public chkForm: any;
    public model = {
        citizenid: "",
        detail: "",
        orphanid: "",
        orphanstatusid: "",
        statusid: "",
        genderid: "",
        prefixid: "",
        fullname: "",
        patronageid: "",
        agestartorphan: "",
        ageendorphan: "",
        orphanstatus: "",
        namePatron: "",
        nameOrphan: "",
        agepatron: "",
        patronid: "",
        birthdate: "",
        homeno: "",
        moono: "",
        soi: "",
        road: "",
        tumbolname: "",
        amphurname: "",
        provincename: "",
        zipcode: "",
        fullnameorphan: "",
        nicknameorphan: "",
        birthdateorphan: "",
        ageorphan: "",
        personid: "",
        relationship: "",
        fosterstatusid: "",
        fosterid: "",
        originalfamilyid: "",
        orphanpersonid: "",
        fosterperson: "",
        originalfamilyperson: "",
        provincecode: "",
        amphurcode: "",
        tumbolcode: "",
        firstname: "",
        lastname: "",
        joinfamilydate: ""

    }
    public modelfoster = {
        fosterid: "",
        personid: "",
        citizenid: "",
        firstname: "",
        lastname: "",
        nickname: "",
        genderid: "",
        prefixid: "",
        birthdate: "",
        fosterstatusid : "",
        occupcode: "",
        racecode: "",
        nationcode: "",
        religioncode: "",
        educationcode: "",
        schoolname: "",
        homeno: "",
        moono: "",
        road: "",
        soi: "",
        tumbolcode: "",
        amphurcode: "",
        provincecode: "",
        zipcode: "",
        orphanid: "",
        fullname: "",
        Age: ""
    }
    public modeloriginalfamily = {
        originalfamilyid: "",
        fullname: "",
        birthdate: "",
        Age: "",
        relationship: "",
        personid: "",
        citizenid: "",
        firstname: "",
        lastname: "",
        nickname: "",
        genderid: "",
        prefixid: "",
        occupcode: "",
        racecode: "",
        nationcode: "",
        religioncode: "",
        educationcode: "",
        schoolname: "",
        homeno: "",
        moono: "",
        road: "",
        soi: "",
        tumbolcode: "",
        amphurcode: "",
        provincecode: "",
        zipcode: "",
        orphanid: "",
        tumbolname: "",
        amphurname: "",
        provincename: "",
    }

    public orphanStatusList: Array<any>;
    public genderList: Array<any>;
    public prefixList: Array<any>;
    public provinceList: Array<any> = [];
    public amphurList: Array<any> = [];
    public tumbolList: Array<any> = [];
    public isCheck: boolean = false;
    public datasource: LocalDataSource;
    public status: number = 0;
    public itab: number = 1;
    public tbSettings: any;
    public foster: any;
    public fosterforeign:any;
    public originalfamily: any;
    public isDuplicate: boolean;
    public ischkdate: boolean;



    form: FormGroup;
    datenow: string;

    constructor(public func: GlobalFunctionService, private api: GlobalApiService, private activeModal: NgbActiveModal, private dropdownApi: GlobalApiService, private apiHttp: GlobalHttpService, private loading: Ng4LoadingSpinnerService, private dialog: DialogBoxService, private modalService: NgbModal) {
        this.datasource = new LocalDataSource();
        this.setColumns();
        this.form = new FormGroup({
            'detail': new FormControl('', [Validators.required]),
            'status': new FormControl('', [Validators.required])
        });

        let toDay = new Date();
        let date = toDay.getDate();
        let month = toDay.getMonth() + 1;
        let yyyy = toDay.getFullYear();
        let dd: any;
        let mm: any;
        if (date <= 9) {
          dd = '0' + date;
        } else {
          dd = date;
        }
        if (month <= 9) {
          mm = '0' + month;
        } else {
          mm = month;
        }
    
        this.datenow = yyyy + '-' + mm + '-' + dd;
    
    }

    ngOnInit() {
        console.log("Model >>> ",this.model.provincecode);
        this.model.provincecode = "";
        this.loading.show();
        this.setupDropdown();
        this.loading.hide();
        this.onChangeStatus();
        this.loading.hide();
        if (this.model.patronid) {
            this.onSetbirthdate(this.model.birthdate);
        }
        this.getProvinceList();
        this.setClear();
        
        
    }

    setForm() {
        this.form.addControl("prefixid", new FormControl('', [Validators.required]));
        this.form.addControl("firstname", new FormControl('', [Validators.required, Validators.maxLength(50)]));
        this.form.addControl("lastname", new FormControl('', [Validators.required, Validators.maxLength(50)]));
        this.form.addControl("birthdate", new FormControl('', [Validators.required]));
        this.form.addControl('genderid', new FormControl('', [Validators.required]));
        this.form.addControl('homeno', new FormControl('', [Validators.required]));
        this.form.addControl('moono', new FormControl('', [Validators.required]));
        this.form.addControl('soi', new FormControl('', [Validators.required]));
        this.form.addControl('road', new FormControl('', [Validators.required]));
        this.form.addControl('provincecode', new FormControl('', [Validators.required]));
        this.form.addControl('amphurcode', new FormControl('', [Validators.required]));
        this.form.addControl('tumbolcode', new FormControl('', [Validators.required]));
        this.form.addControl('zipcode', new FormControl('', [Validators.required]));

    }
    setRelationship(){
        this.form.addControl('relationship', new FormControl('', [Validators.required]));
    }
    setCitienid(){
        this.form.addControl('citizenid', new FormControl('', [Validators.required, Validators.maxLength(13), Validators.minLength(13), Validators.pattern('^[0-9]+$')]));
    }



    removeForm() {
        this.form.removeControl('prefixid');
        this.form.removeControl('firstname');
        this.form.removeControl('lastname');
        this.form.removeControl('birthdate');
        this.form.removeControl('relationship');
        this.form.removeControl('citizenid');
        this.form.removeControl('genderid');
        this.form.removeControl('homemo');
        this.form.removeControl('moono');
        this.form.removeControl('soi');
        this.form.removeControl('road');
        this.form.removeControl('provincecode');
        this.form.removeControl('amphurcode');
        this.form.removeControl('tumbolcode');
        this.form.removeControl('zipcode');
    }
    ClearValidators(){
        this.form.get('prefixid').clearValidators();
        this.form.get('firstname').clearValidators();
        this.form.get('lastname').clearValidators();
        this.form.get('birthdate').clearValidators();
        this.form.get('relationship').clearValidators();
        this.form.get('citizenid').clearValidators();
        this.form.get('genderid').clearValidators();
        this.form.get('homeno').clearValidators();
        this.form.get('moono').clearValidators();
        this.form.get('soi').clearValidators();
        this.form.get('provincecode').clearValidators();
        this.form.get('amphurcode').clearValidators();
        this.form.get('tumbolcode').clearValidators();
        this.form.get('zipcode').clearValidators();
    }

    setColumns() {
        let _this = this;
        this.tbSettings = this.func.getTableSetting({
            fullname: {
                title: 'ชื่อ - นามสกุล',
                type: 'string',
                valuePrepareFunction: (cell, row) => {
                    return row.fullnamepatron;
                }
            },
            choose: {
                title: 'เลือก',
                type: 'custom',
                sort: false,
                renderComponent: PatronFormTbCheckboxComponent,
                onComponentInitFunction(instance) {
                    instance.view.subscribe(row => {
                        _this.doSetPatronid(row);
                    });
                }
            }
        });
    }


    onProvinceChange() {

        if (this.model.provincecode) {
            this.model.amphurcode = '';
            this.model.tumbolcode = '';
            let controlAmphur = this.form.get('amphurcode');
            let controlTumbol = this.form.get('tumbolcode');
            controlAmphur.enable();
            controlTumbol.disable();
            this.getAmphurList();
        } else if(this.model.provincecode == null){
            this.model.amphurcode = '';
            this.model.tumbolcode = '';
            let controlAmphur = this.form.get('amphurcode');
            let controlTumbol = this.form.get('tumbolcode');
            controlAmphur.disable();
            controlTumbol.disable();
        }else{
            this.model.amphurcode = '';
            this.model.tumbolcode = '';
            let controlAmphur = this.form.get('amphurcode');
            let controlTumbol = this.form.get('tumbolcode');
            controlAmphur.disable();
            controlTumbol.disable();
    }
}

    onAmphurChange() {
        if (this.model.amphurcode) {
            this.loading.show();
            this.getTumbolList();
            this.loading.hide();
            let controlTumbol = this.form.get('tumbolcode');
            controlTumbol.enable();
        } else {
            this.model.tumbolcode = '';
            let controlTumbol = this.form.get('tumbolcode');
            controlTumbol.disable();
        }
    }


    async getProvinceList() {
        this.loading.show();
        this.provinceList = await this.dropdownApi.getProvinceList();
        this.getAmphurList();
        this.loading.hide();
    }
    async getAmphurList() {
        this.loading.show();
        if (!this.model.provincecode) {
            return;
        }
        this.amphurList = await this.dropdownApi.getAmphurList(this.model.provincecode);
        this.getTumbolList();
        this.loading.hide();
    }
    async getTumbolList() {
        this.loading.show();
        this.tumbolList = [];
        if (!this.model.amphurcode) {
            return;
        }
        this.tumbolList = await this.dropdownApi.getTumbolList(this.model.amphurcode);
        this.loading.hide();
    }


    doSetPatronid(row) {
        this.model.patronid = row.patronid;
        this.model.fullname = row.fullnamepatron;
        this.model.agepatron = row.agepatron;
        this.model.homeno = row.homeno;
        this.model.moono = row.moono;
        this.model.tumbolname = row.tumbolname;
        this.model.amphurname = row.amphurname;
        this.model.provincename = row.provincename;
        this.onSetbirthdate(row.birthdate);
    }

    onSetbirthdate(fullbirthdate) {

        let year = fullbirthdate.substr(0, 4);
        let month = fullbirthdate.substr(5, 2);
        let day = fullbirthdate.substr(8, 2);
        let y;
        let m;
        let d;

        if (month == "01") {
            m = "มกราคม";
        } else if (month == "02") {
            m = "กุมภาพันธ์";
        } else if (month == "03") {
            m = "มีนาคม";
        } else if (month == "04") {
            m = "เมษายน";
        } else if (month == "05") {
            m = "พฤษภาคม";
        } else if (month == "06") {
            m = "มิถุนายน";
        } else if (month == "07") {
            m = "กรกฎาคม";
        } else if (month == "08") {
            m = "สิงหาคม";
        } else if (month == "09") {
            m = "กันยายน";
        } else if (month == "10") {
            m = "ตุลาคม";
        } else if (month == "11") {
            m = "พฤศจิกายน";
        } else if (month == "12") {
            m = "ธันวาคม";
        }

        let years: any = parseInt(year) + 543;
        y = years;

        if (day < "10") {
            d = day.substr(1, 1);
        } else {
            d = day;
        }
        if (this.modelfoster.fosterid) {
            this.modelfoster.birthdate = d + " " + m + " " + y;
        } else if (this.modeloriginalfamily.originalfamilyid) {
            this.modeloriginalfamily.birthdate = d + " " + m + " " + y;
        }
        else {
            this.model.birthdate = d + " " + m + " " + y;
        }

    }

    async setupDropdown() {
        this.loading.show();
        this.orphanStatusList = await this.dropdownApi.getOrphanstatusList();
        this.genderList = await this.dropdownApi.getGenderList();
        this.loading.hide();
    }

    async setPrefix(genderid) {
        
        if (genderid) {
            this.prefixList = await this.dropdownApi.getPrefixList(genderid );
            let control = this.form.get('prefixid');
            control.enable();
        } else {
            this.model.genderid = "";
            this.model.prefixid = "";
            let control = this.form.get('prefixid');
            control.disable();
        }
    }

    isError(control: FormControl, errorKey: string) {
        if (control.invalid && control.touched || this.isCheck) {
            return control.errors || false;
        }
        return false;
    }

    async onBlur() {
        this.loading.show();
        this.isDuplicate = false;
        let citizenCheck;
        let personid = this.model.personid;
        let citizenid = this.model.citizenid;
        if (!this.model.personid) {
            citizenCheck = await this.apiHttp.post('api/validate/checkduplicate/citizenidall');
            for (let item of citizenCheck.body) {
                if (item.citizenid == this.model.citizenid) {
                    this.isDuplicate = true;
                }
            }
        } else {
            this.isDuplicate = false;
            citizenCheck = await this.apiHttp.post('api/validate/checkduplicate/citizenid', { "personid": personid, "citizenid": citizenid });
            for (let item of citizenCheck.body) {
                if (item.citizenid == this.model.citizenid) {
                    this.isDuplicate = true;
                }
            }
        }
        this.loading.hide();
    }
    // บันทึกข้อมูลสถานภาพเด็ก
    async onSave() {
        this.loading.show();
        let Model = this.model;
        if (this.status == 0) {
            this.isCheck = true;
            this.removeForm();
            if (this.form.invalid) {
                this.dialog.alert({ title: 'แจ้งเตือน',message: "กรุณากรอกข้อมูลให้สมบูรณ์" });
            } else {
                let result: any = await this.apiHttp.post('api/orphan/updateorphanstatus', this.model);
                this.dialog.alert({ title: 'แจ้งเตือน', message: "บันทึกข้อมูลสำเร็จ" });
                this.activeModal.close(result);
                this.onSearchOrphan();
            }
        } else if (this.status == 1) {
            if (Model.patronid != null || Model.patronid != "") {
                console.log('delete patroneage & save staus =>', this.model);
                let joinfamilydate;
                joinfamilydate = new Date().toISOString().slice(0,10); 
                this.model.joinfamilydate = joinfamilydate;
                let result: any = await this.apiHttp.post('api/orphan/updateorphanstatus', this.model);
                this.dialog.alert({ title: 'แจ้งเตือน', message: "บันทึกข้อมูลสำเร็จ" });
                this.activeModal.close(result);
                this.onSearchOrphan();
            } else {
                this.dialog.alert({ title: 'แจ้งเตือน', message: "กรุณาเลือกผู้อุปถัมภ์" });
            }
        }
        this.loading.hide();
    }

    async onSaveFoster() {
        let citizenCheck;
        let personid = this.model.personid;
        let citizenid = this.model.citizenid;
        if (!this.modelfoster.personid) {
            citizenCheck = await this.apiHttp.post('api/validate/checkduplicate/citizenidall');
            for (let item of citizenCheck.body) {
                if (item.citizenid == this.model.citizenid) {
                    this.isDuplicate = true;
                }
            }
        } else {
            this.isDuplicate = false;
            citizenCheck = await this.apiHttp.post('api/validate/checkduplicate/citizenid', { "personid": personid, "citizenid": citizenid });
            for (let item of citizenCheck.body) {
                if (item.citizenid == this.model.citizenid) {
                    this.isDuplicate = true;
                }
            }
        }
        if (!this.modelfoster.fosterid) {
            this.isCheck = true;
            if (this.form.invalid) {
                this.dialog.error({ title: 'แจ้งเตือน', message: "กรุณากรอกข้อมูลให้ครบ !" });
            } else {
                this.loading.show();
                let result: any = await this.apiHttp.post('api/foster/put', this.model);
                if (result.success) {
                    let result: any = await this.apiHttp.post('api/orphan/updateorphanstatus', this.model);
                    this.activeModal.close(result);
                    this.dialog.alert({ title: 'แจ้งเตือน', message: 'บันทึกข้อมูลผู้บุญธรรมสำเร็จ' });
                    this.onSearchOrphan();
                } else {
                    this.dialog.error({ title: 'แจ้งเตือน', message: 'ไม่สามารถบันทึกข้อมูลผู้บุญธรรมได้' }, () => {
                    });
                }
                this.loading.hide();
            }
        } else {
            if (this.foster) {
                this.removeForm();
                this.isCheck = true;
                if (this.form.invalid) {
                    this.dialog.error({ title: 'แจ้งเตือน', message: "กรุณากรอกข้อมูลให้ครบ !" });
                } else {
                    this.loading.show();
                    this.model.personid = this.modelfoster.personid;
                    let result: any = await this.apiHttp.post('api/orphan/updateorphanstatus', this.model);
                    if (result.success) {
                        this.dialog.alert({ title: 'แจ้งเตือน', message: 'บันทึกข้อมูลผู้บุญธรรมสำเร็จ' });
                        this.activeModal.close(result);
                        this.onSearchOrphan();
                    } else {
                        this.dialog.error({ title: 'แจ้งเตือน', message: 'ไม่สามารถบันทึกข้อมูลผู้บุญธรรมได้' });
                    }
                    this.loading.hide();
                }
            } else {
                this.isCheck = true;
                if (this.form.invalid) {
                    this.dialog.error({ title: 'แจ้งเตือน', message: "กรุณากรอกข้อมูลให้ครบ !" });
                } else {
                    this.loading.show();
                    this.model.personid = this.modelfoster.personid;
                    let result: any = await this.apiHttp.post('api/foster/put', this.model);
                    if (result.success) {
                        let result: any = await this.apiHttp.post('api/orphan/updateorphanstatus', this.model);
                        this.activeModal.close(result);
                        this.dialog.alert({ title: 'แจ้งเตือน', message: 'บันทึกข้อมูลผู้บุญธรรมสำเร็จ' });
                        this.onSearchOrphan();
                    } else {
                        this.dialog.error({ title: 'แจ้งเตือน', message: 'ไม่สามารถบันทึกข้อมูลผู้บุญธรรมได้' }, () => {
                        });
                    }
                    this.loading.hide();
                }
            }
        }
    }

    async onSaveOriginalFamily() {
        let citizenCheck;
        let personid = this.model.personid;
        let citizenid = this.model.citizenid;
        if (!this.modeloriginalfamily.personid) {
            citizenCheck = await this.apiHttp.post('api/validate/checkduplicate/citizenidall');
            for (let item of citizenCheck.body) {
                if (item.citizenid == this.model.citizenid) {
                    this.isDuplicate = true;
                }
            }
        } else {
            this.isDuplicate = false;
            citizenCheck = await this.apiHttp.post('api/validate/checkduplicate/citizenid', { "personid": personid, "citizenid": citizenid });
            for (let item of citizenCheck.body) {
                if (item.citizenid == this.model.citizenid) {
                    this.isDuplicate = true;
                }
            }
        }
        if (!this.modeloriginalfamily.originalfamilyid) {
            this.isCheck = true;
            if (this.form.invalid) {
                this.dialog.error({ title: 'แจ้งเตือน', message: "กรุณากรอกข้อมูลให้ครบ !" });
                console.log("Valid at => ", this.form.value);
            } else {
                this.loading.show();
                let result: any = await this.apiHttp.post('api/originalfamily/put', this.model);
                if (result.success) {
                    let result: any = await this.apiHttp.post('api/orphan/updateorphanstatus', this.model);
                    this.activeModal.close(result);
                    this.dialog.alert({ title: 'แจ้งเตือน', message: 'บันทึกข้อมูลสำเร็จ' });
                    this.onSearchOrphan();
                } else {
                    this.dialog.error({ title: 'แจ้งเตือน', message: 'ไม่สามารถบันทึกข้อมูลได้' });
                }
                this.loading.hide();
            }
        } else {
            if (this.originalfamily) {
                this.loading.show();
                this.model.personid = this.originalfamily.personid;
                let result: any = await this.apiHttp.post('api/orphan/updateorphanstatus', this.model);
                if (result.success) {
                    this.dialog.alert({ title: 'แจ้งเตือน', message: 'บันทึกข้อมูลครอบครับเดิมสำเร็จ' });
                    this.activeModal.close(result);
                    this.onSearchOrphan();
                } else {
                    this.dialog.error({ title: 'แจ้งเตือน', message: 'ไม่สามารถบันทึกข้อมูลครอบครับเดิมได้' });
                }
                this.loading.hide();
            } else {
                this.isCheck = true;
                if (this.form.invalid) {
                    this.dialog.error({ title: 'แจ้งเตือน', message: "กรุณากรอกข้อมูลให้ครบ !" });
                    console.log("Valid at => ", this.form.value);
                } else {
                    this.loading.show();
                    this.model.personid = this.modeloriginalfamily.personid;
                    let result: any = await this.apiHttp.post('api/originalfamily/put', this.model);
                    if (result.success) {
                        let result: any = await this.apiHttp.post('api/orphan/updateorphanstatus', this.model);
                        this.activeModal.close(result);
                        this.dialog.alert({ title: 'แจ้งเตือน', message: 'บันทึกข้อมูลครอบครับเดิมสำเร็จ' });
                        this.onSearchOrphan();
                    } else {
                        this.dialog.error({ title: 'แจ้งเตือน', message: 'ไม่สามารถบันทึกข้อมูลครอบครับเดิมได้' });
                    }
                    this.loading.hide();
                }
            }
        }
    }

    // เมื่อเปลี่ยนสถานะเด็ก
    async onChangeStatus() {
        if (this.model.statusid == "2") {
            this.status = 1;
        } else if (this.model.statusid == "3") {
            this.status = 2;
            this.model.fosterstatusid = "1";

        }else if (this.model.statusid == "4") {
            this.status = 3;
        } else if (this.model.statusid == "5") {
            this.status = 4;
            this.model.fosterstatusid = "2";
            
        }
        else {
            this.status = 0;
        }
    }


    // ยกเลิกการบันทึกข้อมูล
    closeModal() {
        this.dialog.confirm({ title: 'ยืนยัน', message: 'ต้องการยกเลิกใช่หรือไม่' }, (ans) => {
            if (ans) {
                this.activeModal.close();
            }
        });
    }
setClear(){
    this.model.firstname = "";
    this.model.lastname = "";
    this.model.homeno = "";
    this.model.moono = "";
    this.model.road = "";
    this.model.soi = "";
    this.model.zipcode = "";
}
    // ค้นหาเด็ก
    async onSearchOrphan() {
        this.loading.show();
        let result = await this.apiHttp.post('api/orphan/orphan/search', { fullname: "", agestartorphan: "", ageendorphan: "", orphanstatus: "" });
        if (result.success) {
            this.datasource = this.func.getTableDataSource(result.body);
        }
        this.loading.hide();
    }

    itab1(itab) {
        if (itab != 1) {
            this.itab = 1;   
            this.removeForm();
        }
        if(this.form.valid == false){
            for (const key in this.form.controls) {
                this.form.get(key).clearValidators();
                this.form.get(key).updateValueAndValidity();
           }
        }
    }
    itab2(itab) {
        if (itab != 2) {
            this.itab = 2;
            this.onSearchPatron();
        }
        this.removeForm();
    }
    itab3(itab) {
        if (itab != 3) {
            this.itab = 3;
        }
        this.removeForm();
        this.onGetFoster();
        this.setForm();
        this.setCitienid();
        this.setPrefix(this.model.genderid);
        this.onProvinceChange();
    }
    itab4(itab) {
        if (itab != 4) {
            this.itab = 4;
        }
        this.removeForm();
        this.onGetOriginalFamily();
        this.setForm();
        this.setCitienid();
        this.setRelationship();
        this.setPrefix(this.model.genderid);
        this.onProvinceChange();
    }
    itab5(itab) {
        if (itab != 5) {
            this.itab = 5;
        }
        this.removeForm();
        this.onGetFoster();
        this.setForm();
        this.setPrefix(this.model.genderid);
        this.onProvinceChange();
    }

    async onSearchPatron() {
        let patron;
        let orphan;
        if (this.model.namePatron) {
            patron = this.model.namePatron.trim();
            orphan = "";
        } else {
            patron = "";
            orphan = "";
        }
        this.loading.show();
        let result = await this.apiHttp.post('api/patronage/patron/searchpatron', { fullnamepatron: patron, fullnameorphan: orphan });
        if (result.success) {
            this.datasource = this.func.getTableDataSource(result.body);
        }
        this.loading.hide();
    }

    async onGetFoster() {
        this.loading.show();
        let orphanid = this.model.orphanid;
        let result = await this.apiHttp.post('api/foster/get', { orphanid: orphanid });
        this.modelfoster = result.body;
        
        if(this.modelfoster.fosterstatusid == "1"){
            if (this.modelfoster.fosterid == null) {
                this.foster = false;
            } else {
                this.foster = true;
                this.onSetbirthdate(this.modelfoster.birthdate);
            }
        }else if(this.modelfoster.fosterstatusid == "2"){
            if (this.modelfoster.fosterid == null) {
                this.fosterforeign = false;
            } else {
                this.fosterforeign = true;
                this.onSetbirthdate(this.modelfoster.birthdate);
            }

        }

        console.log("Status Foster ID > > >",this.modelfoster.fosterstatusid);
        
        this.loading.hide();
    }

    async onGetOriginalFamily() {
        this.loading.show();
        let orphanid = this.model.orphanid;
        let result = await this.apiHttp.post('api/originalfamily/get', { orphanid: orphanid });
        this.modeloriginalfamily = result.body;
        if (this.modeloriginalfamily.originalfamilyid == null) {
            this.originalfamily = false;
        } else {
            this.originalfamily = true;
            this.onSetbirthdate(this.modeloriginalfamily.birthdate);
        }
        this.loading.hide();
    }

    onReset() {
        this.model.namePatron = "";
        this.onSearchPatron();
    }

    onKeySearch($event) {
        var keycode = $event.which || $event.keycode;
        if (keycode == 13) {
            this.onSearchPatron();

        }
    }

    onUnSelectPatron() {
        this.model.patronid = "";
    }
    onUnSelectFoster() {
        this.foster = false;
        this.setPrefix(this.model.genderid);
    }
    onUnSelectorOriginal() {
        this.originalfamily = false;
    }

    onAddPatron() {
        this.loading.show();
        const activeModal = this.modalService.open(ModalPatronFormComponent, { size: 'lg', keyboard: false, backdrop: "static", container: 'nb-layout' });
        activeModal.componentInstance.model.fullnameorphan = this.model.fullnameorphan;
        activeModal.componentInstance.model.nicknameorphan = this.model.nicknameorphan;
        activeModal.componentInstance.model.birthdateorphan = this.model.birthdateorphan;
        activeModal.componentInstance.model.ageorphan = this.model.ageorphan;
        activeModal.result.then((result) => {
            if (result && result.success) {
            }
        });
        this.loading.hide();
        this.activeModal.close();
    }
    chkCitizenid($event){
        let charCode = $event.which || $event.keyCode;
        if(charCode > 31 && (charCode < 48 || charCode > 57)){
            return false;
        }else{
            return true;
        }
    }
    chkName($event){
        let charCode = $event.which || $event.keyCode;
        if(charCode > 31 && (charCode < 48 || charCode > 57)){
            return true;
        }else{
            return false;
        }
    }
    chkAddress($event){
        let charCode = $event.which || $event.keyCode;
        if(charCode > 31 && (charCode < 48 || charCode > 57)){
            return false;
        }else{
            return true;
        }
    }

    chkDate(){
        console.log("Date > > >",this.datenow);
        
        if(this.model.birthdate > this.datenow){
            this.ischkdate = true;
        }else{
            this.ischkdate = false;
        }
    }


}


@Component({
    selector: 'tb-patron-list-button-view',
    template: `<div class="text-center">
      <span class="fas fa-check-circle" (click)="choose()"></span>
    </div>
    `,
    styleUrls: ['./modal-add-status-orphan.component.scss']
})
export class PatronFormTbCheckboxComponent implements ViewCell, OnInit {
    renderValue: string;
    @Input() value: string | number;
    @Input() rowData: any;
    @Output() view: EventEmitter<any> = new EventEmitter();
    @Output() edit: EventEmitter<any> = new EventEmitter();
    model: any;
    datenow: any;
    ischkdate: boolean;

    constructor() { }

    ngOnInit() {
        this.renderValue = this.value.toString().toUpperCase();
    }

    choose() {
        this.view.emit(this.rowData);
    }
}
