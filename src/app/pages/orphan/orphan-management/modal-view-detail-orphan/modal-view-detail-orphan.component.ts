import { Component, OnInit } from '@angular/core';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { GlobalFunctionService } from '../../../../shares/global-function.service';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { GlobalHttpService } from '../../../../shares/global-http.service';
import { GlobalApiService } from '../../../../shares/global-api.service';

@Component({
  selector: 'ngx-modal-view-detail-orphan',
  templateUrl: './modal-view-detail-orphan.component.html',
  styleUrls: ['./modal-view-detail-orphan.component.scss']
})
export class ModalViewDetailOrphanComponent implements OnInit {
  
  public model = {
    fullnameorphan: "",
    nickname: "",
    birthdate:"",
    age: "",
    picturepath: "" ,
    orphanstatusname: "",
    gendername:""

  }

  public imagePreView = "";
  constructor(private modalService: NgbModal,private activeModal:NgbActiveModal, public func: GlobalFunctionService, private loading: Ng4LoadingSpinnerService, private http: GlobalHttpService, private dropdownApi: GlobalApiService) {



  }

  ngOnInit() {
    this.onSetbirthdate();
  }
  
  onSetbirthdate() {
    let fullbirthdate  = this.model.birthdate;
    let year = fullbirthdate.substr(0, 4);
    let month = fullbirthdate.substr(5, 2);
    let day = fullbirthdate.substr(8, 2);
    let y;
    let m;
    let d;

    if (month == "01") {
        m = "มกราคม";
    } else if (month == "02") {
        m = "กุมภาพันธ์";
    } else if (month == "03") {
        m = "มีนาคม";
    } else if (month == "04") {
        m = "เมษายน";
    } else if (month == "05") {
        m = "พฤษภาคม";
    } else if (month == "06") {
        m = "มิถุนายน";
    } else if (month == "07") {
        m = "กรกฎาคม";
    } else if (month == "08") {
        m = "สิงหาคม";
    } else if (month == "09") {
        m = "กันยายน";
    } else if (month == "10") {
        m = "ตุลาคม";
    } else if (month == "11") {
        m = "พฤศจิกายน";
    } else if (month == "12") {
        m = "ธันวาคม";
    }

    let years: any = parseInt(year) + 543;
    y = years;

    if (day < "10") {
        d = day.substr(1, 1);
    } else {
        d = day;
    }
  
        this.model.birthdate = d + " " + m + " " + y;
    

}

  close(){
    this.activeModal.close();
  }

}
