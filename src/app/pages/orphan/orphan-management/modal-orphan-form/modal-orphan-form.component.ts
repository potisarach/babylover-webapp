import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormControl, Validators, RequiredValidator } from '@angular/forms';
import { GlobalApiService } from '../../../../shares/global-api.service';
import { GlobalHttpService } from '../../../../shares/global-http.service';
import { DialogBoxService } from '../../../../shares/dialog-box/dialog-box.service';
import { GlobalFunctionService } from '../../../../shares/global-function.service';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

declare var $: any;

@Component({
  selector: 'ngx-modal-orphan-form',
  templateUrl: './modal-orphan-form.component.html',
  styleUrls: ['./modal-orphan-form.component.scss']
})
export class ModalOrphanFormComponent implements OnInit {

  public prefixList: Array<any>;
  public genderList: Array<any>;
  public OrphanstatusList: Array<any>
  public isCheck: boolean = false;
  public datenow: any;
  public isDuplicate: boolean;
  public isCheckDate: boolean;
  public selectedFile: File = null;
  imagePreView: string = '';
  public model = {
    personid: "",
    citizenid: "",
    genderid: "",
    prefixid: "",
    firstname: "",
    lastname: "",
    birthdate: "",
    nickname: "",
    orphanstatusid: "",
    picturepath: ""
  }

  form: FormGroup;


  constructor(private dialog: DialogBoxService,
    private activeModal: NgbActiveModal,
    private dropdownApi: GlobalApiService,
    private apiHttp: GlobalHttpService,
    public func: GlobalFunctionService,
    private loading: Ng4LoadingSpinnerService
  ) {
    this.form = new FormGroup({
      'citizenid': new FormControl('', [Validators.required, Validators.pattern('^[0-9]+$')]),
      'genderid': new FormControl('', [Validators.required]),
      'prefixid': new FormControl('', [Validators.required]),
      'firstname': new FormControl('', [Validators.required, Validators.maxLength(50)]),
      'lastname': new FormControl('', [Validators.required, Validators.maxLength(50)]),
      'birthdate': new FormControl('', [Validators.required]),
      'nickname': new FormControl('', [Validators.required, Validators.maxLength(50)]),
      'picturepath': new FormControl(null)
    })
    let toDay = new Date();
    let date = toDay.getDate();
    let month = toDay.getMonth() + 1;
    let yyyy = toDay.getFullYear();
    let dd: any;
    let mm: any;
    if (date <= 9) {
      dd = '0' + date;
    } else {
      dd = date;
    }
    if (month <= 9) {
      mm = '0' + month;
    } else {
      mm = month;
    }

    this.datenow = yyyy + '-' + mm + '-' + dd
  }

  ngOnInit() {
    this.loading.show();
    this.setupDropdown();
    this.onPrefixid();
    console.log("Model ===== ", this.model);

    this.loading.hide();
  }

  isError(control) {
    if (control.invalid && control.touched || this.isCheck) {
      // return errorKey in control.errors
      return control.errors || false;
    }
    return false;
  }

  async setupDropdown() {
    this.loading.show();
    this.genderList = await this.dropdownApi.getGenderList();
    this.OrphanstatusList = await this.dropdownApi.getOrphanstatusList();
    this.loading.hide();
  }

  closeModal() {
    this.dialog.confirm({ title: 'ยืนยัน', message: 'ต้องการยกเลิกใช่หรือไม่' }, (ans) => {
      if (ans) {
        this.activeModal.close();
      }
    });
  }

  async onFileSelected(event, input) {
    this.onConvertImage(input)
    console.log("Input > > >",input);
    
    console.log("Evn>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>", event);
    this.selectedFile = await <File>event.target.files[0];
    console.log("selectedFile", this.selectedFile);

  }
  onConvertImage(input: HTMLInputElement) {
    console.log("Value", this.form.value)
    console.log("INPUT>>>>>>>>>>>>>>>>" + input + "<<<<<<<<<<<<<<<<<<<<<");
    // const imageControl = this.form.controls['personimg'];
    // imageControl.setValue(null)
    console.log("HTMLInputElement = ", input);
    if (input.files.length == 0) return this.imagePreView = '';
    console.log("INPUTFile", input.files[0].type);
    const reader = new FileReader();
    reader.readAsDataURL(input.files[0]);
    reader.addEventListener('load', () => {

      this.imagePreView = reader.result.toString();
      console.log(this.model);

    })
  }
  
  async onSave() {
    this.loading.show();
    if (this.selectedFile) {
      console.log(this.selectedFile.size);
    }
    this.isCheck = true;
    this.model.picturepath = this.imagePreView
    console.log(this.model);

    if (this.form.invalid || this.isCheckDate == true) {
      this.dialog.error({ title: 'แจ้งเตือน', message: "กรุณากรอกข้อมูลให้สมบูรณ์" });
    } else {
      let result: any = await this.apiHttp.post('api/orphan/put', this.model);
      if (result.success) {
        this.activeModal.close({ success: true });
        this.dialog.alert({ title: 'แจ้งเตือน', message: "บันทึกข้อมูลสำเร็จ" });
      } else {
        this.dialog.error({ title: 'แจ้งเตือน', message: "ไม่สามารถบันทึกข้อมูลได้" });
      }
    }
    this.loading.hide();
  }
  async onBlur() {
    this.loading.show();
    this.isDuplicate = false;
    let citizenCheck;
    let personid = this.model.personid;
    let citizenid = this.model.citizenid;
    if (!this.model.personid) {
      console.log("! Personid");
      citizenCheck = await this.apiHttp.post('api/validate/checkduplicate/citizenidall');
      console.log(citizenCheck.body);
      for (let item of citizenCheck.body) {
        if (item.citizenid == this.model.citizenid) {
          //this.dialog.alert({ title: "เลขประจำตัวประชาชนซ้ำ", message: "เลขประจำตัวประชาชนซ้ำ" })
          this.isDuplicate = true;
        }
      }
    } else {
      this.isDuplicate = false;
      console.log("PersonID");
      citizenCheck = await this.apiHttp.post('api/validate/checkduplicate/citizenid', { "personid": personid, "citizenid": citizenid });
      console.log(citizenCheck.body);
      for (let item of citizenCheck.body) {
        if (item.citizenid == this.model.citizenid) {
          this.isDuplicate = true;
        }
      }
    }
    this.loading.hide();
  }

  async onPrefixid() {
    let genderid = this.model.genderid;
    if (genderid) {
      this.prefixList = await this.dropdownApi.getPrefixList(genderid);
      let control = this.form.get('prefixid');
      control.enable();
    } else {
      this.model.prefixid = "";
      let control = this.form.get('prefixid');
      control.disable();
    }
  }

  chkDate(){
    if(this.model.birthdate > this.datenow){
      this.isCheckDate = true;
              console.log("err");
    }else{
      this.isCheckDate = false;

    }

    
  }
  // เช็คตัวอักษรเลข ปชช.
  chkCitizenid($event) {
    let charCode = $event.which || $event.keyCode;

    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    } else {
      return true;
    }
  }

  // เช็คการกรอกชื่อ-นามสกุล - ชื่อเล่น
  chkName($event) {
    let charCode = $event.which || $event.keyCode;

    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return true;
    } else {
      return false;
    }
  }
    // เช็ค space bar
    chkSpacebar($event) {
      let charCode = $event.which || $event.keyCode;
  
      if (charCode == 32) {
        console.log("false > ", $event.keyCode);
        return false;
      } else {
        return true;
      }
    }
}
