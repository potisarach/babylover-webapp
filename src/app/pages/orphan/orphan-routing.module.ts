
import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { OrphanManagementComponent } from './orphan-management/orphan-management.component';

const routes: Routes = [
    {
        path: 'management',
        component: OrphanManagementComponent
    }
]
@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
  })
  export class OrphanRoutingModule {
  }