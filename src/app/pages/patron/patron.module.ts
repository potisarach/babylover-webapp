import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { PatronEvaluationComponent } from './patron-evaluation/patron-evaluation.component';
import { PatronRoutingModule } from './patron-routing.module';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { ThemeModule } from '../../@theme/theme.module';
import { PatronManagementComponent, PatronFormTbButtonViewComponent, PatronFormTbTextViewComponent,} from './patron-management/patron-management.component';
import {MatButtonModule, MatCheckboxModule, MatIconModule} from '@angular/material';
import { Ng4LoadingSpinnerModule } from 'ng4-loading-spinner';
import { ModalViewDetailPatronComponent } from './patron-management/modal-view-detail-patron/modal-view-detail-patron.component';
import { ModalPatronFormComponent, PatronFormTbCheckboxComponent } from './patron-management/modal-patron-form/modal-patron-form.component';
import { PatronFrom1Component } from "./patron-evaluation/patron-from1/patron-from1.component";
import { PatronFrom3Component } from "./patron-evaluation/patron-from3/patron-from3.component";
import { PatronFrom4Component } from './patron-evaluation/patron-from4/patron-from4.component';
import { PatronFrom5Component } from './patron-evaluation/patron-from5/patron-from5.component';
import { PatronFrom6Component } from './patron-evaluation/patron-from6/patron-from6.component';
import { PatronFrom7Component } from './patron-evaluation/patron-from7/patron-from7.component';
import { PatronFrom8Component } from './patron-evaluation/patron-from8/patron-from8.component';
import { PatronFrom9Component } from './patron-evaluation/patron-from9/patron-from9.component';
import { PatronFrom10Component } from './patron-evaluation/patron-from10/patron-from10.component';
@NgModule({
  imports: [
    CommonModule,
    PatronRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    Ng2SmartTableModule,
    ThemeModule,
    MatButtonModule,
    MatCheckboxModule,
    MatIconModule,
    Ng4LoadingSpinnerModule.forRoot()
  ],
  declarations: [
    PatronManagementComponent,
    PatronFormTbButtonViewComponent,
    PatronEvaluationComponent,
    PatronFormTbTextViewComponent,
    ModalViewDetailPatronComponent,
    ModalPatronFormComponent,
    PatronFrom1Component,
    PatronFrom3Component,
    PatronFrom4Component,
    PatronFrom5Component,
    PatronFrom6Component,
    PatronFrom7Component,
    PatronFrom8Component,
    PatronFrom9Component,
    PatronFrom10Component,
    PatronFormTbCheckboxComponent
    ]
  ,entryComponents:[
    PatronFormTbButtonViewComponent,
    PatronFormTbTextViewComponent,
    ModalViewDetailPatronComponent
  ]
})
export class PatronModule { }
