import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { LocalDataSource, ViewCell } from 'ng2-smart-table';
import { GlobalFunctionService } from '../../../shares/global-function.service';
import { GlobalHttpService } from '../../../shares/global-http.service';
import { DialogBoxService } from '../../../shares/dialog-box/dialog-box.service';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { Router, RouterLink } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ModalPatronFormComponent } from './modal-patron-form/modal-patron-form.component';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ModalViewDetailPatronComponent } from './modal-view-detail-patron/modal-view-detail-patron.component';

declare var $: any;

@Component({
  selector: 'ngx-patron-management',
  templateUrl: './patron-management.component.html',
  styleUrls: ['./patron-management.component.scss']
})
export class PatronManagementComponent implements OnInit {

  public action: string;
  public model = {
    namePatron: "",
    nameOrphan: ""
  }

  public datasource: LocalDataSource;
  public tbSettings: any;
  constructor(private route: Router, private http: GlobalHttpService, public func: GlobalFunctionService, private dialog: DialogBoxService, private loading: Ng4LoadingSpinnerService, private modalService: NgbModal) {
    this.datasource = new LocalDataSource();
    this.setColumns();
  }

  ngOnInit() {
    this.onSearchPatron();
  }

  setColumns() {
    let _this = this;
    this.tbSettings = this.func.getTableSetting({
      fullname: {
        title: 'ชื่อ - นามสกุล',
        width: '24%',
        type: 'string',
        valuePrepareFunction: (cell, row) => {
          return row.fullnamepatron;
        }
      },
      age: {
        title: 'อายุ (ปี)',
        width: '12%',
        type: 'html',
        valuePrepareFunction: (cell, row) => {
          let dateNow: any = new Date();
          let birthdate = row.birthdate.substr(0, 4);
          let age = dateNow.getFullYear() - birthdate
          return '<div class="text-center">' + age + '</div>';
        }
      },
      orphan: {
        title: 'เด็กในอุปถัมภ์',
        width: '24%',
        type: 'html',
        valuePrepareFunction: (cell, row) => {
          if (!row.firstnameorphan && !row.lastnameorphan) {
            return '<div class="text-center">--ไม่มีการอุปถัมภ์--</div>'
          } else {
            return '<div class="text-center">'+row.fullnameorphan+'</div>';
          }
        }
      },
      evalute: {
        title: 'ประเมินสภาพความเป็นอยู่',
        width: '20%',
        type: 'custom',
        renderComponent: PatronFormTbTextViewComponent,
        onComponentInitFunction(instance) {
          instance.edit.subscribe(row => {
            console.log(row);
            // _this.doEvalute(row);
          });
        }
      },
      action: {
        title: 'จัดการ',
        width: '19%',
        sort: false,
        type: 'custom',
        renderComponent: PatronFormTbButtonViewComponent,
        onComponentInitFunction(instance) {
          instance.view.subscribe(row => {
            console.log(row);
            _this.doView(row);
          });
          instance.edit.subscribe(row => {
            console.log(row);
            _this.doEdit(row);
          });
        }
      }
    });
  }
  addPatronList() {
    this.loading.show();
    const activeModal = this.modalService.open(ModalPatronFormComponent, { size: 'lg', keyboard: false, backdrop: "static", container: 'nb-layout' });
    activeModal.result.then((result) => {
      console.log(result);
      if (result && result.success) {
        this.onSearchPatron();
      }
    });
    this.loading.hide();
  }
  async doView(rowData){
    this.loading.show();
    let patron = await this.http.post('api/patronage/patron/get', { "patronid": rowData.patronid });
    if (patron.status != 'success') {
      // return;
    }
    const activeModal = this.modalService.open(ModalViewDetailPatronComponent, { size: 'lg', keyboard: false, backdrop: "static", container: 'nb-layout' });
    activeModal.componentInstance.model = this.func.cloneObject(patron.body);
    activeModal.result.then((result) => {
      console.log(result);

      if (result && result.success) {
        this.onSearchPatron();
      }
    });
    this.loading.hide();

  }
  async doEdit(rowData) {
    this.loading.show();
    let patron = await this.http.post('api/patronage/patron/get', { "patronid": rowData.patronid });
    if (patron.status != 'success') {
      return;
    }
    const activeModal = this.modalService.open(ModalPatronFormComponent, { size: 'lg', keyboard: false, backdrop: "static", container: 'nb-layout' });
    activeModal.componentInstance.model = this.func.cloneObject(patron.body);
    activeModal.result.then((result) => {
      console.log(result);

      if (result && result.success) {
        this.onSearchPatron();
      }
    });
    this.loading.hide();
  }

  async onSearchPatron() {
    let patron = this.model.namePatron.trim();
    let orphan = this.model.nameOrphan.trim();
    this.loading.show();
    let result = await this.http.post('api/patronage/patron/search', { fullnamepatron: patron, fullnameorphan: orphan });
    
    if (result.success) {
      this.datasource = this.func.getTableDataSource(result.body);
    }
    this.loading.hide();
  }

  async onReset() {
    this.model.namePatron = "";
    this.model.nameOrphan = "";
    this.onSearchPatron();
  }

  onDetailPatron() {
    this.loading.show();
    console.log("Detail Patron");
    this.loading.hide();
  }

  onEnter($event) {
    var keycode = $event.which || $event.keyCode;
    if (keycode == 13) {
      this.onSearchPatron();
    }
  }

  // เช็คการพิมพ์ตัวเลข(พิมพ์ได้เฉพาะตัวอักษร)
  chkNum($event) {
    let charCode = $event.which || $event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return true;
    } else {
      return false;
    }
  }

}


@Component({
  selector: 'tb-patron-list-button-view',
  template: `<div class="text-center">
    <a (click)="onVeiw()" class="button-view" title="ดูข้อมูล่วนตัว"><i class="far fa-eye icon"></i></a>&nbsp;
    <a (click)="onEdit()" class="button-view" title="แก้ไขข้อมูลส่วนตัว"><i class="fas fa-user-edit icon"></i></a>&nbsp;
    <a href="{{func.getHostAPI()}}report/patron/?patronid={{rowData.patronid}}" class="button-view" title="ดาวน์โหลดข้อมูล"><i class="fas fa-file-download icon"></i></a>&nbsp;
    </div>
  `,
  styleUrls: ['./patron-management.component.scss']
})
export class PatronFormTbButtonViewComponent implements ViewCell, OnInit {
  renderValue: string;
  @Input() value: string | number;
  @Input() rowData: any;
  @Output() view: EventEmitter<any> = new EventEmitter();
  @Output() edit: EventEmitter<any> = new EventEmitter();

  constructor(public func: GlobalFunctionService) { }

  ngOnInit() { //ฟังก์ชันแรกที่ทำงาน
    this.renderValue = this.value.toString().toUpperCase();
  }

  onEdit() {
    this.edit.emit(this.rowData);
  }
  onVeiw() {
    this.view.emit(this.rowData);
    // alert("ดูข้อมูลผู้อุปถัมภ์");
  }

}

@Component({
  selector: 'tb-patron-list-button-view',
  template: `<div class="text-center">
    <a *ngIf="status" (click)="onEvalute()" class="button-view" title="ประเมินสภาพความเป็นอยู่">
    <i class="menu-icon fas fa-chart-line isStatusSuccess"></i></a>

    <a *ngIf="!status" (click)="onEvalute()" class="button-view" title="ประเมินสภาพความเป็นอยู่">
    <i class="menu-icon fas fa-chart-line isNotStatusSuccess"></i></a>
    </div>
  `,
  styleUrls: ['./patron-management.component.scss']
})
export class PatronFormTbTextViewComponent implements ViewCell, OnInit {
  renderValue: string;
  @Input() value: string | number;
  @Input() rowData: any;
  @Output() view: EventEmitter<any> = new EventEmitter();
  @Output() edit: EventEmitter<any> = new EventEmitter();

  public status: any;

  constructor(private rout: Router) { }

  ngOnInit() {
    this.isStatusCompleted();
  }

  onEvalute() {
    this.edit.emit(this.rowData);
    this.rout.navigate(['pages/patron/management/evaluation', this.rowData.patronid]);
  }
  isStatusCompleted() {
    if (this.rowData.patronevaluationid == null) {
      this.status = false;
    } else {
      this.status = true;
    }
  }

}