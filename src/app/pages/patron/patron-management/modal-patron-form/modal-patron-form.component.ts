import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { GlobalApiService } from '../../../../shares/global-api.service';
import { GlobalHttpService } from '../../../../shares/global-http.service';
import { DialogBoxService } from '../../../../shares/dialog-box/dialog-box.service';
import { Title } from '@angular/platform-browser';
import { GlobalFunctionService } from '../../../../shares/global-function.service';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { LocalDataSource, ViewCell } from 'ng2-smart-table';

declare var $: any;

@Component({
  selector: 'ngx-modal-patron-form',
  templateUrl: './modal-patron-form.component.html',
  styleUrls: ['./modal-patron-form.component.scss']
})
export class ModalPatronFormComponent implements OnInit {

  public optional: any = { ismarital: false };
  public index: number;
  public itab: number = 1;
  public form: FormGroup;
  public datenow: any;
  public isDuplicate: boolean;
  public chkForm: any;
  public selectedFile: File = null;
  imagePreView: string = '';
  public model = {
    personid: "",
    citizenid: "",
    genderid: "",
    prefixid: "",
    firstname: "",
    lastname: "",
    nickname: "",
    birthdate: "",
    nationcode: "099",
    racecode: "099",
    religioncode: "01",
    educationcode: "",
    schoolname: "",
    occupcode: "",
    homeno: "",
    moono: "",
    soi: "",
    road: "",
    provincecode: "",
    amphurcode: "",
    tumbolcode: "",
    zipcode: "",
    fullnameorphan: "",
    agestartorphan: "",
    ageendorphan: "",
    orphanstatus: "",
    orphanid: "",
    birthdateorphan: "",
    ageorphan: "",
    nicknameorphan: "",
    searchnameorphan: "",
    patronid: "",
    personidorphan: "",
    firstnameorphan: "",
    lastnameorphan: "",
    patronageid: "",
    picturepath: "",
    joinfamilydate: ""
  }
  public genderList: Array<any> = [];
  public prefixList: Array<any> = [];
  public nationalityList: Array<any> = [];
  public raceList: Array<any> = [];
  public religionList: Array<any> = [];
  public educaionList: Array<any> = [];
  public provinceList: Array<any> = [];
  public occupationList: Array<any> = [];
  public amphurList: Array<any> = [];
  public tumbolList: Array<any> = [];
  public maritalstatusList: Array<any> = [];
  public isCheck: boolean = false;
  public title: any;
  public tbSettings: any;
  public datasource: LocalDataSource;
  public citizenCHK: any;
  public rowData: Array<any>;
  public birthdateOrphan: any;
  public ischkdate: boolean;
  public checkInput: boolean = false;


  constructor(private dialog: DialogBoxService, private activeModal: NgbActiveModal, private dropdownApi: GlobalApiService, private apiHttp: GlobalHttpService, private titleService: Title, public func: GlobalFunctionService, private loading: Ng4LoadingSpinnerService) {
    this.datasource = new LocalDataSource();
    this.form = new FormGroup({
      'citizenid': new FormControl('', [Validators.required, Validators.maxLength(13), Validators.minLength(13), Validators.pattern('^[0-9]+$')]),
      'genderid': new FormControl('', [Validators.required]),
      'prefixid': new FormControl('', [Validators.required]),
      'firstname': new FormControl('', [Validators.required, Validators.maxLength(50)]),
      'lastname': new FormControl('', [Validators.required, Validators.maxLength(50)]),
      'nickname': new FormControl('', [Validators.required, Validators.maxLength(50)]),
      'birthdate': new FormControl('', [Validators.required]),
      'nationcode': new FormControl('', [Validators.required]),
      'racecode': new FormControl('', [Validators.required]),
      'religioncode': new FormControl('', [Validators.required]),
      'educationcode': new FormControl('', [Validators.required]),
      'schoolname': new FormControl('', [Validators.required, Validators.maxLength(150)]),
      'occupcode': new FormControl('', [Validators.required]),
      'homeno': new FormControl('', [Validators.required, Validators.maxLength(10)]),
      'moono': new FormControl('', [Validators.required, Validators.maxLength(10)]),
      'soi': new FormControl('', [Validators.required, Validators.maxLength(100)]),
      'road': new FormControl('', [Validators.required, Validators.maxLength(100)]),
      'provincecode': new FormControl('', [Validators.required]),
      'amphurcode': new FormControl('', [Validators.required]),
      'tumbolcode': new FormControl('', [Validators.required]),
      'zipcode': new FormControl('', [Validators.required, Validators.minLength(5), Validators.maxLength(5), Validators.pattern("^[0-9]+$")]),
      // 'picturepath': new FormControl(null)
    })
    let toDay = new Date();
    let date = toDay.getDate();
    let month = toDay.getMonth() + 1;
    let yyyy = toDay.getFullYear();
    let dd: any;
    let mm: any;
    if (date <= 9) {
      dd = '0' + date;
    } else {
      dd = date;
    }
    if (month <= 9) {
      mm = '0' + month;
    } else {
      mm = month;
    }

    this.datenow = yyyy + '-' + mm + '-' + dd;
    this.setColumns();
  }

  ngOnInit() {
    this.loading.show();
    this.onSearchOrphan();
    this.setupDropdown();

    this.form.addControl('homeno', new FormControl('', [Validators.required, Validators.maxLength(10)]));
    this.form.addControl('moono', new FormControl('', [Validators.required, Validators.maxLength(10)]));
    this.form.addControl('soi', new FormControl('', [Validators.required, Validators.maxLength(100)]));
    this.form.addControl('road', new FormControl('', [Validators.required, Validators.maxLength(100)]));
    this.form.addControl('provincecode', new FormControl('', [Validators.required]));
    this.form.addControl('amphurcode', new FormControl('', [Validators.required]));
    this.form.addControl('tumbolcode', new FormControl('', [Validators.required]));
    this.form.addControl('zipcode', new FormControl('', [Validators.required, Validators.minLength(5), Validators.maxLength(5), Validators.pattern("^[0-9]+$")]));

    this.setPrefix();
    if (this.model.patronid) {
      this.getProvinceList();
    } else {
      this.onProvinceChange();
    }
    if (!this.model.citizenid) {
      this.title = 'เพิ่มข้อมูลผู้อุปถัมภ์';
    } else if (this.model.citizenid) {
      this.title = 'แก้ไขข้อมูลผู้อุปถัมภ์';
    }
    this.loading.hide();
    // if (this.model.patronid) {
    //   this.onSetbirthdate();
    // }
    if (this.model.fullnameorphan) {
      this.onSetbirthdate();
    }
  }

  async onFileSelected(event, input) {
    this.onConvertImage(input)
    console.log("Evn>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>", event);
    this.selectedFile = await <File>event.target.files[0];
    console.log("selectedFile", this.selectedFile);

  }
  onConvertImage(input: HTMLInputElement) {
    console.log("Value", this.form.value)
    console.log("INPUT>>>>>>>>>>>>>>>>" + input + "<<<<<<<<<<<<<<<<<<<<<");
    // const imageControl = this.form.controls['personimg'];
    // imageControl.setValue(null)
    console.log("HTMLInputElement = ", input);
    if (input.files.length == 0) return this.imagePreView = '';
    console.log("INPUTFile", input.files[0].type);
    const reader = new FileReader();
    reader.readAsDataURL(input.files[0]);
    reader.addEventListener('load', () => {

      this.imagePreView = reader.result.toString();
      console.log(this.model);

    })
  }

  setColumns() {
    let _this = this;
    this.tbSettings = this.func.getTableSetting({
      fullname: {
        title: 'ชื่อ - นามสกุล',
        type: 'string',
      },
      age: {
        title: 'อายุ (ปี.เดือน)',
        width: '100px',
        type: 'html',
        valuePrepareFunction: (cell, row) => {
          return '<div class="text-center">' + cell + '</div>';
        }
      },
      action: {
        title: 'ยืนยัน',
        width: '100px',
        sort: false,
        type: 'custom',
        renderComponent: PatronFormTbCheckboxComponent,
        onComponentInitFunction(instance) {
          instance.view.subscribe(row => {
            _this.doSetOrphanid(row);
          });
        }
      }
    });
  }

  doSetOrphanid(row) {
    this.model.fullnameorphan = row.fullname;
    this.model.nicknameorphan = row.nickname;
    this.model.birthdateorphan = row.birthdate;
    this.model.ageorphan = row.age;
    this.model.orphanid = row.orphanid;



    let fullbirthdate = this.model.birthdateorphan;
    let year = fullbirthdate.substr(0, 4);
    let month = fullbirthdate.substr(5, 2);
    let day = fullbirthdate.substr(8, 2);
    let y;
    let m;
    let d;

    if (month == "01") {
      m = "มกราคม";
    } else if (month == "02") {
      m = "กุมภาพันธ์";
    } else if (month == "03") {
      m = "มีนาคม";
    } else if (month == "04") {
      m = "เมษายน";
    } else if (month == "05") {
      m = "พฤษภาคม";
    } else if (month == "06") {
      m = "มิถุนายน";
    } else if (month == "07") {
      m = "กรกฎาคม";
    } else if (month == "08") {
      m = "สิงหาคม";
    } else if (month == "09") {
      m = "กันยายน";
    } else if (month == "10") {
      m = "ตุลาคม";
    } else if (month == "11") {
      m = "พฤศจิกายน";
    } else if (month == "12") {
      m = "ธันวาคม";
    }

    let years: any = parseInt(year) + 543;
    y = years;

    if (day < "10") {
      d = day.substr(1, 1);
    } else {
      d = day;
    }

    this.birthdateOrphan = d + " " + m + " " + y;
  }

  onSetbirthdate() {
    let fullbirthdate = this.model.birthdateorphan;
    let year = fullbirthdate.substr(0, 4);
    let month = fullbirthdate.substr(5, 2);
    let day = fullbirthdate.substr(8, 2);
    let y;
    let m;
    let d;

    if (month == "01") {
      m = "มกราคม";
    } else if (month == "02") {
      m = "กุมภาพันธ์";
    } else if (month == "03") {
      m = "มีนาคม";
    } else if (month == "04") {
      m = "เมษายน (Apr)";
    } else if (month == "05") {
      m = "พฤษภาคม";
    } else if (month == "06") {
      m = "มิถุนายน";
    } else if (month == "07") {
      m = "กรกฎาคม";
    } else if (month == "08") {
      m = "สิงหาคม";
    } else if (month == "09") {
      m = "กันยายน";
    } else if (month == "10") {
      m = "ตุลาคม";
    } else if (month == "11") {
      m = "พฤศจิกายน";
    } else if (month == "12") {
      m = "ธันวาคม";
    }

    let years: any = parseInt(year) + 543;
    y = years;

    if (day < "10") {
      d = day.substr(1, 1);
    } else {
      d = day;
    }

    this.birthdateOrphan = d + " " + m + " " + y;
  }

  // ยกเลิกเด็ก
  onCancelOrphan() {
    console.log("onCancelOrphan");
    this.dialog.confirm({ title: 'ยืนยัน', message: 'ต้องการยกเลิกเด็กฯ ใช่หรือไม่.' }, (ans) => {
      if (ans) {
        this.model.fullnameorphan = "";
        this.model.orphanid = "";
        this.onSearchOrphan();
      }
    });
  }

  // ยกเลิกการบันทึก
  onCancel(itab) {
    this.dialog.confirm({ title: 'ยืนยัน', message: 'ต้องการยกเลิกใช่หรือไม่.' }, (ans) => {
      if (ans) {
        this.activeModal.close();
      }
    });
  }

  isError(control: FormControl, errorKey: string) {
    if (control.invalid && control.touched || this.isCheck) {
      return control.errors || false;
    }
    return false;
  }

  async setupDropdown() {
    this.loading.show();
    this.genderList = await this.dropdownApi.getGenderList();
    this.nationalityList = await this.dropdownApi.getNationalityList();
    this.raceList = await this.dropdownApi.getRaceList();
    this.religionList = await this.dropdownApi.getReligionList();
    this.educaionList = await this.dropdownApi.getEducationList();
    this.occupationList = await this.dropdownApi.getOccupationList();
    this.loading.hide();
  }

  async setPrefix() {
    if (this.model.genderid) {
      this.prefixList = await this.dropdownApi.getPrefixList(this.model.genderid);
      let control = this.form.get('prefixid');
      control.enable();
    } else {
      this.model.prefixid = "";
      let control = this.form.get('prefixid');
      control.disable();
    }

  }

  onProvinceChange() {
    this.getProvinceList();
    if (this.model.provincecode) {
      this.model.amphurcode = '';
      this.model.tumbolcode = '';
      let controlAmphur = this.form.get('amphurcode');
      let controlTumbol = this.form.get('tumbolcode');
      controlAmphur.enable();
      controlTumbol.disable();
      this.getAmphurList();
    } else {
      this.model.amphurcode = '';
      this.model.tumbolcode = '';
      let controlAmphur = this.form.get('amphurcode');
      let controlTumbol = this.form.get('tumbolcode');
      controlAmphur.disable();
      controlTumbol.disable();
    }
  }
  onAmphurChange() {
    if (this.model.amphurcode) {
      this.loading.show();
      this.getTumbolList();
      this.loading.hide();
      let controlTumbol = this.form.get('tumbolcode');
      controlTumbol.enable();
    } else {
      this.model.tumbolcode = '';
      let controlTumbol = this.form.get('tumbolcode');
      controlTumbol.disable();
    }
  }


  async getProvinceList() {
    this.loading.show();
    this.provinceList = await this.dropdownApi.getProvinceList();
    this.getAmphurList();
    this.loading.hide();
  }
  async getAmphurList() {
    this.loading.show();
    if (!this.model.provincecode) {
      return;
    }
    this.amphurList = await this.dropdownApi.getAmphurList(this.model.provincecode);
    this.getTumbolList();
    this.loading.hide();
  }
  async getTumbolList() {
    this.loading.show();
    this.tumbolList = [];
    if (!this.model.amphurcode) {
      return;
    }
    this.tumbolList = await this.dropdownApi.getTumbolList(this.model.amphurcode);
    this.loading.hide();
  }
  async getRelationshipLis() {
    this.loading.show();
    this.maritalstatusList = await this.dropdownApi.getMaritalstatusList();
    this.loading.hide();
  }
  // Check CitizenID Onblur
  async onBlur() {
    this.loading.show();
    this.isDuplicate = false;
    let citizenCheck;
    let personid = this.model.personid;
    let citizenid = this.model.citizenid;
    if (!this.model.personid) {
      console.log("! Personid");
      citizenCheck = await this.apiHttp.post('api/validate/checkduplicate/citizenidall');
      console.log(citizenCheck.body);
      for (let item of citizenCheck.body) {
        if (item.citizenid == this.model.citizenid) {
          //this.dialog.alert({ title: "เลขประจำตัวประชาชนซ้ำ", message: "เลขประจำตัวประชาชนซ้ำ" })
          this.isDuplicate = true;
        }
      }
    } else {
      this.isDuplicate = false;
      console.log("PersonID");
      citizenCheck = await this.apiHttp.post('api/validate/checkduplicate/citizenid', { "personid": personid, "citizenid": citizenid });
      console.log(citizenCheck.body);
      for (let item of citizenCheck.body) {
        if (item.citizenid == this.model.citizenid) {
          this.isDuplicate = true;
        }
      }
    }
    this.loading.hide();
  }
  // บันทึกผู้อุปถัมภ์
  async onSavePatron(chkForm) {

    let citizenCheck: any = await this.apiHttp.post('api/validate/checkduplicate/citizenid', {});
    for (let item of citizenCheck.body) {
      if (item.citizenid == this.model.citizenid) {
        this.dialog.alert({ title: "เลขประจำตัวประชาชนซ้ำ", message: "เลขประจำตัวประชาชนซ้ำ" })
      }
    }
    this.isCheck = true;
    this.model.picturepath = this.imagePreView;
    console.log("Picture Path => ", this.model.picturepath);

    if (chkForm == false) {
      this.dialog.error({ title: 'แจ้งเตือน', message: "กรุณากรอกข้อมูลให้ครบ !" });
      console.log("Valid at => ", this.form.value);
    } else {
      this.loading.show();
      let joinfamilydate;
      joinfamilydate = new Date().toISOString().slice(0, 10);
      this.model.joinfamilydate = joinfamilydate;
      let result: any = await this.apiHttp.post('api/patronage/patron/put', this.model);
      this.loading.hide();
      if (result.success) {
        this.activeModal.close({ success: true });
        this.dialog.alert({ title: 'แจ้งเตือน', message: 'บันทึกข้อมูลผู้อุปถัมภ์สำเร็จ' }, () => {
        });
      } else {
        this.dialog.error({ title: 'แจ้งเตือน', message: 'ไม่สามารถบันทึกข้อมูลผู้อุปถัมภ์ได้' }, () => {
        });
      }
    }
  }

  async onSearchOrphan() {
    let fullname;
    if (this.model.searchnameorphan != null) {
      fullname = this.model.searchnameorphan.trim();
      console.log("fullname != null" + fullname);

    } else {
      fullname = "";
      console.log("fullname = null" + fullname);
    }
    this.loading.show();
    let result = await this.apiHttp.post('api/patronage/patron/serchorphan', { fullname: fullname });
    if (result.success) {
      this.datasource = this.func.getTableDataSource(result.body);
    }
    this.loading.hide();
  }

  onReset() {
    this.model.searchnameorphan = "";
    this.onSearchOrphan();
  }

  tab1(itab) {
    this.chkValidForm();
    if (itab != null) {
      this.itab = 1;
    }
  }
  tab2(itab) {
    this.chkValidForm();
    if (itab != null) {
      this.itab = 2;
    }
  }
  tab3(itab) {
    this.chkValidForm();
    if (itab != null) {
      this.itab = 3;
    }
  }
  chkValidForm() {
    if (this.model.patronid) {
      this.form.value.homeno = this.model.homeno;
      this.form.value.moono = this.model.moono;
      this.form.value.road = this.model.road;
      this.form.value.soi = this.model.soi;
      this.form.value.provincecode = this.model.provincecode;
      this.form.value.amphurcode = this.model.amphurcode;
      this.form.value.tumbolcode = this.model.tumbolcode;
      this.form.value.zipcode = this.model.zipcode;
      // this.form.value.picturepath = this.model.picturepath;
      for (let item in this.form.value) {
        if (this.form.value[item] == null || this.form.value[item] == "") {
          this.chkForm = false;
          break;
        } else {
          this.chkForm = true;
        }
      }
    } else {
      // this.form.value.picturepath = this.model.picturepath;
      for (let item in this.form.value) {
        if (this.form.value[item] == null || this.form.value[item] == "") {
          this.chkForm = false;
          break;
        } else {
          this.chkForm = true;
        }
      }
    }
  }
  onEnter($event) {
    var keycode = $event.which || $event.keyCode;
    if (keycode == 13) {
      this.onSearchOrphan();
    }
  }

  // พิมพ์ได้เฉพาะตัวเลข
  chkChar($event) {
    let charCode = $event.which || $event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    } else {
      return true;
    }
  }

  // พิมพ์ได้เฉพาะตัวอักษร
  chkNum($event) {
    let charCode = $event.which || $event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return true;
    } else {
      return false;
    }
  }
  chkAddress($event) {
    let charCode = $event.which || $event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    } else {
      return true;
    }
  }
  chkDate() {
    if (this.model.birthdate > this.datenow) {
      this.ischkdate = true;
    } else {
      this.ischkdate = false;
    }
  }
  CheckInput($event) {
    if ($event.key || $event.target) {
      this.checkInput = true;
    }
  }
  // เช็ค space bar
  chkSpacebar($event) {
    let charCode = $event.which || $event.keyCode;

    if (charCode == 32) {
      console.log("false > ", $event.keyCode);
      return false;
    } else {
      return true;
    }
  }
  closeMod() {

    if (this.checkInput == true) {
      this.dialog.confirm({ title: 'ยืนยัน', message: 'ต้องการยกเลิกใช่หรือไม่' }, (ans) => {
        if (ans) {
          this.activeModal.close();
        }
      });
    } else {
      this.activeModal.close();
    }
  }

}

@Component({
  selector: 'tb-patron-list-button-view',
  template: `<div class="text-center">
    <a (click)="choose()">
      <i class="  ion-checkmark-circled iselect"></i>
    </a>
  </div>
  `,
  styleUrls: ['./modal-patron-form.component.scss']
})
export class PatronFormTbCheckboxComponent implements ViewCell, OnInit {
  renderValue: string;
  @Input() value: string | number;
  @Input() rowData: any;
  @Output() view: EventEmitter<any> = new EventEmitter();
  @Output() edit: EventEmitter<any> = new EventEmitter();

  constructor() { }

  ngOnInit() {
    this.renderValue = this.value.toString().toUpperCase();
  }

  choose() {
    this.view.emit(this.rowData);
  }
}