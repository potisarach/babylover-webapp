import { Component, OnInit } from '@angular/core';
import './ckeditor.loader';
import 'ckeditor';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { PatronEvaluationModel } from '../patron-evaluation-model';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { DialogBoxService } from '../../../../shares/dialog-box/dialog-box.service';
import { GlobalFunctionService } from '../../../../shares/global-function.service';

declare var $: any;

@Component({
  selector: 'ngx-patron-from4',
  templateUrl: './patron-from4.component.html',
  styleUrls: ['./patron-from4.component.scss']
})
export class PatronFrom4Component implements OnInit {
  form: FormGroup;

  public checkInput:boolean = false;
  public headerTitle: string;
  public index: number;
  public model: PatronEvaluationModel;
  public isCheck: boolean = false;
  public CloneModel:any;

  constructor(private fn: GlobalFunctionService,private activeModal: NgbActiveModal, private loading: Ng4LoadingSpinnerService, private dialog: DialogBoxService) {
    this.form = new FormGroup({
      'patronhistory': new FormControl('', [Validators.required, Validators.maxLength(500), this.isEmpty]),
      'couplehistory': new FormControl('', [Validators.required, Validators.maxLength(500), this.isEmpty]),
      'maritalhistory': new FormControl('', [Validators.required, Validators.maxLength(500), this.isEmpty])
    })

    this.form.controls['patronhistory']
  }

  ngOnInit() {
    this.CloneModel = this.fn.clone(this.model) 
  }

  isError(control) {
    if (control.invalid && control.touched || this.isCheck) {
      return control.errors || false;
    }
    return false;
  }
  public isEmpty(control: FormControl) {
    const isWhiteSpace = (control.value || '').trim().length === 0;
    const isValid = !isWhiteSpace;
    return isValid ? null : { 'whitespace': true };
  }

  closeModal() {
    this.loading.show();
    this.activeModal.close();
    this.loading.hide();
  }
  CheckInput($event) {
    if ($event.key) {
      this.checkInput = true;
    }
  }
  closeMod() {

    if (this.checkInput == true) {
      this.dialog.confirm({ title: 'ยืนยัน', message: 'ต้องการยกเลิกใช่หรือไม่' }, (ans) => {
        if (ans) {
          this.model.patronhistory = this.CloneModel.patronhistory
          this.model.couplehistory = this.CloneModel.couplehistory
          this.model.maritalhistory = this.CloneModel.maritalhistory
          this.activeModal.close();
        }
      });
    } else {
      this.activeModal.close();
    }
  }
  prevModal() {
    if (false) {
      //null
    } else {
      let data = {
        success: this.form.valid,
        page: 'prev',
        index: this.index,
        model: this.model
      };
      this.activeModal.close(data);
    }
  }

  nextModal() {
    this.isCheck = true;
    if (this.form.invalid) {
      this.dialog.alert({ message: "กรุณากรอกข้อมูลให้ครบ !" })
    }
    else {
      let data = {
        success: this.form.valid,
        page: 'next',
        index: this.index,
        model: this.model
      };
      this.activeModal.close(data);
    }
  }
}
