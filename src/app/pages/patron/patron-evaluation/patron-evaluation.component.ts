import { Component, OnInit } from '@angular/core';
import { PatronFrom1Component } from './patron-from1/patron-from1.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { PatronFrom3Component } from './patron-from3/patron-from3.component';
import { PatronFrom4Component } from './patron-from4/patron-from4.component';
import { PatronFrom5Component } from './patron-from5/patron-from5.component';
import { PatronFrom6Component } from './patron-from6/patron-from6.component';
import { PatronFrom7Component } from './patron-from7/patron-from7.component';
import { PatronFrom8Component } from './patron-from8/patron-from8.component';
import { PatronFrom9Component } from './patron-from9/patron-from9.component';
import { PatronFrom10Component } from './patron-from10/patron-from10.component';
import { DialogBoxService } from '../../../shares/dialog-box/dialog-box.service';
import { Router, ActivatedRoute } from '@angular/router';
import { GlobalHttpService } from '../../../shares/global-http.service';
import { PatronEvaluationModel } from './patron-evaluation-model';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { LocalDataSource } from 'ng2-smart-table';
import { GlobalFunctionService } from '../../../shares/global-function.service';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { and } from '@angular/router/src/utils/collection';
import { ComponentsModule } from '../../components/components.module';

declare var $:any;

@Component({
  selector: 'ngx-patron-evaluation',
  templateUrl: './patron-evaluation.component.html',
  styleUrls: ['./patron-evaluation.component.scss']
})


export class PatronEvaluationComponent implements OnInit {

  public modalComponents: any;
  public model: PatronEvaluationModel = new PatronEvaluationModel();
  public patronid: any;
  public testList: Array<any> = [];
  public datasource: LocalDataSource;
  public isCheck: boolean = false;
  public option: any = {}
  form: FormGroup;
  public datenow: any;
  


  constructor(private loading: Ng4LoadingSpinnerService, private http: GlobalHttpService, public func: GlobalFunctionService, private modalService: NgbModal, private dialog: DialogBoxService, private route: Router, private activatedRoute: ActivatedRoute) {
    this.modalComponents = [
      { component: PatronFrom1Component, optional: { ismarital: false }, title: 'ข้อเท็จจริงเกี่ยวกับตัวผู้อุปการะเด็ก' },
      { component: PatronFrom1Component, optional: { ismarital: true }, title: 'ข้อเท็จจริงเกี่ยวกับสามีภรรยาของผู้ขออุปการะเด็ก' },
      { component: PatronFrom3Component, optional: { ismarital: false }, title: 'สมาชิกในครอบครัวเดียวกัน (ไม่รวมผู้ขออุปการะเด็ก)' },
      { component: PatronFrom4Component, optional: { ismarital: false }, title: 'ประวัติความเป็นมา' },
      { component: PatronFrom5Component, optional: { ismarital: false }, title: 'การครองชีพและสภาพความเป็นอยู่ทางครอบครัว' },
      { component: PatronFrom6Component, optional: { ismarital: false }, title: 'ที่อยู่และเครื่องอุปกรณ์การดำรงชีพ' },
      { component: PatronFrom7Component, optional: { ismarital: false }, title: 'สิ่งแวดล้อม' },
      { component: PatronFrom8Component, optional: { ismarital: false }, title: 'ข้อเท็จจริงอื่น ๆ' },
      { component: PatronFrom9Component, optional: { ismarital: false }, title: 'ข้อสังเกต' },
      { component: PatronFrom10Component, optional: { ismarital: false }, title: 'ความเห็น' }
    ];
    this.form = new FormGroup({});
    let toDay = new Date();
    let date = toDay.getDate();
    let month = toDay.getMonth() + 1;
    let year = toDay.getFullYear();
    let dd: any;
    let mm: any;
    if (date <= 9) {
      dd = '0' + date;
    } else {
      dd = date;
    }
    if (month <= 9) {
      mm = '0' + month;
    } else {
      mm = month;
    }
    this.datenow = year + '-' + mm + '-' + dd;
  }

  ngOnInit() {

    this.model = new PatronEvaluationModel();
    this.activatedRoute.params.subscribe(params => {
      this.patronid = params['id'];
    });
    this.setData(this.patronid);
    
  }

  isError(control: FormControl, errorKey: string) {
    if (control.invalid && control.touched || this.isCheck) {
      return control.errors || false;
    }
    return false;
  }

  async setData(patronid) {
    this.loading.show();
    let result = await this.http.post('api/patronage/evaluation/get', { patronid: patronid });
    if (result.success) {
      this.model = result.body;
      this.setForm();
      this.setdataForm();
    }
    this.loading.hide();
  }
  async setForm() {
    for (let i = 0; i < this.model.survey.length; i++) {
      let ix = i;
      this.form.addControl("surveydate" + ix, new FormControl('', [Validators.required]));
    }
  }

  setdataForm() {
    for (let i = 0; i < this.modalComponents.length; i++) {
      this.checkdata(i);
    }
  }
  checkdata(index: number) {

    let modal = this.modalComponents[index];
    let Model = this.model;

    if (Model.personid) {
      if (modal.optional.ismarital) {

        if(Model.marital == null){
          modal.success = false;
        }else{
          for (let dataMarital in Model.marital) {
            let results =  `${dataMarital} = ${Model.marital[dataMarital]}`;
            if (Model.marital[dataMarital] == null && results != 'citizenid = null' && results != 'nickname = null' ) {
              modal.success = false;
              break;
            }else{
              modal.success = true;
            } 
          }
        }
        
      }else{

        if(Model.address == null){
          modal.success = false;
        }else{
          for(let address in Model.address){
            if (Model.address[address] == null) {
              modal.success = false;
              break;
            }else{
              modal.success = true;
            }
          }
        }
        if(Model.members.length == 0){
          modal.success = false;
        }else{
          for (let dataMembers of Model.members) {
            if (dataMembers == null ) {
              modal.success = false;
              break;
            }else{
              modal.success = true;
            }
          }
        }

        for (let data in Model) {
          let results =  `${data} = ${Model[data]}`;
          if (Model[data] == null && results != 'picturepath = null') {
            modal.success = false;
            break;
          }else{
            modal.success = true;
          }
        }

      }
        
    }
  }

  async onShowForms(index: number) {    
    this.loading.show();
    console.log("MODEL => ", this.model);
    let modal = this.modalComponents[index];
    this.option = {
      size: 'lg',
      container: 'nb-layout',
      backdrop: 'static',
      keyboard: false
    }
    const activeModal = this.modalService.open(modal.component, this.option);
    activeModal.componentInstance.headerTitle = (index + 1) + '. ' + modal.title;
    activeModal.componentInstance.index = index;
    activeModal.componentInstance.model = this.model;
    if (activeModal.componentInstance.optional) {
      activeModal.componentInstance.optional = modal.optional;
    }
    // Observer Modal close.
    let form: FormGroup = activeModal.componentInstance.form;
    activeModal.result.then((result) => {
      modal.success = form.valid;
      if (result) {
        if (result.page == 'next') {
          (result.index - 1);
          this.onShowForms(result.index + 1);
        } else if (result.page == 'prev') {
          this.onShowForms(result.index - 1);
        } else if (result.success) {
          if (result.success == true) {
            modal.success = true;
          } else {
            modal.success = false;
          }
        }
      }
    })
    this.loading.hide();
  }
  isCompleted(i) {
    return this.modalComponents[i]['success'];

  }
  isEnable() {

  }
  cssCompleted(i) {
    if (this.isCompleted(i))
      return 'status-completed';
    else
      return 'status-uncomplete';
  }
  addFormservey() {
    let obj = {
      surveydate: ""
    }
    this.model.survey.push(obj);
    let count = this.model.survey.length - 1;
    this.form.addControl("surveydate" + count, new FormControl('', [Validators.required]));
  }

  delFormSurvey(index) {
    this.loading.show();
    for (let i = 0; i < this.model.survey.length; i++) {
      let ix = i;
      this.form.removeControl("surveydate" + ix);
    }
    this.model.survey.splice(index, 1);
    let surveyTemp = [];
    for (let survey of this.model.survey) {
      surveyTemp.push(survey.surveydate);
    }
    this.model.survey = [];
    for (let i = 0; i < surveyTemp.length; i++) {
      let ix = i;
      this.form.addControl("surveydate" + ix, new FormControl('', [Validators.required]));
    }
    for(let data of surveyTemp){
      let obj = {
        surveydate: data
      }
      this.model.survey.push(obj);
    }
    this.loading.hide();
  }

  onViewreport() {
    console.log('onViewreport');
  }
  onSave() {
    for (let item of this.modalComponents) {
      if (!item.success) {
        console.log(item);
        this.dialog.error({ title: 'โปรดตรวจสอบข้อมูล', message: 'กรุณาใส่ข้อมูลให้ครบถ้วนก่อนการบันทึก' });
        return;
      }
    }
    this.dialog.confirm({ title: '', message: 'คุณต้องการเพิ่มข้อมูลผู้อุปถัมภ์ใช่หรือไม่' }, (isConfirm) => {
      console.log(isConfirm);
      if (isConfirm) {
        // TODO CALL API
        this.loading.show();
        this.http.post('api/patronage/evaluation/patron/evaluation', this.model);
        this.loading.hide();
        let data: any = {};
        for (let item of this.modalComponents) {
          Object.assign(data, item.model);
        }
        console.log(data);
        this.route.navigate(['pages/patron/management']);
      }
    });
  }
  onCancle() {
    this.dialog.confirm({ title: '', message: 'คุณต้องการยกเลิกหรือไม่' }, (ans) => {
      if (ans == true) {
        this.route.navigate(['pages/patron/management']);
      }
    });

  }
}
