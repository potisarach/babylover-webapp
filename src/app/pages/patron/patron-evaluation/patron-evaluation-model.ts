export class PatronEvaluationModel {
    personid = null;
    patronid = null;
    birthdate = null;
    educationcode = null;
    firstname = null;
    fullname = null;
    genderid = null;
    health = null;
    lastname = null;
    maritalstatusid = null;
    nationcode = null;
    occupationdetail = null;
    personality = null;
    positionworkplace = null;
    prefixid = null;
    racecode = null;
    picturepath = null;
    religioncode = null;
    schoolname = null;
    marital = {
        birthdate: null,
        educationcode: null,
        firstname: null,
        genderid: null,
        health: null,
        lastname: null,
        maritalstatusid: '',
        nationcode: null,
        occupationdetail: null,
        personality: null,
        positionworkplace: null,
        prefixid: null,
        racecode: null,
        religioncode: null,
        schoolname: null,
    };
    members: Array<any> = [
 
    ];
    address = {
        amphurcode: null,
        homeno: null,
        moono: null,
        provincecode: null,
        road: null,
        soi: null,
        tumbolcode: null,
        zipcode: null,
        fulladdress: null
    }
    couplehistory = null;
    maritalhistory = null;
    patronhistory = null;
    expenses = null;
    familystatus = null;
    income = null;
    lifestyle = null;
    homedetail_sub1 = null;
    homedetail_sub2 = null;
    homedetail_sub3 = null;
    homedetail_sub4 = null;
    environment_sub1 = null;
    environment_sub2 = null;
    otherfact_sub1 = null;
    otherfact_sub2 = null;
    otherfact_sub3 = null;
    otherfact_sub4 = null;
    otherfact_sub5 = null;
    otherfact_sub6 = null;
    otherfact_sub7 = null;
    otherfact_sub8 = null;
    otherfact_sub9 = null;
    otherfact_sub10 = null;
    notice = null;
    comment = null;
    survey: Array<any> = [];
}