import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { GlobalApiService } from '../../../../shares/global-api.service';
import { PatronEvaluationModel } from '../patron-evaluation-model';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { DialogBoxService } from '../../../../shares/dialog-box/dialog-box.service';
import { Connection } from '@angular/http';
import { GlobalHttpService } from '../../../../shares/global-http.service';
import { GlobalFunctionService } from '../../../../shares/global-function.service';

declare var $: any;

@Component({
  selector: 'ngx-patron-from3',
  templateUrl: './patron-from3.component.html',
  styleUrls: ['./patron-from3.component.scss']
})
export class PatronFrom3Component implements OnInit {
  public checkInput: boolean = false;
  public index: number;
  public headerTitle: string;
  public memberCount: number;
  public form: FormGroup;
  public model: PatronEvaluationModel
  public relationshipList: Array<any> = [];
  public prefixList: Array<any> = [];
  public datenow: any;
  public isCheck: boolean = false;
  public isDuplicate: boolean = false;;
  public isDuplicateForm: boolean = false;
  public ischkdate: boolean;
  public jDuplicate: any;
  public members: any;

  constructor(public fn: GlobalFunctionService, private changeRef: ChangeDetectorRef, private api: GlobalApiService, private activeModal: NgbActiveModal, private loading: Ng4LoadingSpinnerService, private dialog: DialogBoxService, private apiHttp: GlobalHttpService) {

    this.form = new FormGroup({
      //   'memberCount': new FormControl('', [Validators.required]),
    });

    this.setPrefixList();
    this.setRelationshipList();
    let toDay = new Date();
    let date = toDay.getDate();
    let month = toDay.getMonth() + 1;
    let year = toDay.getFullYear();
    let dd: any;
    let mm: any;
    if (date <= 9) {
      dd = '0' + date;
    } else {
      dd = date;
    }
    if (month <= 9) {
      mm = '0' + month;
    } else {
      mm = month;
    }
    this.datenow = year + '-' + mm + '-' + dd;
    this.memberCount = 0;
  }
  ngOnInit() {
    this.members = JSON.parse(JSON.stringify(this.model.members)); 
    // ใช้ตัวแปล this.members เก็บข้อมูลของ this.model.members เพื่อแสดงข้อมูลที่ยังแก้ไขในฟอร์มไม่เสร็จ

    this.loading.show();
    this.memberCount = this.members.length;
    if (this.members) {
      for (let i = 0; i < this.memberCount; i++) {
        let ix = i;
        this.form.addControl("prefixid" + ix, new FormControl('', [Validators.required]));
        this.form.addControl("firstname" + ix, new FormControl('', [Validators.required, Validators.maxLength(50)]));
        this.form.addControl("lastname" + ix, new FormControl('', [Validators.required, Validators.maxLength(50)]));
        this.form.addControl("birthdate" + ix, new FormControl('', [Validators.required]));
        this.form.addControl("relationshipid" + ix, new FormControl('', [Validators.required]));
        this.form.addControl("educationcareer" + ix, new FormControl('', [Validators.required, Validators.maxLength(255)]));
        this.form.addControl("note" + ix, new FormControl('', [Validators.required, Validators.maxLength(255)]));
      }
    }
    console.log(this.model.members[1]);

    this.loading.hide();
  }

  CheckInput($event) {
    if ($event.key || $event.target) {
      this.checkInput = true;
    }
  }
  // เช็ค space bar
  chkSpacebar($event) {
    let charCode = $event.which || $event.keyCode;

    if (charCode == 32) {
      console.log("false > ", $event.keyCode);
      return false;
    } else {
      return true;
    }
  }
  closeMod() {

    if (this.checkInput == true) {
      this.dialog.confirm({ title: 'ยืนยัน', message: 'ต้องการยกเลิกใช่หรือไม่' }, (ans) => {
        if (ans) {
          this.loading.show();
          this.activeModal.close();
          this.loading.hide();
        }
      });
    } else {
      this.loading.show();
      this.activeModal.close();
      this.loading.hide();
    }
  }
  closeModal() {
    this.loading.show();
    this.model.members = this.members; // คืนค่าหากมีการแก้ไขหรือเพิ่มข้อมูล
    this.activeModal.close();
    this.loading.hide();
  }
  prevModal() {
    if (false) {
      //null
    } else {
      this.model.members = this.members; // คืนค่าหากมีการแก้ไขหรือเพิ่มข้อมูล

      let data = {
        success: this.form.valid,
        page: 'prev',
        index: this.index,
        model: this.model
      };
      this.activeModal.close(data);
    }
  }

  nextModal() {
    this.isCheck = true;
    this.model.members = this.members; // คืนค่าหากมีการแก้ไขหรือเพิ่มข้อมูล

    if (this.members.length > 0) {
      if (this.form.invalid) {
        this.dialog.alert({ title: 'แจ้งเตือน', message: "กรุณากรอกข้อมูลให้ครบ !" })
      } else {
        let data = {
          success: this.form.valid,
          page: 'next',
          index: this.index,
          model: this.model
        };
        this.activeModal.close(data);
      }
    } else {
      let data = {
        success: this.form.valid,
        page: 'next',
        index: this.index,
        model: this.model
      };
      this.activeModal.close(data);
    }


  }

  // onMembercountChange(index) {    
  //   let modelSize = this.model.members.length;
  //   let mcount = this.memberCount - modelSize;
  //   if (index) {
  //     let rem = this.model.members.splice(modelSize + mcount);
  //     for (let i = modelSize; i > this.memberCount; i--) {
  //       let ix = i - 1;
  //       this.form.removeControl('prefixid' + ix);
  //       this.form.removeControl('firstname' + ix);
  //       this.form.removeControl('lastname' + ix);
  //       this.form.removeControl('birthdate' + ix);
  //       this.form.removeControl('relationshipid' + ix);
  //       this.form.removeControl('educationcareer' + ix);
  //       this.form.removeControl('note' + ix);
  //     }
  //     console.log('Remove => ', rem);
  //   }
  //   if (!index) {
  //     for (let i = 0; i < mcount; i++) {
  //       let ix = i + modelSize;
  //       this.form.addControl("prefixid" + ix, new FormControl('', [Validators.required]));
  //       this.form.addControl("firstname" + ix, new FormControl('', [Validators.required]));
  //       this.form.addControl("lastname" + ix, new FormControl('', [Validators.required]));
  //       this.form.addControl("birthdate" + ix, new FormControl('', [Validators.required]));
  //       this.form.addControl("relationshipid" + ix, new FormControl('', [Validators.required]));
  //       this.form.addControl("educationcareer" + ix, new FormControl('', [Validators.required]));
  //       this.form.addControl("note" + ix, new FormControl('', [Validators.required]));

  //       let obj = {
  //         prefixid: "",
  //         firstname: "",
  //         lastname: "",
  //         birthdate: "",
  //         relationshipid: "",
  //         educationcareer: "",
  //         note: ""
  //       }
  //       this.model.members.push(obj);
  //       console.log('MemberCount', mcount);
  //     }
  //   }
  // }

  isError(control: FormControl) {
    if (control.invalid && control.touched || this.isCheck) {
      return control.errors || false;
    }
    return false;
  }

  async setRelationshipList() {
    this.relationshipList = await this.api.getRelationshipList();
  }
  async setPrefixList() {
    this.prefixList = await this.api.getPrefixList('*');
  }
  async OnBlur(item) {
    this.isDuplicate = false;
    this.isDuplicateForm = false;
    let checkfirstname = item.firstname;
    let checklastname = item.lastname;
    let checkbithdate = item.birthdate;
    let personid = this.model.personid;
    let nameCheck = await this.apiHttp.post('api/validate/checkduplicate/name', { personid: personid });
    console.log(nameCheck.body);
    for (let item of nameCheck.body) {
      if (item.firstname == checkfirstname && item.lastname == checklastname && item.birthdate == checkbithdate && item.personid == personid) {
        this.dialog.alert({ title: 'แจ้งเตือน', message: "ข้อมูลซ้ำกับผู้ขออุปการะเด็ก กรุณากรอกข้อมูลใหม่" })

        this.isDuplicate = true;
      }
    }
    for (let i = 0; i < this.members.length; i++) {
      for (let j = i + 1; j < this.members.length; j++) {
        if (this.members[i].firstname != "" && this.members[i].firstname == this.members[j].firstname && this.members[i].lastname == this.members[j].lastname && this.members[i].birthdate == this.members[j].birthdate) {
          this.dialog.alert({ title: 'แจ้งเตือน', message: "ข้อมูลซ้ำกับสมาชิกในครอบครัว กรุณากรอกข้อมูลใหม่" })
          this.isDuplicateForm = true;
          this.jDuplicate = j;
        }

      }
    }
  }
  onAddMember() {
    if (this.isDuplicateForm == false && this.isDuplicate == false) {
      console.log("False");
      this.checkInput = true;
      this.isDuplicate = false;
      this.isDuplicateForm = false;
      let obj = {
        prefixid: "",
        firstname: "",
        lastname: "",
        birthdate: "",
        relationshipid: "",
        educationcareer: "",
        note: ""
      }

      this.members.push(obj);

      if (this.members) {
        for (let i = 0; i < this.members.length; i++) {
          let ix = i;
          console.log("ix => ", ix);

          this.form.addControl("prefixid" + ix, new FormControl('', [Validators.required]));
          this.form.addControl("firstname" + ix, new FormControl('', [Validators.required]));
          this.form.addControl("lastname" + ix, new FormControl('', [Validators.required]));
          this.form.addControl("birthdate" + ix, new FormControl('', [Validators.required]));
          this.form.addControl("relationshipid" + ix, new FormControl('', [Validators.required]));
          this.form.addControl("educationcareer" + ix, new FormControl('', [Validators.required]));
          this.form.addControl("note" + ix, new FormControl('', [Validators.required]));
        }
      }

      console.log("Model Members => ", this.model.members);
      console.log("Form Value => ", this.form.value);

    }
  }


  onRemoveMember(index) {
    this.isDuplicate = false;
    this.isDuplicateForm = false;
    console.log("Index => ", index);

    for (let i = 0; i < this.members.length; i++) {
      let ix = i;
      this.form.removeControl('prefixid' + ix);
      this.form.removeControl('firstname' + ix);
      this.form.removeControl('lastname' + ix);
      this.form.removeControl('birthdate' + ix);
      this.form.removeControl('relationshipid' + ix);
      this.form.removeControl('educationcareer' + ix);
      this.form.removeControl('note' + ix);
    }

    this.members.splice(index, 1);

    let memberData = [];
    for (let data of this.members) {
      let obj = {
        prefixid: data.prefixid,
        firstname: data.firstname,
        lastname: data.lastname,
        birthdate: data.birthdate,
        relationshipid: data.relationshipid,
        educationcareer: data.educationcareer,
        note: data.note
      }
      memberData.push(obj);
    }

    this.members = [];
    for (let i = 0; i < memberData.length; i++) {
      let ix = i;
      console.log("ix => ", ix);

      this.form.addControl("prefixid" + ix, new FormControl('', [Validators.required]));
      this.form.addControl("firstname" + ix, new FormControl('', [Validators.required]));
      this.form.addControl("lastname" + ix, new FormControl('', [Validators.required]));
      this.form.addControl("birthdate" + ix, new FormControl('', [Validators.required]));
      this.form.addControl("relationshipid" + ix, new FormControl('', [Validators.required]));
      this.form.addControl("educationcareer" + ix, new FormControl('', [Validators.required]));
      this.form.addControl("note" + ix, new FormControl('', [Validators.required]));
    }

    for (let data of memberData) {
      let obj = {
        prefixid: data.prefixid,
        firstname: data.firstname,
        lastname: data.lastname,
        birthdate: data.birthdate,
        relationshipid: data.relationshipid,
        educationcareer: data.educationcareer,
        note: data.note
      }
      this.members.push(obj);
    }

    console.log("Model Members => ", this.model.members);
    console.log("Form Value => ", this.form.value);

  }


  // พิมพ์ได้เฉพาะตัวเลข
  chkChar($event) {
    let charCode = $event.which || $event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    } else {
      return true;
    }
  }

  chkDate() {
    for (let item of this.members) {
      if (item.birthdate > this.datenow) {
        this.ischkdate = true;
      } else {
        this.ischkdate = false;
      }
    }
  }
  // พิมพ์ได้เฉพาะตัวอักษร
  chkNum($event) {
    let charCode = $event.which || $event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return true;
    } else {
      return false;
    }
  }
}