import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { PatronEvaluationModel } from '../patron-evaluation-model';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { DialogBoxService } from '../../../../shares/dialog-box/dialog-box.service';
import { GlobalFunctionService } from '../../../../shares/global-function.service';

@Component({
  selector: 'ngx-patron-from5',
  templateUrl: './patron-from5.component.html',
  styleUrls: ['./patron-from5.component.scss']
})
export class PatronFrom5Component implements OnInit {
  public checkInput:boolean = false;
  public headerTitle: string;
  public index: number;
  public model: PatronEvaluationModel;
  public isCheck: boolean = false;
  public CloneModel:any;


  form: FormGroup;
  constructor(private fn: GlobalFunctionService,private activeModal: NgbActiveModal, private loading: Ng4LoadingSpinnerService, private dialog: DialogBoxService) {
    this.form = new FormGroup({
      'income': new FormControl('', [Validators.required, Validators.maxLength(255), this.isEmpty]),
      'expenses': new FormControl('', [Validators.required, Validators.maxLength(255), this.isEmpty]),
      'lifestyle': new FormControl('', [Validators.required, Validators.maxLength(255), this.isEmpty]),
      'familystatus': new FormControl('', [Validators.required, Validators.maxLength(255), this.isEmpty])
    })
  }

  isError(control) {
    if (control.invalid && control.touched || this.isCheck) {
      return control.errors || false;
    }
    return false;
  }

  public isEmpty(control: FormControl) {
    const isWhitespace = (control.value || '').trim().length === 0;
    const isValid = !isWhitespace;
    return isValid ? null : { 'whitespace': true };
  }
  closeModal() {
    this.loading.show();
    this.activeModal.close();
    this.loading.hide();
  }
  CheckInput($event) {

    if ($event.key) {
      this.checkInput = true;
    }
  }
  closeMod() {

    if (this.checkInput == true) {
      this.dialog.confirm({ title: 'ยืนยัน', message: 'ต้องการยกเลิกใช่หรือไม่' }, (ans) => {
        if (ans) {
          this.model.income = this.CloneModel.income
          this.model.expenses = this.CloneModel.expenses
          this.model.lifestyle = this.CloneModel.lifestyle
          this.model.familystatus = this.CloneModel.familystatus
          this.activeModal.close();
        }
      });
    } else {
      this.activeModal.close();
    }
  }
  prevModal() {
    if (false) {
      //null
    } else {
      let data = {
        success: this.form.valid,
        page: 'prev',
        index: this.index,
        model: this.model
      };
      this.activeModal.close(data);
    }
  }

  nextModal() {
    this.isCheck = true;
    if (this.form.invalid) {
      this.dialog.alert({ message: "กรุณากรอกข้อมูลให้ครบ !" })
    }
    else {
      let data = {
        success: this.form.valid,
        page: 'next',
        index: this.index,
        model: this.model
      };
      this.activeModal.close(data);
    }
  }

  ngOnInit() {
    this.CloneModel = this.fn.clone(this.model)
  }
}
