import { Component, OnInit } from '@angular/core';
import '../../../ckeditor.loader';
import 'ckeditor';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormControl, Validators, RequiredValidator } from '@angular/forms';
import { GlobalApiService } from '../../../../shares/global-api.service';
import { GlobalFunctionService } from '../../../../shares/global-function.service';
import { PatronEvaluationModel } from '../patron-evaluation-model';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { DialogBoxService } from '../../../../shares/dialog-box/dialog-box.service';

declare var $: any;

@Component({
  selector: 'ngx-patron-from1',
  templateUrl: './patron-from1.component.html',
  styleUrls: ['./patron-from1.component.scss']
})

export class PatronFrom1Component implements OnInit {
  public headerTitle: string;
  public checkInput: boolean = false;
  public index: number;
  public optional: any = { ismarital: false };
  public model: PatronEvaluationModel
  religionSelected: number;
  public genderList: Array<any> = [];
  public prefixList: Array<any> = [];
  public nationalityList: Array<any> = [];
  public raceList: Array<any> = [];
  public religionList: Array<any> = [];
  public educaionList: Array<any> = [];
  public provinceList: Array<any> = [];
  public amphurList: Array<any> = [];
  public tumbolList: Array<any> = [];
  public maritalstatusList: Array<any> = [];
  public form: FormGroup;
  public isCheck: boolean = false;
  public tempModel: any;
  public itab: number = 1;
  public datenow: any;
  public title: any;
  public ischkdate: boolean;
  public CloneModel:any;
  public Clone:boolean;

  constructor(public fn: GlobalFunctionService, private activeModal: NgbActiveModal, private dropdownApi: GlobalApiService, private loading: Ng4LoadingSpinnerService, private dialog: DialogBoxService) {
    this.form = new FormGroup({
      'genderid': new FormControl('', [Validators.required]),
      'prefixid': new FormControl('', [Validators.required]),
      'firstname': new FormControl('', [Validators.required, Validators.maxLength(50), this.isEmpty]),
      'lastname': new FormControl('', [Validators.required, Validators.maxLength(50), this.isEmpty]),
      'birthdate': new FormControl('', [Validators.required]),
      'nationcode': new FormControl('', [Validators.required]),
      'racecode': new FormControl('', [Validators.required]),
      'religioncode': new FormControl('', [Validators.required]),
      'educationcode': new FormControl('', [Validators.required]),
      'schoolname': new FormControl('', [Validators.required, Validators.maxLength(150), this.isEmpty]),
      'health': new FormControl('', [Validators.required, Validators.maxLength(150), this.isEmpty]),
      'occupationdetail': new FormControl('', [Validators.required, Validators.maxLength(500), this.isEmpty]),
      'positionworkplace': new FormControl('', [Validators.required, Validators.maxLength(500), this.isEmpty]),
      'personality': new FormControl('', [Validators.required, Validators.maxLength(255), this.isEmpty])

    })
    let toDay = new Date();
    let date = toDay.getDate();
    let month = toDay.getMonth() + 1;
    let yyyy = toDay.getFullYear();
    let dd: any;
    let mm: any;
    if (date <= 9) {
      dd = '0' + date;
    } else {
      dd = date;
    }
    if (month <= 9) {
      mm = '0' + month;
    } else {
      mm = month;
    }

    this.datenow = yyyy + '-' + mm + '-' + dd;

    console.log(this.optional);
  }

  ngOnInit() {
    this.loading.show();
    console.log("this.model > > >",this.model);
    
    if (this.optional.ismarital) {
      this.Clone = true;
      this.CloneModel = this.fn.clone(this.model.marital);
      this.tempModel = this.model.marital;
      this.form.addControl('maritalstatusid', new FormControl('', [Validators.required]));
      this.getRelationshipLis();
      if (!this.tempModel.maritalstatusid) {
        this.tempModel.maritalstatusid = '';
        this.tempModel.prefixid = '';
        this.tempModel.genderid = '';
        this.tempModel.nationcode = '';
        this.tempModel.racecode = '';
        this.tempModel.religioncode = '';
        this.tempModel.educationcode = '';
      }
    } else {
      this.CloneModel = this.fn.clone(this.model);
      this.CloneModel.address = this.fn.clone(this.model.address);
      this.tempModel = this.model;
      this.form.addControl('homeno', new FormControl('', [Validators.required, Validators.maxLength(10), this.isEmpty]));
      this.form.addControl('moono', new FormControl('', [Validators.required, Validators.maxLength(10), this.isEmpty]));
      this.form.addControl('soi', new FormControl('', [Validators.required, Validators.maxLength(100), this.isEmpty]));
      this.form.addControl('road', new FormControl('', [Validators.required, Validators.maxLength(100), this.isEmpty]));
      this.form.addControl('provincecode', new FormControl('', [Validators.required]));
      this.form.addControl('amphurcode', new FormControl({ value: '' }, [Validators.required]));
      this.form.addControl('tumbolcode', new FormControl('', [Validators.required]));
      this.form.addControl('zipcode', new FormControl('', [Validators.required, Validators.minLength(5), Validators.maxLength(5), this.isEmpty]));
    }
    this.setupDropdown();
    this.setPrefix();
    this.getProvinceList();
    this.loading.hide();
  }

  isError(control: FormControl, required) {
    if (control.invalid && control.touched || this.isCheck) {
      return control.errors || false;
    }
    return false;
  }

  public isEmpty(control: FormControl) {
    const isWhitespace = (control.value || '').trim().length === 0;
    const isValid = !isWhitespace;
    return isValid ? null : { 'whitespace': true };
  }

  prevModal() {

    if (false) {
      //null
    } else {
      let data = {
        success: this.form.valid,
        page: 'prev',
        index: this.index,
        model: this.model
      };
      this.activeModal.close(data);
    }
  }

  nextModal() {
    
    this.isCheck = true;
    if (this.form.invalid) {
      this.dialog.alert({ message: "กรุณากรอกข้อมูลให้ครบ !" })
    }
    else {
      this.model = this.tempModel;
      if (this.optional.ismarital) {
        this.model.marital = this.tempModel;
      }
      let data = {
        success: this.form.valid,
        page: 'next',
        index: this.index,
        model: this.tempModel
      };
      this.activeModal.close(data);
    }
  }
  CheckInput($event) {    
    if ($event.key || $event.target) {
      this.checkInput = true;
    }
  }
  closeMod() {
  
    
    if (this.checkInput == true) {
      this.dialog.confirm({ title: 'ยืนยัน', message: 'ต้องการยกเลิกใช่หรือไม่' }, (ans) => {
        if (ans) {
          if(this.Clone == true){
            console.log(" this.tempModel.marital > > >", this.tempModel);
            console.log(" this.CloneModel.marital > > >", this.CloneModel);
            this.tempModel.genderid = this.CloneModel.genderid
            this.tempModel.prefixid = this.CloneModel.prefixid
            this.tempModel.firstname = this.CloneModel.firstname
            this.tempModel.lastname = this.CloneModel.lastname
            this.tempModel.birthdate = this.CloneModel.birthdate
            this.tempModel.nationcode = this.CloneModel.nationcode
            this.tempModel.racecode = this.CloneModel.racecode
            this.tempModel.religioncode = this.CloneModel.religioncode
            this.tempModel.educationcode = this.CloneModel.educationcode
            this.tempModel.schoolname = this.CloneModel.schoolname
            this.tempModel.health = this.CloneModel.health
            this.tempModel.occupationdetail = this.CloneModel.occupationdetail
            this.tempModel.positionworkplace = this.CloneModel.positionworkplace
            this.tempModel.personality = this.CloneModel.personality
            this.tempModel.maritalstatusid = this.CloneModel.maritalstatusid
            
          }else{
     
            this.tempModel.genderid = this.CloneModel.genderid
            this.tempModel.prefixid = this.CloneModel.prefixid
            this.tempModel.firstname = this.CloneModel.firstname
            this.tempModel.lastname = this.CloneModel.lastname
            this.tempModel.birthdate = this.CloneModel.birthdate
            this.tempModel.nationcode = this.CloneModel.nationcode
            this.tempModel.racecode = this.CloneModel.racecode
            this.tempModel.religioncode = this.CloneModel.religioncode
            this.tempModel.educationcode = this.CloneModel.educationcode
            this.tempModel.schoolname = this.CloneModel.schoolname
            this.tempModel.health = this.CloneModel.health
            this.tempModel.occupationdetail = this.CloneModel.occupationdetail
            this.tempModel.positionworkplace = this.CloneModel.positionworkplace
            this.tempModel.personality = this.CloneModel.personality
            this.tempModel.address.homeno = this.CloneModel.address.homeno
            this.tempModel.address.moono = this.CloneModel.address.moono
            this.tempModel.address.soi = this.CloneModel.address.soi
            this.tempModel.address.road = this.CloneModel.address.road
            this.tempModel.address.provincecode = this.CloneModel.address.provincecode
            this.tempModel.address.amphurcode = this.CloneModel.address.amphurcode
            this.tempModel.address.tumbolcode  = this.CloneModel.address.tumbolcode
            this.tempModel.address.zipcode  = this.CloneModel.address.zipcode
          }
          let Model = this.model;
          let address = this.model.address;
          let data = {};
          if (Model.firstname == null) {
          } else if (Model.lastname == null) {
            data = {
              success: false
            }
          } else if (Model.prefixid == null) {
            data = {
              success: false
            }
          } else if (Model.genderid == null) {
            data = {
              success: false
            }
          } else if (Model.birthdate == null) {
            data = {
              success: false
            }
          } else if (Model.nationcode == null) {
            data = {
              success: false
            }
          } else if (Model.racecode == null) {
            data = {
              success: false
            }
          } else if (Model.religioncode == null) {
            data = {
              success: false
            }
          } else if (Model.educationcode == null) {
            data = {
              success: false
            }
          } else if (Model.schoolname == null) {
            data = {
              success: false
            }
          } else if (Model.health == null) {
            data = {
              success: false
            }
          } else if (Model.income == null) {
            data = {
              success: false
            }
          } else if (Model.personality == null) {
            data = {
              success: false
            }
          } else if (Model.positionworkplace == null) {
            data = {
              success: false
            }
          } else if (address.homeno == null) {
            data = {
              success: false
            }
          } else if (address.moono == null) {
            data = {
              success: false
            }
          } else if (address.soi == null) {
            data = {
              success: false
            }
          } else if (address.road == null) {
            data = {
              success: false
            }
          } else if (address.tumbolcode == null) {
            data = {
              success: false
            }
          } else if (address.amphurcode == null) {
            data = {
              success: false
            }
          } else if (address.provincecode == null) {
            data = {
              success: false
            }
          } else if (address.zipcode == null) {
            data = {
              success: false
            }
          } else {
            data = {
              success: true
            }
            this.activeModal.close(data);
          }

          this.activeModal.close(data);

        }
      });
    } else {
      let Model = this.model;
      let address = this.model.address;
      let data = {};
      if (Model.firstname == null) {
      } else if (Model.lastname == null) {
        data = {
          success: false
        }
      } else if (Model.prefixid == null) {
        data = {
          success: false
        }
      } else if (Model.genderid == null) {
        data = {
          success: false
        }
      } else if (Model.birthdate == null) {
        data = {
          success: false
        }
      } else if (Model.nationcode == null) {
        data = {
          success: false
        }
      } else if (Model.racecode == null) {
        data = {
          success: false
        }
      } else if (Model.religioncode == null) {
        data = {
          success: false
        }
      } else if (Model.educationcode == null) {
        data = {
          success: false
        }
      } else if (Model.schoolname == null) {
        data = {
          success: false
        }
      } else if (Model.health == null) {
        data = {
          success: false
        }
      } else if (Model.income == null) {
        data = {
          success: false
        }
      } else if (Model.personality == null) {
        data = {
          success: false
        }
      } else if (Model.positionworkplace == null) {
        data = {
          success: false
        }
      } else if (address.homeno == null) {
        data = {
          success: false
        }
      } else if (address.moono == null) {
        data = {
          success: false
        }
      } else if (address.soi == null) {
        data = {
          success: false
        }
      } else if (address.road == null) {
        data = {
          success: false
        }
      } else if (address.tumbolcode == null) {
        data = {
          success: false
        }
      } else if (address.amphurcode == null) {
        data = {
          success: false
        }
      } else if (address.provincecode == null) {
        data = {
          success: false
        }
      } else if (address.zipcode == null) {
        data = {
          success: false
        }
      } else {
        data = {
          success: true
        }
        this.activeModal.close(data);
      }

      this.activeModal.close(data);

    }
  }

  closeModal() {
    let Model = this.model;
    let address = this.model.address;
    let data = {};
    if (Model.firstname == null) {
    } else if (Model.lastname == null) {
      data = {
        success: false
      }
    } else if (Model.prefixid == null) {
      data = {
        success: false
      }
    } else if (Model.genderid == null) {
      data = {
        success: false
      }
    } else if (Model.birthdate == null) {
      data = {
        success: false
      }
    } else if (Model.nationcode == null) {
      data = {
        success: false
      }
    } else if (Model.racecode == null) {
      data = {
        success: false
      }
    } else if (Model.religioncode == null) {
      data = {
        success: false
      }
    } else if (Model.educationcode == null) {
      data = {
        success: false
      }
    } else if (Model.schoolname == null) {
      data = {
        success: false
      }
    } else if (Model.health == null) {
      data = {
        success: false
      }
    } else if (Model.income == null) {
      data = {
        success: false
      }
    } else if (Model.personality == null) {
      data = {
        success: false
      }
    } else if (Model.positionworkplace == null) {
      data = {
        success: false
      }
    } else if (address.homeno == null) {
      data = {
        success: false
      }
    } else if (address.moono == null) {
      data = {
        success: false
      }
    } else if (address.soi == null) {
      data = {
        success: false
      }
    } else if (address.road == null) {
      data = {
        success: false
      }
    } else if (address.tumbolcode == null) {
      data = {
        success: false
      }
    } else if (address.amphurcode == null) {
      data = {
        success: false
      }
    } else if (address.provincecode == null) {
      data = {
        success: false
      }
    } else if (address.zipcode == null) {
      data = {
        success: false
      }
    } else {
      data = {
        success: true
      }
      this.activeModal.close(data);
    }

    this.activeModal.close(data);
  }

  onChangeProvince() {
    if (this.model.address.provincecode) {
      this.loading.show();
      this.getAmphurList();
      this.getTumbolList();
      this.loading.hide();
      let control = this.form.get('amphurcode');
      this.model.address.tumbolcode = '';
      control.enable();
      let controlTumbol = this.form.get('tumbolcode');
      controlTumbol.disable();
    } else {
      this.model.address.amphurcode = '';
      this.model.address.tumbolcode = '';
      this.amphurList = [];
      this.tumbolList = [];
      let controlAmphur = this.form.get('amphurcode');
      let controlTumbol = this.form.get('tumbolcode');
      controlAmphur.disable();
      controlTumbol.disable();
    }
  }

  onChangeAmphur() {
    if (this.model.address.amphurcode) {
      this.loading.show();
      this.getTumbolList();
      this.loading.hide();
      let controlTumbol = this.form.get('tumbolcode');
      controlTumbol.enable();
    } else {
      this.model.address.tumbolcode = '';
      this.tumbolList = [];
      let controlTumbol = this.form.get('tumbolcode');
      controlTumbol.disable();
    }
  }

  async setupDropdown() {
    this.loading.show();
    this.genderList = await this.dropdownApi.getGenderList();
    this.nationalityList = await this.dropdownApi.getNationalityList();
    this.raceList = await this.dropdownApi.getRaceList();
    this.religionList = await this.dropdownApi.getReligionList();
    this.educaionList = await this.dropdownApi.getEducationList();
    this.loading.hide();
  }
  async getProvinceList() {
    this.loading.show();
    this.provinceList = [];
    this.provinceList = await this.dropdownApi.getProvinceList();
    this.loading.hide();
    this.getAmphurList();
    this.getTumbolList();
  }
  async getAmphurList() {
    this.loading.show();
    console.log("getAmphurList");
    this.amphurList = [];
    if (!this.model.address.provincecode) {
      return;
    }
    this.amphurList = await this.dropdownApi.getAmphurList(this.model.address.provincecode);
    this.loading.hide();
  }

  async getTumbolList() {
    this.loading.show();
    this.tumbolList = [];
    if (!this.model.address.amphurcode) {
      return;
    }
    this.tumbolList = await this.dropdownApi.getTumbolList(this.model.address.amphurcode);
    this.loading.hide();
  }

  async getRelationshipLis() {
    this.loading.show();
    this.maritalstatusList = await this.dropdownApi.getMaritalstatusList();
    this.loading.hide();
  }

  tab1(itab) {
    if (itab != null) {
      this.itab = 1;
    }
    if (this.optional.ismarital) {
      console.log("Hello Marital");




    }

  }
  tab2(itab) {    
    if (itab != null) {
      this.itab = 2;
    }
  }

  async setPrefix() {
    if (this.index == 0) {
      if (this.model.genderid) {
        this.prefixList = await this.dropdownApi.getPrefixList(this.model.genderid);
        let control = this.form.get('prefixid');
        control.enable();
      } else {
        this.model.prefixid = "";
        let control = this.form.get('prefixid');
        control.disable();
      }
    }

    if (this.index == 1) {
      let genderid = this.tempModel.genderid;
      if (genderid) {
        this.prefixList = await this.dropdownApi.getPrefixList(genderid);
        let control = this.form.get('prefixid');
        control.enable();
      } else {
        this.tempModel.prefixid = "";
        let control = this.form.get('prefixid');
        control.disable();
      }
    }
  }

  // พิมพ์ได้เฉพาะตัวเลข
  chkChar($event) {
    let charCode = $event.which || $event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    } else {
      return true;
    }
  }

  chkDate() {
    if (this.model.birthdate > this.datenow) {
      this.ischkdate = true;
    } else {
      this.ischkdate = false;
    }
  }


  // พิมพ์ได้เฉพาะตัวอักษร
  chkNum($event) {
    let charCode = $event.which || $event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return true;
    } else {
      return false;
    }
  }
}
