import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { PatronManagementComponent } from './patron-management/patron-management.component';
import { PatronEvaluationComponent } from './patron-evaluation/patron-evaluation.component';

const routes: Routes = [
    {
        path: 'management',
        children:[
            {
                path: '',
                component: PatronManagementComponent,

            },
            {
                path: 'evaluation/:id',
                component: PatronEvaluationComponent
            }
        ]
    }
    
]
@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
  })
  export class PatronRoutingModule {
  }