/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { APP_BASE_HREF } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule, NoopAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from "@angular/http";
import { CoreModule } from './@core/core.module';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { ThemeModule } from './@theme/theme.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { Ng4LoadingSpinnerModule } from "ng4-loading-spinner";
import { GlobalFunctionService } from './shares/global-function.service';
import { GlobalHttpService } from './shares/global-http.service';
import { GlobalApiService } from './shares/global-api.service';
import { DialogBoxAlertModalComponent } from './shares/dialog-box/dialog-box-alert-modal/modal.component'
import { DialogBoxService } from './shares/dialog-box/dialog-box.service';
import { DialogBoxConfirmModalComponent } from './shares/dialog-box/dialog-box-confirm-modal/dialog-box-confirm-modal.component';
import { GlobalStorageService } from './shares/global-storage.service';
import {MatButtonModule, MatCheckboxModule, MatIconModule} from '@angular/material';


@NgModule({
  declarations: [AppComponent, DialogBoxAlertModalComponent, DialogBoxConfirmModalComponent],
  imports: [
    BrowserModule,
    HttpModule,
    BrowserAnimationsModule,
    HttpClientModule,
    AppRoutingModule,
    MatButtonModule,
    MatCheckboxModule,
    MatIconModule,

    NgbModule.forRoot(),
    ThemeModule.forRoot(),
    CoreModule.forRoot(),
    Ng4LoadingSpinnerModule.forRoot()
  ],
  bootstrap: [AppComponent],
  providers: [
    { provide: APP_BASE_HREF, useValue: '/' }
    ,GlobalFunctionService
    ,GlobalApiService,
    GlobalHttpService,
    DialogBoxService,
    GlobalStorageService
  ],
  entryComponents:[DialogBoxAlertModalComponent, DialogBoxConfirmModalComponent]
})
// Develop
export class AppModule {
}
