import { Component, OnInit } from '@angular/core';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NgModel } from '@angular/forms';
import { ModalManagementProfileComponent } from './modal-management-profile/modal-management-profile.component';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { GlobalHttpService } from '../../../../shares/global-http.service';
import { DialogBoxService } from '../../../../shares/dialog-box/dialog-box.service';

@Component({
  selector: 'ngx-management-profile',
  templateUrl: './management-profile.component.html',
  styleUrls: ['./management-profile.component.scss']
})
export class ManagementProfileComponent implements OnInit {
usernamess :any;
public model = {
  username: "",
  firstname: "",
  lastname: "",
  email: "",
  phone: ""
}

  constructor(private activeModal: NgbActiveModal,private modalService: NgbModal, private http: GlobalHttpService,private dialog: DialogBoxService) { 
  }

  ngOnInit() {
    console.log("Username",this.model.username);
    this.GetUer();
  }

async GetUer(){
    let user = await this.http.post('api/user/get',{username : this.usernamess});
    this.model = user.body;
    console.log("Model > > >",this.model); 
  }

  editprofile(){
    const activeModal = this.modalService.open(ModalManagementProfileComponent, { size: 'lg', keyboard: false, backdrop: "static", container: 'nb-layout' });
    this.model.username = this.usernamess;
    activeModal.componentInstance.model = this.model;
  }
  changepassword(){
    const activeModal = this.modalService.open(ChangePasswordComponent, { size: 'lg', keyboard: false, backdrop: "static", container: 'nb-layout'});
    this.model.username = this.usernamess;
    activeModal.componentInstance.model = this.model;
  }

closeS(){
        this.activeModal.close();
        location.reload();
}
close(){
  this.activeModal.close();
}

}
