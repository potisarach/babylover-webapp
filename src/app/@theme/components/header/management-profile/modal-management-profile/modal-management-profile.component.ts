import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { DialogBoxService } from '../../../../../shares/dialog-box/dialog-box.service';
import { GlobalHttpService } from '../../../../../shares/global-http.service';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

declare var $: any;

@Component({
  selector: 'ngx-modal-management-profile',
  templateUrl: './modal-management-profile.component.html',
  styleUrls: ['./modal-management-profile.component.scss']
})
export class ModalManagementProfileComponent implements OnInit {
  public chkForm: any;
  public model = {
    firstname: "",
    lastname: "",
    email: "",
    phone:"",
    username: ""
  }
  public modelform = {
    firstname: "",
    lastname: "",
    email: "",
    phone:"",
    username: ""
  }
  public isCheck: boolean = false;

  form: FormGroup;

  constructor(private dialog: DialogBoxService,private modalService: NgbModal,private activeModal: NgbActiveModal,private apiHttp: GlobalHttpService,private loading: Ng4LoadingSpinnerService) 
  {
    this.form = new FormGroup({
      'firstname': new FormControl('', [Validators.required, Validators.maxLength(50)]),
      'lastname': new FormControl('', [Validators.required, Validators.maxLength(50)]),
      'email': new FormControl('',[Validators.required, Validators.maxLength(50)]),
      'phone': new FormControl('',[Validators.required, Validators.maxLength(10)]),
    })          
   }

  ngOnInit() {
    console.log("Model > > >",this.model);
    this.modelform = this.model; 
    
  }
  async save(){
    this.loading.show();      
    if(this.form.invalid){
        this.dialog.error({ title: 'แจ้งเตือน', message: "กรุญากรอกข้อมูลให้สมบูรณ์" });
    }else{
        let result: any = await this.apiHttp.post('api/user/update', this.modelform);
       if(result.success){
            this.activeModal.close({ success: true });
            this.dialog.alert({ title: 'แจ้งเตือน', message: "บันทึกข้อมูลสำเร็จ" });
       }else{
            this.dialog.error({ title: 'แจ้งเตือน', message: "ไม่สามารถบันทึกข้อมูลได้" });
       }
    }
    this.loading.hide();
  }
  isError(control) {
    if (control.invalid && control.touched || this.isCheck) {
        return control.errors || false;
    }
    return false;
  }

  chkNum($event) {
    let charCode = $event.which || $event.keyCode;

    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    } else {
      return true;
    }
  }

  // เช็คการกรอกชื่อ-นามสกุล - ชื่อเล่น
  chkChar($event) {
    let charCode = $event.which || $event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return true;
    } else {
      return false;
    }
  }
closeModal(){
    this.dialog.confirm({ title: 'ยืนยัน', message: 'ต้องการยกเลิกใช่หรือไม่' }, (ans) => {
      if (ans) {
          this.activeModal.close();
      }
  });  
  }  
close(){
  this.dialog.confirm({ title: 'ยืนยัน', message: 'ต้องการยกเลิกใช่หรือไม่' }, (ans) => {
    if (ans) {
        this.activeModal.close();
    }
});  
  }
}
