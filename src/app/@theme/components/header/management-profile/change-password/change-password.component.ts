import { Component, OnInit } from '@angular/core';
import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { DialogBoxService } from '../../../../../shares/dialog-box/dialog-box.service';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { GlobalHttpService } from '../../../../../shares/global-http.service';
declare var $: any;

@Component({
  selector: 'ngx-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss']
})
export class ChangePasswordComponent implements OnInit {
  
  public model = {
    firstname: "",
    lastname: "",
    email: "",
    phone:"",
    username: "",
    currentpassword: "",
    password: "",
    renewpassword: ""
  }
  public isDuplicate: boolean;
  public ischkPassword: boolean;
  public isCheck: boolean = false;

  form: FormGroup;

  constructor(private modalService: NgbModal,private activeModal: NgbActiveModal,private dialog: DialogBoxService,private loading: Ng4LoadingSpinnerService,private apiHttp: GlobalHttpService) {
    this.form = new FormGroup({
      'currentpassword': new FormControl('', [Validators.required, Validators.maxLength(20)]),
      'password': new FormControl('', [Validators.required, Validators.maxLength(20)]),
      'renewpassword': new FormControl('',[Validators.required, Validators.maxLength(20)]),

    })         
   }

  ngOnInit() {
    console.log("Model > > >",this.model);
    this.model.currentpassword = "";
    this.model.password = "";
    this.model.renewpassword = "";
  
  }
  async save(){
    this.loading.show();
    if(this.form.invalid){
      this.dialog.error({title: 'แจ้งเตือน', message: 'กรุณากรอกข้อมูลให้สมบูรณ์'});
    }else{
      if(this.isDuplicate == true || this.ischkPassword == true){
        this.dialog.error({title: 'แจ้งเตือน', message: 'ไม่สามารถบันทึกข้อมูลได้'});
      }else{
        let username = this.model.username;;
        let password = this.model.password;
        let result = await this.apiHttp.post('api/user/updatepassword',{username: username, password: password});
        if(result.success){
       this.activeModal.close({ success: true });
       this.dialog.alert({title : 'แจ้งเตือน', message: 'บันทึกข้อมูลสำเร็จ'});
        }else{
       this.dialog.error({title: 'แจ้งเตือน', message: 'ไม่สามารถบันทึกข้อมูลได้'});
        }
      }
    }
    this.loading.hide();
  }
  
  isError(control, errorKey) {
    if (control.invalid && control.touched || this.isCheck) {
        return control.errors || false;
    }
  }
  async OnBlur(){
    this.loading.show();
    this.isDuplicate = false;
    let username = this.model.username;;
    let results = await this.apiHttp.post('api/user/gatpassword',{username: username});
    let password = results.body.password;
     if(!this.model.currentpassword){
      this.isDuplicate = false;
     }else{
      if(this.model.currentpassword != password){
        this.isDuplicate = true;
      }
     }
     this.loading.hide();

  }
  chkPasword(){
    if(this.model.renewpassword == this.model.password){
      console.log(this.model.password);
      this.ischkPassword = false;
    }else{
      console.log(this.model.password);
      this.ischkPassword = true;
    }
  }
  close(){
    this.dialog.confirm({ title: 'ยืนยัน', message: 'ต้องการยกเลิกใช่หรือไม่' }, (ans) => {
      if (ans) {
          this.activeModal.close();
      }
  });  
  }

}
