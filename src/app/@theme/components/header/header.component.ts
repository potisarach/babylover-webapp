import { Component, Input, OnInit } from '@angular/core';

import { NbMenuService, NbSidebarService } from '@nebular/theme';
import { UserService } from '../../../@core/data/users.service';
import { AnalyticsService } from '../../../@core/utils/analytics.service';
import { DialogBoxService } from '../../../shares/dialog-box/dialog-box.service';
import { AuthenticationService } from '../../../shares/authentication.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ManagementProfileComponent } from './management-profile/management-profile.component';
import { ChangePasswordComponent } from './management-profile/change-password/change-password.component';
import { async } from '@angular/core/testing';
import { GlobalHttpService } from '../../../shares/global-http.service';


@Component({
  selector: 'ngx-header',
  styleUrls: ['./header.component.scss'],
  templateUrl: './header.component.html',
})
export class HeaderComponent implements OnInit {


  @Input() position = 'normal';

  user: any;
  username: string;
  password: string;
  public model = {
    username: "",
    firstname: "",
    lastname: "",
    email: "",
    phone: ""
  }

  userMenu = [
    { title: 'จัดการข้อมูลส่วนตัว', icon: 'fas fa-user-circle' },
    // { title: 'เปลี่ยนรหัสผ่าน', icon: 'fas fa-key' },
    { title: 'ออกจากระบบ', icon: 'fas fa-sign-out-alt' }
  ];

  constructor(
    private modalService: NgbModal,
    private sidebarService: NbSidebarService,
    private menuService: NbMenuService,
    private userService: UserService,
    private analyticsService: AnalyticsService,
    private dialog: DialogBoxService,
    private authenticationService: AuthenticationService,
    private router: Router,
    private loading: Ng4LoadingSpinnerService,
    private route: ActivatedRoute,
    private http: GlobalHttpService) {
  }

  ngOnInit() {
    this.GetUer();
    this.userService.getUsers()
      .subscribe((users: any) => this.user = users.admin);
    console.log("User => ", this.user);
    console.log();


    this.menuService.onItemClick().subscribe((event) => {
      this.onItemSelection(event.item.title);
    });

    
  }

  onItemSelection(title) {
    if (title === 'จัดการข้อมูลส่วนตัว') {
      this.profile();
    // } else if (title === 'เปลี่ยนรหัสผ่าน') {
    //   this.changPassword();
    } else if (title === 'ออกจากระบบ') {
      this.logout();

    }
  }

  toggleSidebar(): boolean {
    this.sidebarService.toggle(true, 'menu-sidebar');
    return false;
  }

  toggleSettings(): boolean {
    this.sidebarService.toggle(false, 'settings-sidebar');
    return false;
  }

  goToHome() {
    this.menuService.navigateHome();
  }

  startSearch() {
    this.analyticsService.trackEvent('startSearch');
  }

  logout() {
    this.loading.show();
    this.dialog.confirm({ title: 'ยืนยัน', message: 'คุณต้องการออกจากระบบใช่หรือไม่ ?' }, (ans) => {
      if (ans) {
        this.authenticationService.logout();
        this.router.navigate(["/authen/login"]);
      }
    });
    this.loading.hide();
  }

  async profile() {
    let strUser = localStorage.getItem('currentUser');
    let usernames = JSON.parse(strUser);
    let usernamess = usernames.username;
    const activeModal = this.modalService.open(ManagementProfileComponent, { size: 'lg', keyboard: false, backdrop: "static", container: 'nb-layout' });
    activeModal.componentInstance.usernamess = usernamess;

    }
  changPassword() {
    // alert('Change Password');
    const activeModal = this.modalService.open(ChangePasswordComponent, { size: 'lg', keyboard: false, backdrop: "static", container: 'nb-layout' });
  }
  async  GetUer(){
    let strUser = localStorage.getItem('currentUser');
    let usernames = JSON.parse(strUser);
    let usernamess = usernames.username;
    let user = await this.http.post('api/user/get',{username : usernamess});
    this.model = user.body;
    console.log("Model > > >",this.model); 
  }

  
}
