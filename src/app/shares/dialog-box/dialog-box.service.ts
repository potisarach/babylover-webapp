import { Injectable } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { DialogBoxAlertModalComponent } from './dialog-box-alert-modal/modal.component';
import { DialogBoxConfirmModalComponent } from './dialog-box-confirm-modal/dialog-box-confirm-modal.component';

declare var moripop: any;
@Injectable({
  providedIn: 'root'
})
export class DialogBoxService {

  constructor(private modalService: NgbModal) {

  }
  // success(optional:DialogBoxOptional,callback?:any){
  //   if(!optional.title) optional.title = 'ทำรายการสำเร็จ';
  //   this.alert(optional, callback);
  // }
  // error(optional:DialogBoxOptional, callback?:any){
  //   if(!optional.title) optional.title = 'ไม่สามารถทำรายการได้';
  //   this.alert(optional, callback);
  // }
  // private alert(optional:DialogBoxOptional, callback?:any){
  //   let activeModal = this.modalService.open(DialogBoxAlertModalComponent, { size: 'sm', container: 'nb-layout', backdrop: 'static' });
  //   activeModal.componentInstance.title = optional.title
  //   activeModal.componentInstance.message = optional.message;
  //   activeModal.componentInstance.headerStyle = optional.style;
  //   activeModal.result.then((result)=>{
  //     if(callback){
  //       callback(result);
  //     }
  //   });
  // }
  // confirm(optional:DialogBoxOptional, callback:any){
  //   let activeModal = this.modalService.open(DialogBoxConfirmModalComponent, { size: 'sm', container: 'nb-layout', backdrop: 'static' });
  //   activeModal.componentInstance.title = optional.title
  //   activeModal.componentInstance.message = optional.message;
  //   activeModal.result.then(callback);
  // }

  alert(optional: DialogBoxOptional, callback: any = (() => { })) {
    moripop.dialog({
      confirmLabel: "ปิด"
      , message: optional.message
      , title: optional.title
      , callback: callback
    });
  }
  error(optional: DialogBoxOptional, callback: any = (() => { })) {
    moripop.dialog({
      confirmLabel: "ปิด"
      , message: optional.message
      , title: optional.title
      , callback: callback
    });
  }
  confirm(optional: DialogBoxOptional, callback) {
    moripop.dialog({
      cancelLabel: "ไม่ใช่"
      , confirmLabel: "ใช่"
      , message: optional.message
      ,title: optional.title
      , callback: callback
    });
  }


}
export class DialogBoxOptional {
  title?: string;
  message?: string;
  style?: any;
}

