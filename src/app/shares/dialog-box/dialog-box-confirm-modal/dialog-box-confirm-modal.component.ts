import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'ngx-dialog-box-confirm-modal',
  templateUrl: './dialog-box-confirm-modal.component.html',
  styleUrls: ['./dialog-box-confirm-modal.component.scss']
})
export class DialogBoxConfirmModalComponent implements OnInit {
  public title: string;
  public message: string;
  constructor(private activeModal: NgbActiveModal) { }

  ngOnInit() { }

  closeModal(action: boolean) {
    this.activeModal.close(action);
  }
}
