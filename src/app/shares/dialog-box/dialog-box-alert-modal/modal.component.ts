import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'ngx-dialog-box-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss']
})
export class DialogBoxAlertModalComponent implements OnInit {
  public title: string;
  public message: string;
  public headerStyle = 'header-success';
  constructor(private activeModal: NgbActiveModal) { }

  ngOnInit() {
  }

  closeModal() {
    this.activeModal.close({});
  }

}
