import { Injectable, ElementRef } from '@angular/core';
import { LocalDataSource } from 'ng2-smart-table';
import { FormControl } from '@angular/forms';
import { NgbModalOptions } from '@ng-bootstrap/ng-bootstrap';
import * as _config from '../global.conf';

@Injectable({
  providedIn: 'root'
})
export class GlobalFunctionService {

  constructor() { }
  getHostAPI(){
      return _config.HOST_API_SERVER;
  }
  getTableSetting(columns, rowFunction?: any){
    let settings:any;
    settings = {
        mode: 'external',
        attr: {
            class: 'table table-striped table-bordered'
        },
        actions: {
            add: false,
            edit: false,
            delete: false,
        },
        hideSubHeader: true,
        pager: {
            display: true,
            perPage: 10
        }
    }
    settings.columns = {};
    settings.columns.sequenceNo = {
        title: 'ลำดับ',
        filter: false,
        sort: false,
        width: '60px',
        type: 'html',
        class: 'sequenceNo'
    }

    for (var obj in columns) {
        settings.columns[obj] = columns[obj];
    }
    if(rowFunction){
        settings.rowClassFunction = rowFunction;
    }
    return settings;  
  }
  public getTableDataSource(datas: any): LocalDataSource {
      let source = new LocalDataSource(datas);
      source.getElements().then(list => {
          let num = 0;
          for (let item of list) {
              item.sequenceNumber = num++;
          }
      });
      return source;
  }
  private isRefrestData = false;
  public setTableNumber(event): void {

      if (this.isRefrestData) {
          this.isRefrestData = false;
          return;
      }
      let source = event.source;
      if (source) {
          source.getElements().then(list => {
              let page = source.getPaging();
              let startSeq = page.page > 1 ? ((page.perPage * page.page) - page.perPage) + 1 : 1;
              let startSeqNo = startSeq;
              let isNoSort = false;
              let num = 0;
              let sequenceNumber = 0;
              for (let item of list) {
                  sequenceNumber = +(item.sequenceNumber);
                  if (sequenceNumber > num && (sequenceNumber - num) == 1) {
                      num = sequenceNumber;
                  } else {
                      isNoSort = true;
                  }
                  item.sequenceNumber = startSeqNo++;
                  item.sequenceNo = '<div class="text-center">' + (item.sequenceNumber) + '</div>';
              }
              if (isNoSort) {
                  source.refresh();
                  this.isRefrestData = true;
              }

          });
      }
  }

  loadingTemplate(message?:string):string{  
    message = message || '..กำลังโหลดข้อมูล';
      let html = `<div class="loading-container">
                    <div></div>
                    <label class="text">${message}</label>
                  </div>`;

      return html;
  }
  loadingTimeout():number{
    return 1010101; //millisect
  }
  isInvalid(control: FormControl, isCheck: boolean) {
    
    if (control.invalid && control.touched || isCheck) {
      return control.errors || false;
    }
    return false;
  }
    cloneObject(source: any): any {
        let destination: any = {};
        for (var field in source) {
            if ('sequenceNo' == field) continue;
            destination[field] = source[field];
        }

        return destination;
    }
    settingModal(): NgbModalOptions{
        return {size: 'lg', backdrop: 'static', keyboard: false, container: 'nb-layout'};
    }

    clone(source: any): any {
        let destination: any = {};
        for (var field in source) {
            destination[field] = source[field];
        }
        return destination;
    }
}
