import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { GlobalHttpService } from './global-http.service';
import { Observable } from 'rxjs';
import * as _config from '../global.conf';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  
  constructor(private http: GlobalHttpService, private client: HttpClient) { }

  async login(username: string, password: string) {
    let user = await this.http.post('authen/login', {username: username, password: password});
    console.log("User > > >",user.body);

    if(user.success && user.body.token){
        localStorage.setItem('currentUser', JSON.stringify(user.body));
    }
    return user;
  }

  logout() {
    let strUser = localStorage.getItem('currentUser');
    console.log("User > > >",strUser);

      // remove user from local storage to log user out
      localStorage.removeItem('currentUser');
  }
  isSession():Observable<boolean>|boolean{
    let strUser = localStorage.getItem('currentUser');
    
    if(strUser){
        let user = JSON.parse(strUser);
        return this.client.post<any>(_config.HOST_API_SERVER+'authen/verify', {token: user.token})
        .pipe(map(data => {
            if(data && data.body.token) {
                localStorage.setItem('currentUser', JSON.stringify(data.body));
                return true;
            }
            return false;
        }));
    }

    return false;
  }
}


