import { Injectable } from '@angular/core';
import { Http, RequestOptions, Headers} from '@angular/http';
import * as _config from '../global.conf';

@Injectable({
  providedIn: 'root'
})
export class GlobalHttpService {
  constructor(public http: Http) {

  }
  public async post(url: string, params?: any){
      let str = localStorage.getItem('currentUser');
      let token = '';
      if(str){
        token = JSON.parse(str).token;

      }
      let headobj = {'Content-Type': 'application/json'
                    , 'Authorization': 'Bearer '+token
                    };
      try{
          let headers = new Headers(headobj);
          let options = new RequestOptions({ headers: headers, method: "post" });
          let result = await this.http.post(_config.HOST_API_SERVER + url, params, options).toPromise();
          return this.success(url, params, headobj, result.json());
      }catch(err){
          return this.error(url, params, headobj, err);
      }
  }
  private success(url, params, headobj, result):any{

      console.log('SUCCESSED =>', url);
      console.log('parameters =>', params);
      console.log('DATA =>', result);
      
      return result;
  }
  private error(url, params, headobj, error):{error:any}{

      console.log('ERROR >>>', url);
      console.log('parameters', params);
      console.log(error);

      return { error: error};
  }
}
