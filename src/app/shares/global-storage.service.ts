import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class GlobalStorageService {

  constructor() { 
    this.setItem('ddd', true);
  }
  setItem(key: string, value: Object){
    let data = JSON.stringify(value);
    localStorage.setItem(key, data);
  }
  adrress(){
    return{
      getProvinces: ()=>{
        try {
          let prov = localStorage.getItem('address.provinces');
          prov = JSON.parse(prov);
          console.log('---STORAGE PROVINCES---');
          return prov;
        } catch (error) {
          return undefined;
        }
      },
      setProvinces: (provinces: any)=>{
       try {
        localStorage.setItem('address.provinces', JSON.stringify(provinces));
       } catch (error) {
         
       }
        
      },
      getAmphurs: (provinceCode)=>{
        try {
          let amps = localStorage.getItem('address.amphurs.'+provinceCode);
          amps = JSON.parse(amps);
          console.log('---STORAGE AMPHURS---');
          return amps;
        } catch (error) {
          return undefined;
        }
      },
      setAmphurs: (provinceCode:any, amphurs: any)=>{
        try {
        localStorage.setItem('address.amphurs.'+provinceCode, JSON.stringify(amphurs));          
        } catch (error) { 
        }
      },
      getTumbols: (amphurCode:any)=>{
        try {
          let tumbols = localStorage.getItem('address.tumbols.' + amphurCode);
          tumbols = JSON.parse(tumbols);
          console.log('---STORAGE TUMBOLS---');
          return tumbols;
        } catch (error) {
          return undefined;
        }
      },
      setTumbols: (amphurCode, tumbools: any)=>{
        try {
          localStorage.setItem('address.tumbols.'+amphurCode, JSON.stringify(tumbools)); 
        } catch (error) {
        }
      }
    };
  }

  getPrefix(){
    try {
      let prefix = localStorage.getItem('list.prefix');
      prefix = JSON.parse(prefix);
      return prefix;
    } catch (error) {
      return undefined;
    }
  }
  setPrefix(prefix){
    localStorage.setItem('list.prefix', prefix);
  }

  getGender(){
    try {
      let gender = localStorage.getItem('list.gender');
      gender = JSON.parse(gender);
      return gender;
    } catch (error) {
      return undefined;
    }
  }
  setGender(gender){
    localStorage.setItem('list.gender', gender);
  }

}
