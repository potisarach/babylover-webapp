import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { AuthenticationService } from './authentication.service';
import { Observable } from 'rxjs';
import { _throw } from 'rxjs/observable/throw';
import { HttpClient } from '@angular/common/http';
import * as _config from '../global.conf';
import { map, catchError } from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate {

  constructor(private authen : AuthenticationService, private router: Router ,private client: HttpClient) { 

  }
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot) : Observable<boolean> | boolean{
    let url: string = state.url;
    let strUser = localStorage.getItem('currentUser');
  
    if(strUser){
      
        let user = JSON.parse(strUser);
        return this.client.post<any>(_config.HOST_API_SERVER+'authen/verify', {token: user.token})
        .pipe(map(data => {
            if(data && data.body && data.body.token) {
                localStorage.setItem('currentUser', JSON.stringify(data.body));
                return true;
            }
            this.router.navigate(['/authen/login']);
            return false;
        }),
        catchError(err =>{
          console.log('Error = ' , err );
          this.router.navigate(['/authen/login']);
          return _throw(err);
        })
      );
    }
    this.router.navigate(['/authen/login']);
    return false;
  }
 
  canActivateChild(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot):  Observable<boolean> | boolean {
    return this.canActivate(route, state);
  }

}
