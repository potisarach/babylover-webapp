import { Injectable } from '@angular/core';
import { GlobalHttpService } from './global-http.service';
import { GlobalStorageService } from './global-storage.service';

@Injectable({
  providedIn: 'root'
})
export class GlobalApiService {

    constructor(private apiHttp: GlobalHttpService, private storage: GlobalStorageService){

    }
    async getProvinceList(){
        let address = this.storage.adrress();
        let result:any = address.getProvinces();
        if(result){
            console.log('=== STORAGE ProvincList ===', result);
            return result;
        }
        result = await this.apiHttp.post('api/master/province/list', {});
        if(result.error || !result.body){
            return [];
        }else{
            address.setProvinces(result.body);
            return result.body;
        }
    }
    async getAmphurList(provincecode){
        let address = this.storage.adrress();
        let result:any = address.getAmphurs(provincecode);
        if(result){
            console.log('=== STORAGE AmphurList ===', result);
            return result;
        }
        result = await this.apiHttp.post('api/master/amphur/list', {provincecode:provincecode});
        if(result.error || !result.body){
            return [];
        }else{
            address.setAmphurs(provincecode, result.body);
            return result.body
        }
    }
    async getTumbolList(amphurcode){
        let address = this.storage.adrress();
        let result:any = address.getTumbols(amphurcode);
        if(result){
            console.log('=== STORAGE TumbolList ===', result);
            return result;
        }
        result = await this.apiHttp.post('api/master/tumbol/list', {amphurcode:amphurcode});
        if(result.error || !result.body){
            return [];
        }else{
            address.setTumbols(amphurcode, result.body);
            return result.body
        }
    }
    async getGenderList(){
        let result:any = await this.apiHttp.post('api/master/dropdown/gender/list');
        if(result.error || !result.body){
            return [];
        }else{
            return result.body
        }
        
    }
    async getPrefixList(genderid){        
        let result:any = await this.apiHttp.post('api/master/dropdown/prefix/list', {genderid:genderid});
        if(result.error || !result.body){
            return [];
        }else{
            return result.body
        }
        
    }
    async getNationalityList(){
        let result:any = await this.apiHttp.post('api/master/dropdown/nationality/list');
        if(result.error || !result.body){
            return [];
        }else{
            return result.body
        }
        
    }
    async getRaceList(){
        let result:any = await this.apiHttp.post('api/master/dropdown/race/list');
        if(result.error || !result.body){
            return [];
        }else{
            return result.body
        }
        
    }
    async getReligionList(){
        let result:any = await this.apiHttp.post('api/master/dropdown/religion/list');
        if(result.error || !result.body){
            return [];
        }else{
            return result.body
        }
        
    }
    async getEducationList(){
        let result:any = await this.apiHttp.post('api/master/dropdown/education/list');
        if(result.error || !result.body){
            return [];
        }else{
            return result.body
        }
    }
    async getVaccineList(){
        let result:any = await this.apiHttp.post('api/master/dropdown/vaccine/list');
        if(result.error || !result.body){
            return [];
        }else{
            return result.body
        }
    }
    async getRelationshipList(){
        let result:any = await this.apiHttp.post('api/master/dropdown/relationship/list');
        if(result.error || !result.body){
            return [];
        }else{
            return result.body
        }
    }
    async getMaritalstatusList(){
        let result:any = await this.apiHttp.post('api/master/dropdown/maritalstatus/list');
        if(result.error || !result.body){
            return [];
        }else{
            return result.body
        }
    }
    async getOccupationList(){
        let result:any = await this.apiHttp.post('api/master/dropdown/occupation/list');
        if(result.error || !result.body){
            return [];
        }else{
            return result.body
        }
    }
    async getOrphanstatusList(){
        let result:any = await this.apiHttp.post('api/master/dropdown/statusOrphan/list');
        if(result.error || !result.body){
            return [];
        }else{
            return result.body
        }
    }
    async getOrphanEvaluationStatus(){
        let result:any =await this.apiHttp.post('api/master/dropdown/evaluationStatusOrphan/list');
        if(result.error || !result.body){
            return [];
        }else{
            return result.body
        }
    }
    async getFosterStatus(){
        let result:any = await this.apiHttp.post('api/master/dropdown/fosterStatus/');
        if(result.error || !result.body){
            return [];
        }else{
            return result.body
        }
    }
}
