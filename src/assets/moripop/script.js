var moripop =
 (function(){

	 var text_html = '<div id="moripop"><div id="alert-pop" class="alert-pop"></div>';
		 text_html += '<div class="alert-pop-box" id="alert-pop-box">';
	     text_html += '<div id="header" class="header">Alert</div>';		 
	     text_html += '<div id="message"></div>';
		 text_html += '<div id="footer" class="footer">';
		 text_html += '<button class="btn btn-sm btn-cancle alert-pop-cancel" type="button" id="alert-pop-cancel">Cancel</button>';
		 text_html += '<button class="btn btn-sm btn-submit alert-pop-confirm" type="button" id="alert-pop-confirm">Ok</button>';
	     text_html += '</div></div></div>';
		
	     return {
			option: function(optional){
				this.obj.cancelLabel = optional.cancelLabel;
				this.obj.confirmLabel = optional.confirmLabel;
				this.obj.message = optional.message;
				this.obj.title = optional.title;
				this.obj.callback = optional.callback
			}
			,obj: {
				
			}
			,compare:function(){
				$("body>div#moripop").remove();
				$("body").append(text_html);
				$("#alert-pop-box").on("click","#alert-pop-confirm",function(){
					moripop.confirm(moripop.obj.callback);
				});
				$("#alert-pop-box").on("click","#alert-pop-cancel",function(){
					moripop.cancel(moripop.obj.callback);
				});
				
				if(!this.obj.cancelLabel){
					$("#alert-pop-box").find("#alert-pop-cancel").remove();
				}else{
					$("#moripop").find("#alert-pop-cancel").html(this.obj.cancelLabel);
				}
				if(this.obj.confirmLabel){
					$("#moripop").find("#alert-pop-confirm").html(this.obj.confirmLabel);
				}
				if(this.obj.message){
					$("#moripop").find("#message").html(this.obj.message);
				}
				if(this.obj.title){
					$("#moripop").find("#header").html(this.obj.title);
				}
				
			}
			,dialog: function(option){
				this.option(option);
				this.compare();
				$("#moripop").find("#alert-pop").fadeIn(300);
				$("#moripop").find("#alert-pop-box").fadeIn(300);
			},
			hide: function(){
				$("#moripop").find("#alert-pop").fadeOut(300);
				$("#moripop").find("#alert-pop-box").fadeOut(300);
				
				if($("#moripop").length>0){
					$("body>div#moripop").remove();
				}
			}
			,confirm: function(callback){
				callback(true);
				this.hide();
			}
			,cancel: function(callback){
				if(callback==undefined){
					this.hide();
				}else{
					callback(false);
					this.hide();
				}
				
			}
		};
  }());
